/*-
 * Copyright (c) 2021 Juan Romero Pardines.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "xbps_api_impl.h"

static int
pkgdb_cb(struct xbps_handle *xhp,
	xbps_object_t obj,
	const char *obj_key UNUSED,
	void *arg UNUSED,
	bool *done UNUSED)
{
	xbps_dictionary_t pkgmetad;
	xbps_dictionary_keysym_t key;
	xbps_array_t array, files_keys, pkg_array;
	xbps_object_t pobj;
	const char *keyname = NULL, *pkgver = NULL, *file = NULL;

	if (!xbps_dictionary_get_string(obj, "pkgver", &pkgver)) {
		return EINVAL;
	}
	pkgmetad = xbps_pkgdb_get_pkg_files(xhp, pkgver);
	if (pkgmetad == NULL) {
		return 0;
	}
	pkg_array = xbps_array_create();
	assert(pkg_array);
	files_keys = xbps_dictionary_all_keys(pkgmetad);
	assert(files_keys);
	for (unsigned int i = 0; i < xbps_array_count(files_keys); i++) {
		key = xbps_array_get(files_keys, i);
		keyname = xbps_dictionary_keysym_value(key);
		if (strcmp(keyname, "dirs") == 0) {
			continue;
		}
		array = xbps_dictionary_get_keysym(pkgmetad, key);
		for (unsigned int x = 0; x < xbps_array_count(array); x++) {
			pobj = xbps_array_get(array, x);
			xbps_dictionary_get_string(pobj, "file", &file);
			if (file == NULL) {
				continue;
			}
			xbps_array_add_string(pkg_array, file);
		}
	}
	xbps_object_release(pkgmetad);
	xbps_object_release(files_keys);
	xbps_dictionary_set(xhp->pkgdb_files, pkgver, pkg_array);
	xbps_object_release(pkg_array);

	return 0;
}

int
xbps_pkgdb_files_update(struct xbps_handle *xhp)
{
	char *plist;
	int rv;

	if (!xhp) {
		return 0;
	}
	if (xhp->pkgdb_files == NULL) {
		xhp->pkgdb_files = xbps_dictionary_create();
		assert(xhp->pkgdb_files);
	}
	rv = xbps_pkgdb_foreach_cb(xhp, pkgdb_cb, NULL);
	if (rv != 0) {
		return rv;
	}
	plist = xbps_xasprintf("%s/%s", xhp->metadir, XBPS_PKGDB_FILES);
	assert(plist);
	if (!xbps_dictionary_externalize_to_file(xhp->pkgdb_files, plist)) {
		free(plist);
		return EINVAL;
	}
	free(plist);
	xbps_object_release(xhp->pkgdb_files);
	xhp->pkgdb_files = NULL;
	return 0;
}
