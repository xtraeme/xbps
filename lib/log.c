/*-
 * Copyright (c) 2011-2020 Juan Romero Pardines. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include "xbps_api_impl.h"

static void __attribute__((__format__ (__printf__, 3, 0)))
common_printf(FILE *f, const char *msg, const char *fmt, va_list ap)
{
	if (msg != NULL)
		fprintf(f, "%s", msg);

	vfprintf(f, fmt, ap);
}

void __attribute__((__format__ (__printf__, 2, 0)))
xbps_dbg_printf_append(struct xbps_handle *xhp, const char *fmt, ...)
{
	va_list ap;

	if (!xhp)
		return;

	if ((xhp->flags & XBPS_FLAG_DEBUG) == 0)
		return;

	va_start(ap, fmt);
	common_printf(stderr, NULL, fmt, ap);
	va_end(ap);
}

void __attribute__((__format__ (__printf__, 2, 0)))
xbps_dbg_printf(struct xbps_handle *xhp, const char *fmt, ...)
{
	va_list ap;

	if (!xhp)
		return;

	if ((xhp->flags & XBPS_FLAG_DEBUG) == 0)
		return;

	va_start(ap, fmt);
	common_printf(stderr, "[DEBUG] ", fmt, ap);
	va_end(ap);
}

void __attribute__((__format__ (__printf__, 1, 0)))
xbps_error_printf(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	common_printf(stderr, "ERROR: ", fmt, ap);
	va_end(ap);
}

void __attribute__((__format__ (__printf__, 1, 0)))
xbps_warn_printf(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	common_printf(stderr, "WARNING: ", fmt, ap);
	va_end(ap);
}

struct event {
	xbps_log_event_t le;
	const char *s;
};

static const struct event events[] = {
	{ XBPS_LOG_EVENT_INSTALL, "install" },
	{ XBPS_LOG_EVENT_REINSTALL, "reinstall" },
	{ XBPS_LOG_EVENT_UPDATE, "update" },
	{ XBPS_LOG_EVENT_CONFIGURE, "configure" },
	{ XBPS_LOG_EVENT_DOWNGRADE, "downgrade" },
	{ XBPS_LOG_EVENT_REMOVE, "remove" },
	{ XBPS_LOG_EVENT_AUTO, "automatic" },
	{ XBPS_LOG_EVENT_MANUAL, "manual" },
	{ XBPS_LOG_EVENT_HOLD, "hold" },
	{ XBPS_LOG_EVENT_UNHOLD, "unhold" },
	{ XBPS_LOG_EVENT_REPOLOCK, "repolock" },
	{ XBPS_LOG_EVENT_REPOUNLOCK, "repounlock" },
	{ XBPS_LOG_EVENT_ALTGROUP_SET, "alternatives-group-set" },
	{ XBPS_LOG_EVENT_ALTGROUP_REMOVED, "alternatives-group-removed" },
	{ XBPS_LOG_EVENT_UNKNOWN, NULL }
};

void
xbps_log_event(struct xbps_handle *xhp, xbps_log_event_t le,
		const char *pkgver, const char *arg1, const char *arg2)
{
	FILE *fp = NULL;
	time_t t;
	const struct event *evp;
	struct tm tm, *tmp = NULL;
	char *narg1 = NULL, *narg2 = NULL, *logf = NULL, *entry = NULL;
	char date[16] = {0};
	bool error = false;
	int rv;

	if (!xhp || !pkgver) {
		return;
	}
	if (xhp->flags & XBPS_FLAG_DISABLE_LOG) {
		return;
	}

	for (evp = events; evp->s != NULL; evp++) {
		if (evp->le == le)
			break;
	}
	if (le == XBPS_LOG_EVENT_UNKNOWN) {
		xbps_dbg_printf(xhp, "[log] unknown log event %d for %s\n", le, pkgver);
		return;
	}
	logf = xbps_xasprintf("%s/events.txt", xhp->metadir);
	if (!logf) {
		error = true;
		goto out;
	}
	t = time(NULL);
	if ((tmp = localtime_r(&t, &tm)) == NULL) {
		error = true;
		goto out;
	}
	if (strftime(date, sizeof(date)-1, "%Y%m%d%H%M%S", tmp) == 0) {
		error = true;
		goto out;
	}
	if (arg1) {
		narg1 = xbps_xasprintf(" %s", arg1);
		if (!narg1) {
			error = true;
			goto out;
		}
	}
	if (arg2) {
		narg2 = xbps_xasprintf(" %s", arg2);
		if (!narg2) {
			error = true;
			goto out;
		}
	}
	entry = xbps_xasprintf("%s %s %s%s%s\n", date, pkgver,
			evp->s, narg1 ? narg1 : "", narg2 ? narg2 : "");
	if (!entry) {
		error = true;
		goto out;
	}
	if ((fp = fopen(logf, "a")) == NULL) {
		error = true;
		goto out;
	}
	if ((rv = fseek(fp, 0, SEEK_END)) != 0) {
		error = true;
		goto out;
	}
	if (fwrite(entry, strlen(entry), 1, fp) != 1) {
		error = true;
		goto out;
	}
	if ((rv = fdatasync(fileno(fp))) != 0) {
		error = true;
	}
out:
	if (fp) {
		fclose(fp);
	}
	free(entry);
	free(logf);
	free(narg1);
	free(narg2);
	if (error) {
		xbps_dbg_printf(xhp, "[log] failed to log event %d for %s\n", le, pkgver);
	}
}
