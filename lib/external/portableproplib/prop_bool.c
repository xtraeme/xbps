/*	$NetBSD: prop_bool.c,v 1.19 2020/06/06 21:25:59 thorpej Exp $	*/

/*-
 * Copyright (c) 2006 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by Jason R. Thorpe.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "prop_object_impl.h"
#include <xbps/xbps_bool.h>

struct _xbps_bool {
	struct _xbps_object	pb_obj;
	bool		pb_value;
};

static struct _xbps_bool _xbps_bool_true;
static struct _xbps_bool _xbps_bool_false;

static _xbps_object_free_rv_t
		_xbps_bool_free(xbps_stack_t, xbps_object_t *);
static bool	_xbps_bool_externalize(
				struct _xbps_object_externalize_context *,
				void *);
static _xbps_object_equals_rv_t
		_xbps_bool_equals(xbps_object_t, xbps_object_t,
				  void **, void **,
				  xbps_object_t *, xbps_object_t *);

static const struct _xbps_object_type _xbps_object_type_bool = {
	.pot_type	=	XBPS_TYPE_BOOL,
	.pot_free	=	_xbps_bool_free,
	.pot_extern	=	_xbps_bool_externalize,
	.pot_equals	=	_xbps_bool_equals,
};

#define	xbps_object_is_bool(x)		\
	((x) != NULL && (x)->pb_obj.po_type == &_xbps_object_type_bool)

/* ARGSUSED */
static _xbps_object_free_rv_t
_xbps_bool_free(xbps_stack_t stack, xbps_object_t *obj)
{
	/*
	 * This should never happen as we "leak" our initial reference
	 * count.
	 */

	/* XXX forced assertion failure? */
	return (_XBPS_OBJECT_FREE_DONE);
}

static bool
_xbps_bool_externalize(struct _xbps_object_externalize_context *ctx,
		       void *v)
{
	xbps_bool_t pb = v;

	return (_xbps_object_externalize_empty_tag(ctx,
	    pb->pb_value ? "t" : "f"));
}

/* ARGSUSED */
static _xbps_object_equals_rv_t
_xbps_bool_equals(xbps_object_t v1, xbps_object_t v2,
    void **stored_pointer1, void **stored_pointer2,
    xbps_object_t *next_obj1, xbps_object_t *next_obj2)
{
	xbps_bool_t b1 = v1;
	xbps_bool_t b2 = v2;

	if (! (xbps_object_is_bool(b1) &&
	       xbps_object_is_bool(b2)))
		return (_XBPS_OBJECT_EQUALS_FALSE);

	/*
	 * Since we only ever allocate one true and one false,
	 * save ourselves a couple of memory operations.
	 */
	if (b1 == b2)
		return (_XBPS_OBJECT_EQUALS_TRUE);
	else
		return (_XBPS_OBJECT_EQUALS_FALSE);
}

_XBPS_ONCE_DECL(_xbps_bool_init_once)

static int
_xbps_bool_init(void)
{

	_xbps_object_init(&_xbps_bool_true.pb_obj,
	    &_xbps_object_type_bool);
	_xbps_bool_true.pb_value = true;

	_xbps_object_init(&_xbps_bool_false.pb_obj,
	    &_xbps_object_type_bool);
	_xbps_bool_false.pb_value = false;

	return 0;
}

static xbps_bool_t
_xbps_bool_alloc(bool val)
{
	xbps_bool_t pb;

	_XBPS_ONCE_RUN(_xbps_bool_init_once, _xbps_bool_init);
	pb = val ? &_xbps_bool_true : &_xbps_bool_false;
	xbps_object_retain(pb);

	return (pb);
}

/*
 * xbps_bool_create --
 *	Create a xbps_bool_t and initialize it with the
 *	provided boolean value.
 */
xbps_bool_t
xbps_bool_create(bool val)
{

	return (_xbps_bool_alloc(val));
}

/*
 * xbps_bool_copy --
 *	Copy a xbps_bool_t.
 */
xbps_bool_t
xbps_bool_copy(xbps_bool_t opb)
{

	if (! xbps_object_is_bool(opb))
		return (NULL);

	/*
	 * Because we only ever allocate one true and one false, this
	 * can be reduced to a simple retain operation.
	 */
	xbps_object_retain(opb);
	return (opb);
}

/*
 * xbps_bool_value --
 *	Get the value of a xbps_bool_t.
 */
bool
xbps_bool_value(xbps_bool_t pb)
{

	if (! xbps_object_is_bool(pb))
		return (false);

	return (pb->pb_value);
}

/*
 * xbps_bool_true --
 *	Historical alias for xbps_bool_value().
 */
bool
xbps_bool_true(xbps_bool_t pb)
{
	return xbps_bool_value(pb);
}

/*
 * xbps_bool_equals --
 *	Return true if the boolean values are equivalent.
 */
bool
xbps_bool_equals(xbps_bool_t b1, xbps_bool_t b2)
{
	if (!xbps_object_is_bool(b1) || !xbps_object_is_bool(b2))
		return (false);

	return (xbps_object_equals(b1, b2));
}

/*
 * _xbps_bool_internalize --
 *	Parse a <true/> or <false/> and return the object created from
 *	the external representation.
 */

/* ARGSUSED */
bool
_xbps_bool_internalize(xbps_stack_t stack, xbps_object_t *obj,
    struct _xbps_object_internalize_context *ctx)
{
	bool val;
	const char *tag_true, *tag_false;

	/* No attributes, and it must be an empty element. */
	if (ctx->poic_tagattr != NULL ||
	    ctx->poic_is_empty_element == false)
	    	return (true);

	if (ctx->poic_xbps) {
		tag_true = "t";
		tag_false = "f";
	} else {
		tag_true = "true";
		tag_false = "false";
	}

	if (_XBPS_TAG_MATCH(ctx, tag_true)) {
		val = true;
	} else {
		_XBPS_ASSERT(_XBPS_TAG_MATCH(ctx, tag_false));
		val = false;
	}

	*obj = xbps_bool_create(val);
	return (true);
}
