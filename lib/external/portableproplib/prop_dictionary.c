/*	$NetBSD: prop_dictionary.c,v 1.42 2020/06/06 21:25:59 thorpej Exp $	*/

/*-
 * Copyright (c) 2006, 2007, 2020 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by Jason R. Thorpe.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "prop_object_impl.h"
#include <xbps/xbps_array.h>
#include <xbps/xbps_dictionary.h>
#include <xbps/xbps_string.h>
#include <rbtree.h>
#include <errno.h>

/*
 * We implement these like arrays, but we keep them sorted by key.
 * This allows us to binary-search as well as keep externalized output
 * sane-looking for human eyes.
 */

#define	EXPAND_STEP		16

/*
 * xbps_dictionary_keysym_t is allocated with space at the end to hold the
 * key.  This must be a regular object so that we can maintain sane iterator
 * semantics -- we don't want to require that the caller release the result
 * of xbps_object_iterator_next().
 *
 * We'd like to have some small'ish keysym objects for up-to-16 characters
 * in a key, some for up-to-32 characters in a key, and then a final bucket
 * for up-to-128 characters in a key (not including NUL).  Keys longer than
 * 128 characters are not allowed.
 */
struct _xbps_dictionary_keysym {
	struct _xbps_object		pdk_obj;
	size_t				pdk_size;
	struct rb_node			pdk_link;
	char 				pdk_key[1];
	/* actually variable length */
};

	/* pdk_key[1] takes care of the NUL */
#define	PDK_SIZE_16		(sizeof(struct _xbps_dictionary_keysym) + 16)
#define	PDK_SIZE_32		(sizeof(struct _xbps_dictionary_keysym) + 32)
#define	PDK_SIZE_128		(sizeof(struct _xbps_dictionary_keysym) + 128)

#define	PDK_MAXKEY		128

_XBPS_POOL_INIT(_xbps_dictionary_keysym16_pool, PDK_SIZE_16, "pdict16")
_XBPS_POOL_INIT(_xbps_dictionary_keysym32_pool, PDK_SIZE_32, "pdict32")
_XBPS_POOL_INIT(_xbps_dictionary_keysym128_pool, PDK_SIZE_128, "pdict128")

struct _xbps_dict_entry {
	xbps_dictionary_keysym_t	pde_key;
	xbps_object_t			pde_objref;
};

struct _xbps_dictionary {
	struct _xbps_object	pd_obj;
	_XBPS_RWLOCK_DECL(pd_rwlock)
	struct _xbps_dict_entry	*pd_array;
	unsigned int		pd_capacity;
	unsigned int		pd_count;
	int			pd_flags;

	uint32_t		pd_version;
};

#define	PD_F_IMMUTABLE		0x01	/* dictionary is immutable */

_XBPS_POOL_INIT(_xbps_dictionary_pool, sizeof(struct _xbps_dictionary),
		"propdict")

static _xbps_object_free_rv_t
		_xbps_dictionary_free(xbps_stack_t, xbps_object_t *);
static void	_xbps_dictionary_emergency_free(xbps_object_t);
static bool	_xbps_dictionary_externalize(
				struct _xbps_object_externalize_context *,
				void *);
static _xbps_object_equals_rv_t
		_xbps_dictionary_equals(xbps_object_t, xbps_object_t,
				        void **, void **,
					xbps_object_t *, xbps_object_t *);
static void	_xbps_dictionary_equals_finish(xbps_object_t, xbps_object_t);
static xbps_object_iterator_t
		_xbps_dictionary_iterator_locked(xbps_dictionary_t);
static xbps_object_t
		_xbps_dictionary_iterator_next_object_locked(void *);
static xbps_object_t
		_xbps_dictionary_get_keysym(xbps_dictionary_t,
					    xbps_dictionary_keysym_t, bool);
static xbps_object_t
		_xbps_dictionary_get(xbps_dictionary_t, const char *, bool);

static void _xbps_dictionary_lock(void);
static void _xbps_dictionary_unlock(void);

static const struct _xbps_object_type _xbps_object_type_dictionary = {
	.pot_type		=	XBPS_TYPE_DICTIONARY,
	.pot_free		=	_xbps_dictionary_free,
	.pot_emergency_free	=	_xbps_dictionary_emergency_free,
	.pot_extern		=	_xbps_dictionary_externalize,
	.pot_equals		=	_xbps_dictionary_equals,
	.pot_equals_finish	=	_xbps_dictionary_equals_finish,
	.pot_lock 	        =       _xbps_dictionary_lock,
	.pot_unlock 	        =       _xbps_dictionary_unlock,		
};

static _xbps_object_free_rv_t
		_xbps_dict_keysym_free(xbps_stack_t, xbps_object_t *);
static bool	_xbps_dict_keysym_externalize(
				struct _xbps_object_externalize_context *,
				void *);
static _xbps_object_equals_rv_t
		_xbps_dict_keysym_equals(xbps_object_t, xbps_object_t,
					 void **, void **,
					 xbps_object_t *, xbps_object_t *);

static const struct _xbps_object_type _xbps_object_type_dict_keysym = {
	.pot_type	=	XBPS_TYPE_DICT_KEYSYM,
	.pot_free	=	_xbps_dict_keysym_free,
	.pot_extern	=	_xbps_dict_keysym_externalize,
	.pot_equals	=	_xbps_dict_keysym_equals,
};

#define	xbps_object_is_dictionary(x)		\
	((x) != NULL && (x)->pd_obj.po_type == &_xbps_object_type_dictionary)
#define	xbps_object_is_dictionary_keysym(x)	\
	((x) != NULL && (x)->pdk_obj.po_type == &_xbps_object_type_dict_keysym)

#define	xbps_dictionary_is_immutable(x)		\
				(((x)->pd_flags & PD_F_IMMUTABLE) != 0)

struct _xbps_dictionary_iterator {
	struct _xbps_object_iterator pdi_base;
	unsigned int		pdi_index;
};

/*
 * Dictionary key symbols are immutable, and we are likely to have many
 * duplicated key symbols.  So, to save memory, we unique'ify key symbols
 * so we only have to have one copy of each string.
 */

static int
/*ARGSUSED*/
_xbps_dict_keysym_rb_compare_nodes(void *ctx _XBPS_ARG_UNUSED,
				   const void *n1, const void *n2)
{
	const struct _xbps_dictionary_keysym *pdk1 = n1;
	const struct _xbps_dictionary_keysym *pdk2 = n2;

	return strcmp(pdk1->pdk_key, pdk2->pdk_key);
}

static int
/*ARGSUSED*/
_xbps_dict_keysym_rb_compare_key(void *ctx _XBPS_ARG_UNUSED,
				 const void *n, const void *v)
{
	const struct _xbps_dictionary_keysym *pdk = n;
	const char *cp = v;

	return strcmp(pdk->pdk_key, cp);
}

static const rb_tree_ops_t _xbps_dict_keysym_rb_tree_ops = {
	.rbto_compare_nodes = _xbps_dict_keysym_rb_compare_nodes,
	.rbto_compare_key = _xbps_dict_keysym_rb_compare_key,
	.rbto_node_offset = offsetof(struct _xbps_dictionary_keysym, pdk_link),
	.rbto_context = NULL
};

static struct rb_tree _xbps_dict_keysym_tree;

_XBPS_ONCE_DECL(_xbps_dict_init_once)
_XBPS_MUTEX_DECL_STATIC(_xbps_dict_keysym_tree_mutex)

static int
_xbps_dict_init(void)
{

	_XBPS_MUTEX_INIT(_xbps_dict_keysym_tree_mutex);
	rb_tree_init(&_xbps_dict_keysym_tree,
			   &_xbps_dict_keysym_rb_tree_ops);
	return 0;
}

static void
_xbps_dict_keysym_put(xbps_dictionary_keysym_t pdk)
{

	if (pdk->pdk_size <= PDK_SIZE_16)
		_XBPS_POOL_PUT(_xbps_dictionary_keysym16_pool, pdk);
	else if (pdk->pdk_size <= PDK_SIZE_32)
		_XBPS_POOL_PUT(_xbps_dictionary_keysym32_pool, pdk);
	else {
		_XBPS_ASSERT(pdk->pdk_size <= PDK_SIZE_128);
		_XBPS_POOL_PUT(_xbps_dictionary_keysym128_pool, pdk);
	}
}

/* ARGSUSED */
static _xbps_object_free_rv_t
_xbps_dict_keysym_free(xbps_stack_t stack, xbps_object_t *obj)
{
	xbps_dictionary_keysym_t pdk = *obj;

	rb_tree_remove_node(&_xbps_dict_keysym_tree, pdk);
	_xbps_dict_keysym_put(pdk);

	return _XBPS_OBJECT_FREE_DONE;
}

static bool
_xbps_dict_keysym_externalize(struct _xbps_object_externalize_context *ctx,
			     void *v)
{
	xbps_dictionary_keysym_t pdk = v;

	/* We externalize these as strings, and they're never empty. */

	_XBPS_ASSERT(pdk->pdk_key[0] != '\0');

	if (_xbps_object_externalize_start_tag(ctx, "s") == false ||
	    _xbps_object_externalize_append_encoded_cstring(ctx,
						pdk->pdk_key) == false ||
	    _xbps_object_externalize_end_tag(ctx, "s") == false)
		return (false);
	
	return (true);
}

/* ARGSUSED */
static _xbps_object_equals_rv_t
_xbps_dict_keysym_equals(xbps_object_t v1, xbps_object_t v2,
    void **stored_pointer1, void **stored_pointer2,
    xbps_object_t *next_obj1, xbps_object_t *next_obj2)
{
	xbps_dictionary_keysym_t pdk1 = v1;
	xbps_dictionary_keysym_t pdk2 = v2;

	/*
	 * There is only ever one copy of a keysym at any given time,
	 * so we can reduce this to a simple pointer equality check.
	 */
	if (pdk1 == pdk2)
		return _XBPS_OBJECT_EQUALS_TRUE;
	else
		return _XBPS_OBJECT_EQUALS_FALSE;
}

static xbps_dictionary_keysym_t
_xbps_dict_keysym_alloc(const char *key)
{
	xbps_dictionary_keysym_t opdk, pdk, rpdk;
	size_t size;

	_XBPS_ONCE_RUN(_xbps_dict_init_once, _xbps_dict_init);

	/*
	 * Check to see if this already exists in the tree.  If it does,
	 * we just retain it and return it.
	 */
	_XBPS_MUTEX_LOCK(_xbps_dict_keysym_tree_mutex);
	opdk = rb_tree_find_node(&_xbps_dict_keysym_tree, key);
	if (opdk != NULL) {
		xbps_object_retain(opdk);
		_XBPS_MUTEX_UNLOCK(_xbps_dict_keysym_tree_mutex);
		return (opdk);
	}
	_XBPS_MUTEX_UNLOCK(_xbps_dict_keysym_tree_mutex);

	/*
	 * Not in the tree.  Create it now.
	 */

	size = sizeof(*pdk) + strlen(key) /* pdk_key[1] covers the NUL */;

	if (size <= PDK_SIZE_16)
		pdk = _XBPS_POOL_GET(_xbps_dictionary_keysym16_pool);
	else if (size <= PDK_SIZE_32)
		pdk = _XBPS_POOL_GET(_xbps_dictionary_keysym32_pool);
	else if (size <= PDK_SIZE_128)
		pdk = _XBPS_POOL_GET(_xbps_dictionary_keysym128_pool);
	else
		pdk = NULL;	/* key too long */

	if (pdk == NULL)
		return (NULL);

	_xbps_object_init(&pdk->pdk_obj, &_xbps_object_type_dict_keysym);

	strcpy(pdk->pdk_key, key);
	pdk->pdk_size = size;

	/*
	 * We dropped the mutex when we allocated the new object, so
	 * we have to check again if it is in the tree.
	 */
	_XBPS_MUTEX_LOCK(_xbps_dict_keysym_tree_mutex);
	opdk = rb_tree_find_node(&_xbps_dict_keysym_tree, key);
	if (opdk != NULL) {
		xbps_object_retain(opdk);
		_XBPS_MUTEX_UNLOCK(_xbps_dict_keysym_tree_mutex);
		_xbps_dict_keysym_put(pdk);
		return (opdk);
	}
	rpdk = rb_tree_insert_node(&_xbps_dict_keysym_tree, pdk);
	_XBPS_ASSERT(rpdk == pdk);
	_XBPS_MUTEX_UNLOCK(_xbps_dict_keysym_tree_mutex);
	return (rpdk);
}

static _xbps_object_free_rv_t
_xbps_dictionary_free(xbps_stack_t stack, xbps_object_t *obj)
{
	xbps_dictionary_t pd = *obj;
	xbps_dictionary_keysym_t pdk;
	xbps_object_t po;

	_XBPS_ASSERT(pd->pd_count <= pd->pd_capacity);
	_XBPS_ASSERT((pd->pd_capacity == 0 && pd->pd_array == NULL) ||
		     (pd->pd_capacity != 0 && pd->pd_array != NULL));

	/* The empty dictorinary is easy, handle that first. */
	if (pd->pd_count == 0) {
		if (pd->pd_array != NULL)
			_XBPS_FREE(pd->pd_array, M_XBPS_DICT);

		_XBPS_RWLOCK_DESTROY(pd->pd_rwlock);

		_XBPS_POOL_PUT(_xbps_dictionary_pool, pd);

		return (_XBPS_OBJECT_FREE_DONE);
	}

	po = pd->pd_array[pd->pd_count - 1].pde_objref;
	_XBPS_ASSERT(po != NULL);

	if (stack == NULL) {
		/*
		 * If we are in emergency release mode,
		 * just let caller recurse down.
		 */
		*obj = po;
		return (_XBPS_OBJECT_FREE_FAILED);
	}

	/* Otherwise, try to push the current object on the stack. */
	if (!_xbps_stack_push(stack, pd, NULL, NULL, NULL)) {
		/* Push failed, entering emergency release mode. */
		return (_XBPS_OBJECT_FREE_FAILED);
	}
	/* Object pushed on stack, caller will release it. */
	--pd->pd_count;
	pdk = pd->pd_array[pd->pd_count].pde_key;
	_XBPS_ASSERT(pdk != NULL);

	xbps_object_release(pdk);

	*obj = po;
	return (_XBPS_OBJECT_FREE_RECURSE);
}


static void
_xbps_dictionary_lock(void)
{

	/* XXX: once necessary or paranoia? */
	_XBPS_ONCE_RUN(_xbps_dict_init_once, _xbps_dict_init);
	_XBPS_MUTEX_LOCK(_xbps_dict_keysym_tree_mutex);
}

static void
_xbps_dictionary_unlock(void)
{
	_XBPS_MUTEX_UNLOCK(_xbps_dict_keysym_tree_mutex);
}

static void
_xbps_dictionary_emergency_free(xbps_object_t obj)
{
	xbps_dictionary_t pd = obj;
	xbps_dictionary_keysym_t pdk;

	_XBPS_ASSERT(pd->pd_count != 0);
	--pd->pd_count;

	pdk = pd->pd_array[pd->pd_count].pde_key;
	_XBPS_ASSERT(pdk != NULL);
	xbps_object_release(pdk);
}

static bool
_xbps_dictionary_externalize(struct _xbps_object_externalize_context *ctx,
			     void *v)
{
	xbps_dictionary_t pd = v;
	xbps_dictionary_keysym_t pdk;
	struct _xbps_object *po;
	xbps_object_iterator_t pi;
	unsigned int i;
	bool rv = false;

	_XBPS_RWLOCK_RDLOCK(pd->pd_rwlock);

	if (pd->pd_count == 0) {
		_XBPS_RWLOCK_UNLOCK(pd->pd_rwlock);
		return (_xbps_object_externalize_empty_tag(ctx, "d"));
	}

	if (_xbps_object_externalize_start_tag(ctx, "d") == false ||
	    _xbps_object_externalize_append_char(ctx, '\n') == false)
		goto out;

	pi = _xbps_dictionary_iterator_locked(pd);
	if (pi == NULL)
		goto out;
	
	ctx->poec_depth++;
	_XBPS_ASSERT(ctx->poec_depth != 0);

	while ((pdk = _xbps_dictionary_iterator_next_object_locked(pi))
	    != NULL) {
		po = _xbps_dictionary_get_keysym(pd, pdk, true);
		if (po == NULL ||
		    _xbps_object_externalize_start_tag(ctx, "k") == false ||
		    _xbps_object_externalize_append_encoded_cstring(ctx,
						   pdk->pdk_key) == false ||
		    _xbps_object_externalize_end_tag(ctx, "k") == false ||
		    (*po->po_type->pot_extern)(ctx, po) == false) {
			xbps_object_iterator_release(pi);
			goto out;
		}
	}

	xbps_object_iterator_release(pi);

	ctx->poec_depth--;
	for (i = 0; i < ctx->poec_depth; i++) {
		if (_xbps_object_externalize_append_char(ctx, '\t') == false)
			goto out;
	}
	if (_xbps_object_externalize_end_tag(ctx, "d") == false)
		goto out;
	
	rv = true;

 out:
	_XBPS_RWLOCK_UNLOCK(pd->pd_rwlock);
	return (rv);
}

/* ARGSUSED */
static _xbps_object_equals_rv_t
_xbps_dictionary_equals(xbps_object_t v1, xbps_object_t v2,
    void **stored_pointer1, void **stored_pointer2,
    xbps_object_t *next_obj1, xbps_object_t *next_obj2)
{
	xbps_dictionary_t dict1 = v1;
	xbps_dictionary_t dict2 = v2;
	uintptr_t idx;
	_xbps_object_equals_rv_t rv = _XBPS_OBJECT_EQUALS_FALSE;

	if (dict1 == dict2)
		return (_XBPS_OBJECT_EQUALS_TRUE);

	_XBPS_ASSERT(*stored_pointer1 == *stored_pointer2);

	idx = (uintptr_t)*stored_pointer1;

	if (idx == 0) {
		if ((uintptr_t)dict1 < (uintptr_t)dict2) {
			_XBPS_RWLOCK_RDLOCK(dict1->pd_rwlock);
			_XBPS_RWLOCK_RDLOCK(dict2->pd_rwlock);
		} else {
			_XBPS_RWLOCK_RDLOCK(dict2->pd_rwlock);
			_XBPS_RWLOCK_RDLOCK(dict1->pd_rwlock);
		}
	}

	if (dict1->pd_count != dict2->pd_count)
		goto out;

	if (idx == dict1->pd_count) {
		rv = _XBPS_OBJECT_EQUALS_TRUE;
		goto out;
	}

	_XBPS_ASSERT(idx < dict1->pd_count);

	*stored_pointer1 = (void *)(idx + 1);
	*stored_pointer2 = (void *)(idx + 1);

	*next_obj1 = dict1->pd_array[idx].pde_objref;
	*next_obj2 = dict2->pd_array[idx].pde_objref;

	if (!xbps_dictionary_keysym_equals(dict1->pd_array[idx].pde_key,
					   dict2->pd_array[idx].pde_key))
		goto out;

	return (_XBPS_OBJECT_EQUALS_RECURSE);

 out:
 	_XBPS_RWLOCK_UNLOCK(dict1->pd_rwlock);
	_XBPS_RWLOCK_UNLOCK(dict2->pd_rwlock);
	return (rv);
}

static void
_xbps_dictionary_equals_finish(xbps_object_t v1, xbps_object_t v2)
{
 	_XBPS_RWLOCK_UNLOCK(((xbps_dictionary_t)v1)->pd_rwlock);
 	_XBPS_RWLOCK_UNLOCK(((xbps_dictionary_t)v2)->pd_rwlock);
}

static xbps_dictionary_t
_xbps_dictionary_alloc(unsigned int capacity)
{
	xbps_dictionary_t pd;
	struct _xbps_dict_entry *array;

	if (capacity != 0) {
		array = _XBPS_CALLOC(capacity * sizeof(*array), M_XBPS_DICT);
		if (array == NULL)
			return (NULL);
	} else
		array = NULL;

	pd = _XBPS_POOL_GET(_xbps_dictionary_pool);
	if (pd != NULL) {
		_xbps_object_init(&pd->pd_obj, &_xbps_object_type_dictionary);

		_XBPS_RWLOCK_INIT(pd->pd_rwlock);
		pd->pd_array = array;
		pd->pd_capacity = capacity;
		pd->pd_count = 0;
		pd->pd_flags = 0;

		pd->pd_version = 0;
	} else if (array != NULL)
		_XBPS_FREE(array, M_XBPS_DICT);

	return (pd);
}

static bool
_xbps_dictionary_expand(xbps_dictionary_t pd, unsigned int capacity)
{
	struct _xbps_dict_entry *array, *oarray;

	/*
	 * Dictionary must be WRITE-LOCKED.
	 */

	oarray = pd->pd_array;

	array = _XBPS_CALLOC(capacity * sizeof(*array), M_XBPS_DICT);
	if (array == NULL)
		return (false);
	if (oarray != NULL)
		memcpy(array, oarray, pd->pd_capacity * sizeof(*array));
	pd->pd_array = array;
	pd->pd_capacity = capacity;

	if (oarray != NULL)
		_XBPS_FREE(oarray, M_XBPS_DICT);
	
	return (true);
}

static xbps_object_t
_xbps_dictionary_iterator_next_object_locked(void *v)
{
	struct _xbps_dictionary_iterator *pdi = v;
	xbps_dictionary_t pd = pdi->pdi_base.pi_obj;
	xbps_dictionary_keysym_t pdk = NULL;

	_XBPS_ASSERT(xbps_object_is_dictionary(pd));

	if (pd->pd_version != pdi->pdi_base.pi_version)
		goto out;	/* dictionary changed during iteration */

	_XBPS_ASSERT(pdi->pdi_index <= pd->pd_count);

	if (pdi->pdi_index == pd->pd_count)
		goto out;	/* we've iterated all objects */

	pdk = pd->pd_array[pdi->pdi_index].pde_key;
	pdi->pdi_index++;

 out:
	return (pdk);
}

static xbps_object_t
_xbps_dictionary_iterator_next_object(void *v)
{
	struct _xbps_dictionary_iterator *pdi = v;
	xbps_dictionary_t pd _XBPS_ARG_UNUSED = pdi->pdi_base.pi_obj;
	xbps_dictionary_keysym_t pdk;

	_XBPS_ASSERT(xbps_object_is_dictionary(pd));

	_XBPS_RWLOCK_RDLOCK(pd->pd_rwlock);
	pdk = _xbps_dictionary_iterator_next_object_locked(pdi);
	_XBPS_RWLOCK_UNLOCK(pd->pd_rwlock);
	return (pdk);
}

static void
_xbps_dictionary_iterator_reset_locked(void *v)
{
	struct _xbps_dictionary_iterator *pdi = v;
	xbps_dictionary_t pd = pdi->pdi_base.pi_obj;

	_XBPS_ASSERT(xbps_object_is_dictionary(pd));

	pdi->pdi_index = 0;
	pdi->pdi_base.pi_version = pd->pd_version;
}

static void
_xbps_dictionary_iterator_reset(void *v)
{
	struct _xbps_dictionary_iterator *pdi = v;
	xbps_dictionary_t pd _XBPS_ARG_UNUSED = pdi->pdi_base.pi_obj;

	_XBPS_RWLOCK_RDLOCK(pd->pd_rwlock);
	_xbps_dictionary_iterator_reset_locked(pdi);
	_XBPS_RWLOCK_UNLOCK(pd->pd_rwlock);
}

/*
 * xbps_dictionary_create --
 *	Create a dictionary.
 */
xbps_dictionary_t
xbps_dictionary_create(void)
{

	return (_xbps_dictionary_alloc(0));
}

/*
 * xbps_dictionary_create_with_capacity --
 *	Create a dictionary with the capacity to store N objects.
 */
xbps_dictionary_t
xbps_dictionary_create_with_capacity(unsigned int capacity)
{

	return (_xbps_dictionary_alloc(capacity));
}

/*
 * xbps_dictionary_copy --
 *	Copy a dictionary.  The new dictionary has an initial capacity equal
 *	to the number of objects stored int the original dictionary.  The new
 *	dictionary contains refrences to the original dictionary's objects,
 *	not copies of those objects (i.e. a shallow copy).
 */
xbps_dictionary_t
xbps_dictionary_copy(xbps_dictionary_t opd)
{
	xbps_dictionary_t pd;
	xbps_dictionary_keysym_t pdk;
	xbps_object_t po;
	unsigned int idx;

	if (! xbps_object_is_dictionary(opd))
		return (NULL);

	_XBPS_RWLOCK_RDLOCK(opd->pd_rwlock);

	pd = _xbps_dictionary_alloc(opd->pd_count);
	if (pd != NULL) {
		for (idx = 0; idx < opd->pd_count; idx++) {
			pdk = opd->pd_array[idx].pde_key;
			po = opd->pd_array[idx].pde_objref;

			xbps_object_retain(pdk);
			xbps_object_retain(po);

			pd->pd_array[idx].pde_key = pdk;
			pd->pd_array[idx].pde_objref = po;
		}
		pd->pd_count = opd->pd_count;
		pd->pd_flags = opd->pd_flags;
	}
	_XBPS_RWLOCK_UNLOCK(opd->pd_rwlock);
	return (pd);
}

/*
 * xbps_dictionary_copy_mutable --
 *	Like xbps_dictionary_copy(), but the resulting dictionary is
 *	mutable.
 */
xbps_dictionary_t
xbps_dictionary_copy_mutable(xbps_dictionary_t opd)
{
	xbps_dictionary_t pd;

	if (! xbps_object_is_dictionary(opd))
		return (NULL);

	pd = xbps_dictionary_copy(opd);
	if (pd != NULL)
		pd->pd_flags &= ~PD_F_IMMUTABLE;

	return (pd);
}

/*
 * xbps_dictionary_make_immutable --
 *	Set the immutable flag on that dictionary.
 */
void
xbps_dictionary_make_immutable(xbps_dictionary_t pd)
{

	_XBPS_RWLOCK_WRLOCK(pd->pd_rwlock);
	if (xbps_dictionary_is_immutable(pd) == false)
		pd->pd_flags |= PD_F_IMMUTABLE;
	_XBPS_RWLOCK_UNLOCK(pd->pd_rwlock);
}

/*
 * xbps_dictionary_count --
 *	Return the number of objects stored in the dictionary.
 */
unsigned int
xbps_dictionary_count(xbps_dictionary_t pd)
{
	unsigned int rv;

	if (! xbps_object_is_dictionary(pd))
		return (0);

	_XBPS_RWLOCK_RDLOCK(pd->pd_rwlock);
	rv = pd->pd_count;
	_XBPS_RWLOCK_UNLOCK(pd->pd_rwlock);

	return (rv);
}

/*
 * xbps_dictionary_ensure_capacity --
 *	Ensure that the dictionary has the capacity to store the specified
 *	total number of objects (including the objects already stored in
 *	the dictionary).
 */
bool
xbps_dictionary_ensure_capacity(xbps_dictionary_t pd, unsigned int capacity)
{
	bool rv;

	if (! xbps_object_is_dictionary(pd))
		return (false);

	_XBPS_RWLOCK_WRLOCK(pd->pd_rwlock);
	if (capacity > pd->pd_capacity)
		rv = _xbps_dictionary_expand(pd, capacity);
	else
		rv = true;
	_XBPS_RWLOCK_UNLOCK(pd->pd_rwlock);
	return (rv);
}

static xbps_object_iterator_t
_xbps_dictionary_iterator_locked(xbps_dictionary_t pd)
{
	struct _xbps_dictionary_iterator *pdi;

	if (! xbps_object_is_dictionary(pd))
		return (NULL);

	pdi = _XBPS_CALLOC(sizeof(*pdi), M_TEMP);
	if (pdi == NULL)
		return (NULL);
	pdi->pdi_base.pi_next_object = _xbps_dictionary_iterator_next_object;
	pdi->pdi_base.pi_reset = _xbps_dictionary_iterator_reset;
	xbps_object_retain(pd);
	pdi->pdi_base.pi_obj = pd;
	_xbps_dictionary_iterator_reset_locked(pdi);

	return (&pdi->pdi_base);
}

/*
 * xbps_dictionary_iterator --
 *	Return an iterator for the dictionary.  The dictionary is retained by
 *	the iterator.
 */
xbps_object_iterator_t
xbps_dictionary_iterator(xbps_dictionary_t pd)
{
	xbps_object_iterator_t pi;

	_XBPS_RWLOCK_RDLOCK(pd->pd_rwlock);
	pi = _xbps_dictionary_iterator_locked(pd);
	_XBPS_RWLOCK_UNLOCK(pd->pd_rwlock);
	return (pi);
}

/*
 * xbps_dictionary_all_keys --
 *	Return an array containing a snapshot of all of the keys
 *	in the dictionary.
 */
xbps_array_t
xbps_dictionary_all_keys(xbps_dictionary_t pd)
{
	xbps_array_t array;
	unsigned int idx;
	bool rv = true;

	if (! xbps_object_is_dictionary(pd))
		return (NULL);

	/* There is no pressing need to lock the dictionary for this. */
	array = xbps_array_create_with_capacity(pd->pd_count);

	_XBPS_RWLOCK_RDLOCK(pd->pd_rwlock);

	for (idx = 0; idx < pd->pd_count; idx++) {
		rv = xbps_array_add(array, pd->pd_array[idx].pde_key);
		if (rv == false)
			break;
	}

	_XBPS_RWLOCK_UNLOCK(pd->pd_rwlock);

	if (rv == false) {
		xbps_object_release(array);
		array = NULL;
	}
	return (array);
}

static struct _xbps_dict_entry *
_xbps_dict_lookup(xbps_dictionary_t pd, const char *key,
		  unsigned int *idxp)
{
	struct _xbps_dict_entry *pde;
	unsigned int base, idx, distance;
	int res;

	/*
	 * Dictionary must be READ-LOCKED or WRITE-LOCKED.
	 */

	for (idx = 0, base = 0, distance = pd->pd_count; distance != 0;
	     distance >>= 1) {
		idx = base + (distance >> 1);
		pde = &pd->pd_array[idx];
		_XBPS_ASSERT(pde->pde_key != NULL);
		res = strcmp(key, pde->pde_key->pdk_key);
		if (res == 0) {
			if (idxp != NULL)
				*idxp = idx;
			return (pde);
		}
		if (res > 0) {	/* key > pdk_key: move right */
			base = idx + 1;
			distance--;
		}		/* else move left */
	}

	/* idx points to the slot we looked at last. */
	if (idxp != NULL)
		*idxp = idx;
	return (NULL);
}

static xbps_object_t
_xbps_dictionary_get(xbps_dictionary_t pd, const char *key, bool locked)
{
	const struct _xbps_dict_entry *pde;
	xbps_object_t po = NULL;

	if (! xbps_object_is_dictionary(pd))
		return (NULL);

	if (!locked)
		_XBPS_RWLOCK_RDLOCK(pd->pd_rwlock);
	pde = _xbps_dict_lookup(pd, key, NULL);
	if (pde != NULL) {
		_XBPS_ASSERT(pde->pde_objref != NULL);
		po = pde->pde_objref;
	}
	if (!locked)
		_XBPS_RWLOCK_UNLOCK(pd->pd_rwlock);
	return (po);
}
/*
 * xbps_dictionary_get --
 *	Return the object stored with specified key.
 */
xbps_object_t
xbps_dictionary_get(xbps_dictionary_t pd, const char *key)
{
	xbps_object_t po = NULL;

	if (! xbps_object_is_dictionary(pd))
		return (NULL);

	_XBPS_RWLOCK_RDLOCK(pd->pd_rwlock);
	po = _xbps_dictionary_get(pd, key, true);
	_XBPS_RWLOCK_UNLOCK(pd->pd_rwlock);
	return (po);
}

static xbps_object_t
_xbps_dictionary_get_keysym(xbps_dictionary_t pd, xbps_dictionary_keysym_t pdk,
    bool locked)
{

	if (! (xbps_object_is_dictionary(pd) &&
	       xbps_object_is_dictionary_keysym(pdk)))
		return (NULL);

	return (_xbps_dictionary_get(pd, pdk->pdk_key, locked));
}

/*
 * xbps_dictionary_get_keysym --
 *	Return the object stored at the location encoded by the keysym.
 */
xbps_object_t
xbps_dictionary_get_keysym(xbps_dictionary_t pd, xbps_dictionary_keysym_t pdk)
{

	return (_xbps_dictionary_get_keysym(pd, pdk, false));
}

/*
 * xbps_dictionary_set --
 *	Store a reference to an object at with the specified key.
 *	If the key already exisit, the original object is released.
 */
bool
xbps_dictionary_set(xbps_dictionary_t pd, const char *key, xbps_object_t po)
{
	struct _xbps_dict_entry *pde;
	xbps_dictionary_keysym_t pdk;
	unsigned int idx;
	bool rv = false;

	if (! xbps_object_is_dictionary(pd))
		return (false);

	_XBPS_ASSERT(pd->pd_count <= pd->pd_capacity);

	if (xbps_dictionary_is_immutable(pd))
		return (false);

	_XBPS_RWLOCK_WRLOCK(pd->pd_rwlock);

	pde = _xbps_dict_lookup(pd, key, &idx);
	if (pde != NULL) {
		xbps_object_t opo = pde->pde_objref;
		xbps_object_retain(po);
		pde->pde_objref = po;
		xbps_object_release(opo);
		rv = true;
		goto out;
	}

	pdk = _xbps_dict_keysym_alloc(key);
	if (pdk == NULL)
		goto out;

	if (pd->pd_count == pd->pd_capacity &&
	    _xbps_dictionary_expand(pd,
	    			    pd->pd_capacity + EXPAND_STEP) == false) {
		xbps_object_release(pdk);
	    	goto out;
	}

	/* At this point, the store will succeed. */
	xbps_object_retain(po);

	if (pd->pd_count == 0) {
		pd->pd_array[0].pde_key = pdk;
		pd->pd_array[0].pde_objref = po;
		pd->pd_count++;
		pd->pd_version++;
		rv = true;
		goto out;
	}

	pde = &pd->pd_array[idx];
	_XBPS_ASSERT(pde->pde_key != NULL);

	if (strcmp(key, pde->pde_key->pdk_key) < 0) {
		/*
		 * key < pdk_key: insert to the left.  This is the same as
		 * inserting to the right, except we decrement the current
		 * index first.
		 *
		 * Because we're unsigned, we have to special case 0
		 * (grumble).
		 */
		if (idx == 0) {
			memmove(&pd->pd_array[1], &pd->pd_array[0],
				pd->pd_count * sizeof(*pde));
			pd->pd_array[0].pde_key = pdk;
			pd->pd_array[0].pde_objref = po;
			pd->pd_count++;
			pd->pd_version++;
			rv = true;
			goto out;
		}
		idx--;
	}

	memmove(&pd->pd_array[idx + 2], &pd->pd_array[idx + 1],
		(pd->pd_count - (idx + 1)) * sizeof(*pde));
	pd->pd_array[idx + 1].pde_key = pdk;
	pd->pd_array[idx + 1].pde_objref = po;
	pd->pd_count++;

	pd->pd_version++;

	rv = true;

 out:
	_XBPS_RWLOCK_UNLOCK(pd->pd_rwlock);
	return (rv);
}

/*
 * xbps_dictionary_set_keysym --
 *	Replace the object in the dictionary at the location encoded by
 *	the keysym.
 */
bool
xbps_dictionary_set_keysym(xbps_dictionary_t pd, xbps_dictionary_keysym_t pdk,
			   xbps_object_t po)
{

	if (! (xbps_object_is_dictionary(pd) &&
	       xbps_object_is_dictionary_keysym(pdk)))
		return (false);

	return (xbps_dictionary_set(pd, pdk->pdk_key, po));
}

static void
_xbps_dictionary_remove(xbps_dictionary_t pd, struct _xbps_dict_entry *pde,
    unsigned int idx)
{
	xbps_dictionary_keysym_t pdk = pde->pde_key;
	xbps_object_t po = pde->pde_objref;

	/*
	 * Dictionary must be WRITE-LOCKED.
	 */

	_XBPS_ASSERT(pd->pd_count != 0);
	_XBPS_ASSERT(idx < pd->pd_count);
	_XBPS_ASSERT(pde == &pd->pd_array[idx]);

	idx++;
	memmove(&pd->pd_array[idx - 1], &pd->pd_array[idx],
		(pd->pd_count - idx) * sizeof(*pde));
	pd->pd_count--;
	pd->pd_version++;


	xbps_object_release(pdk);

	xbps_object_release(po);
}

/*
 * xbps_dictionary_remove --
 *	Remove the reference to an object with the specified key from
 *	the dictionary.
 */
void
xbps_dictionary_remove(xbps_dictionary_t pd, const char *key)
{
	struct _xbps_dict_entry *pde;
	unsigned int idx;

	if (! xbps_object_is_dictionary(pd))
		return;

	_XBPS_RWLOCK_WRLOCK(pd->pd_rwlock);

	/* XXX Should this be a _XBPS_ASSERT()? */
	if (xbps_dictionary_is_immutable(pd))
		goto out;

	pde = _xbps_dict_lookup(pd, key, &idx);
	/* XXX Should this be a _XBPS_ASSERT()? */
	if (pde == NULL)
		goto out;

	_xbps_dictionary_remove(pd, pde, idx);
 out:
	_XBPS_RWLOCK_UNLOCK(pd->pd_rwlock);
}

/*
 * xbps_dictionary_remove_keysym --
 *	Remove a reference to an object stored in the dictionary at the
 *	location encoded by the keysym.
 */
void
xbps_dictionary_remove_keysym(xbps_dictionary_t pd,
			      xbps_dictionary_keysym_t pdk)
{

	if (! (xbps_object_is_dictionary(pd) &&
	       xbps_object_is_dictionary_keysym(pdk)))
		return;

	xbps_dictionary_remove(pd, pdk->pdk_key);
}

/*
 * xbps_dictionary_equals --
 *	Return true if the two dictionaries are equivalent.  Note we do a
 *	by-value comparison of the objects in the dictionary.
 */
bool
xbps_dictionary_equals(xbps_dictionary_t dict1, xbps_dictionary_t dict2)
{
	if (!xbps_object_is_dictionary(dict1) ||
	    !xbps_object_is_dictionary(dict2))
		return (false);

	return (xbps_object_equals(dict1, dict2));
}

/*
 * xbps_dictionary_keysym_value --
 *	Return a reference to the keysym's value.
 */
const char *
xbps_dictionary_keysym_value(xbps_dictionary_keysym_t pdk)
{

	if (! xbps_object_is_dictionary_keysym(pdk))
		return (NULL);

	return (pdk->pdk_key);
}

_XBPS_DEPRECATED(xbps_dictionary_keysym_cstring_nocopy,
    "this program uses xbps_dictionary_keysym_cstring_nocopy(), "
    "which is deprecated; use xbps_dictionary_keysym_value() instead.")
const char *
xbps_dictionary_keysym_cstring_nocopy(xbps_dictionary_keysym_t pdk)
{

	if (! xbps_object_is_dictionary_keysym(pdk))
		return (NULL);

	return (pdk->pdk_key);
}

/*
 * xbps_dictionary_keysym_equals --
 *	Return true if the two dictionary key symbols are equivalent.
 *	Note: We do not compare the object references.
 */
bool
xbps_dictionary_keysym_equals(xbps_dictionary_keysym_t pdk1,
			      xbps_dictionary_keysym_t pdk2)
{
	if (!xbps_object_is_dictionary_keysym(pdk1) ||
	    !xbps_object_is_dictionary_keysym(pdk2))
		return (false);

	return (xbps_object_equals(pdk1, pdk2));
}

/*
 * xbps_dictionary_externalize --
 *	Externalize a dictionary, returning a NUL-terminated buffer
 *	containing the XML-style representation.  The buffer is allocated
 *	with the M_TEMP memory type.
 */
char *
xbps_dictionary_externalize(xbps_dictionary_t pd)
{
	struct _xbps_object_externalize_context *ctx;
	char *cp;

	ctx = _xbps_object_externalize_context_alloc();
	if (ctx == NULL)
		return (NULL);

	if (_xbps_object_externalize_header(ctx) == false ||
	    (*pd->pd_obj.po_type->pot_extern)(ctx, pd) == false ||
	    _xbps_object_externalize_footer(ctx) == false) {
		/* We are responsible for releasing the buffer. */
		_XBPS_FREE(ctx->poec_buf, M_TEMP);
		_xbps_object_externalize_context_free(ctx);
		return (NULL);
	}

	cp = ctx->poec_buf;
	_xbps_object_externalize_context_free(ctx);

	return (cp);
}

/*
 * _xbps_dictionary_internalize --
 *	Parse a <dict>...</dict> and return the object created from the
 *	external representation.
 *
 * Internal state in via rec_data is the storage area for the last processed
 * key.
 * _xbps_dictionary_internalize_body is the upper half of the parse loop.
 * It is responsible for parsing the key directly and storing it in the area
 * referenced by rec_data.
 * _xbps_dictionary_internalize_cont is the lower half and called with the value
 * associated with the key.
 */
static bool _xbps_dictionary_internalize_body(xbps_stack_t,
    xbps_object_t *, struct _xbps_object_internalize_context *, char *);

bool
_xbps_dictionary_internalize(xbps_stack_t stack, xbps_object_t *obj,
    struct _xbps_object_internalize_context *ctx)
{
	xbps_dictionary_t dict;
	char *tmpkey;

	/* We don't currently understand any attributes. */
	if (ctx->poic_tagattr != NULL)
		return (true);

	dict = xbps_dictionary_create();
	if (dict == NULL)
		return (true);

	if (ctx->poic_is_empty_element) {
		*obj = dict;
		return (true);
	}

	tmpkey = _XBPS_MALLOC(PDK_MAXKEY + 1, M_TEMP);
	if (tmpkey == NULL) {
		xbps_object_release(dict);
		return (true);
	}

	*obj = dict;
	/*
	 * Opening tag is found, storage for key allocated and
	 * now continue to the first element.
	 */
	return _xbps_dictionary_internalize_body(stack, obj, ctx, tmpkey);
}

static bool
_xbps_dictionary_internalize_continue(xbps_stack_t stack, xbps_object_t *obj,
    struct _xbps_object_internalize_context *ctx, void *data, xbps_object_t child)
{
	xbps_dictionary_t dict = *obj;
	char *tmpkey = data;

	_XBPS_ASSERT(tmpkey != NULL);

	if (child == NULL ||
	    xbps_dictionary_set(dict, tmpkey, child) == false) {
		_XBPS_FREE(tmpkey, M_TEMP);
		if (child != NULL)
			xbps_object_release(child);
		xbps_object_release(dict);
		*obj = NULL;
		return (true);
	}

	xbps_object_release(child);

	/*
	 * key, value was added, now continue looking for the next key
	 * or the closing tag.
	 */
	return _xbps_dictionary_internalize_body(stack, obj, ctx, tmpkey);
}

static bool
_xbps_dictionary_internalize_body(xbps_stack_t stack, xbps_object_t *obj,
    struct _xbps_object_internalize_context *ctx, char *tmpkey)
{
	xbps_dictionary_t dict = *obj;
	size_t keylen;
	const char *tag_d, *tag_k;

	/* Fetch the next tag. */
	if (_xbps_object_internalize_find_tag(ctx, NULL, _XBPS_TAG_TYPE_EITHER) == false)
		goto bad;

	/* Check to see if this is the end of the dictionary. */
	if (ctx->poic_xbps) {
		tag_d = "d";
		tag_k = "k";
	} else {
		tag_d = "dict";
		tag_k = "key";
	}

	if (_XBPS_TAG_MATCH(ctx, tag_d) &&
	    ctx->poic_tag_type == _XBPS_TAG_TYPE_END) {
		_XBPS_FREE(tmpkey, M_TEMP);
		return (true);
	}
	/* Ok, it must be a non-empty key start tag. */
	if (!_XBPS_TAG_MATCH(ctx, tag_k) ||
	    ctx->poic_tag_type != _XBPS_TAG_TYPE_START ||
	    ctx->poic_is_empty_element)
		goto bad;

	if (_xbps_object_internalize_decode_string(ctx,
					tmpkey, PDK_MAXKEY, &keylen,
					&ctx->poic_cp) == false)
		goto bad;

	_XBPS_ASSERT(keylen <= PDK_MAXKEY);
	tmpkey[keylen] = '\0';

	if (_xbps_object_internalize_find_tag(ctx, tag_k,
			_XBPS_TAG_TYPE_END) == false)
		goto bad;
   
	/* ..and now the beginning of the value. */
	if (_xbps_object_internalize_find_tag(ctx, NULL,
				_XBPS_TAG_TYPE_START) == false)
		goto bad;

	/*
	 * Key is found, now wait for value to be parsed.
	 */
	if (_xbps_stack_push(stack, *obj,
			     _xbps_dictionary_internalize_continue,
			     tmpkey, NULL))
		return (false);

 bad:
	_XBPS_FREE(tmpkey, M_TEMP);
	xbps_object_release(dict);
	*obj = NULL;
	return (true);
}

/*
 * xbps_dictionary_internalize --
 *	Create a dictionary by parsing the NUL-terminated XML-style
 *	representation.
 */
xbps_dictionary_t
xbps_dictionary_internalize(const char *xml)
{
	return _xbps_generic_internalize(xml, "dict");
}

/*
 * xbps_dictionary_externalize_to_file --
 *	Externalize a dictionary to the specified file.
 */
bool
xbps_dictionary_externalize_to_file(xbps_dictionary_t dict, const char *fname)
{
	char *xml;
	bool rv;
	int save_errno = 0;	/* XXXGCC -Wuninitialized [mips, ...] */

	xml = xbps_dictionary_externalize(dict);
	if (xml == NULL)
		return (false);
	rv = _xbps_object_externalize_write_file(fname, xml, strlen(xml), false);
	if (rv == false)
		save_errno = errno;
	_XBPS_FREE(xml, M_TEMP);
	if (rv == false)
		errno = save_errno;

	return (rv);
}

/*
 * xbps_dictionary_internalize_from_file --
 *	Internalize a dictionary from a file.
 */
xbps_dictionary_t
xbps_dictionary_internalize_from_file(const char *fname)
{
	struct _xbps_object_internalize_mapped_file *mf;
	xbps_dictionary_t dict;

	mf = _xbps_object_internalize_map_file(fname);
	if (mf == NULL)
		return (NULL);
	dict = xbps_dictionary_internalize(mf->poimf_xml);
	_xbps_object_internalize_unmap_file(mf);

	return (dict);
}
