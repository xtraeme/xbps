/*	$NetBSD: prop_array.c,v 1.21 2012/07/27 09:10:59 pooka Exp $	*/

/*-
 * Copyright (c) 2006, 2007 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by Jason R. Thorpe.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "prop_object_impl.h"
#include <xbps/xbps_array.h>

#include <errno.h>

struct _xbps_array {
	struct _xbps_object	pa_obj;
	_XBPS_RWLOCK_DECL(pa_rwlock)
	xbps_object_t *		pa_array;
	unsigned int		pa_capacity;
	unsigned int		pa_count;
	int			pa_flags;

	uint32_t		pa_version;
};

#define PA_F_IMMUTABLE		0x01	/* array is immutable */

_XBPS_POOL_INIT(_xbps_array_pool, sizeof(struct _xbps_array), "xbpsaray")

static _xbps_object_free_rv_t
		_xbps_array_free(xbps_stack_t, xbps_object_t *);
static void	_xbps_array_emergency_free(xbps_object_t);
static bool	_xbps_array_externalize(
				struct _xbps_object_externalize_context *,
				void *);
static _xbps_object_equals_rv_t
		_xbps_array_equals(xbps_object_t, xbps_object_t,
				   void **, void **,
				   xbps_object_t *, xbps_object_t *);
static void	_xbps_array_equals_finish(xbps_object_t, xbps_object_t);
static xbps_object_iterator_t
		_xbps_array_iterator_locked(xbps_array_t);
static xbps_object_t
		_xbps_array_iterator_next_object_locked(void *);
static void	_xbps_array_iterator_reset_locked(void *);

static const struct _xbps_object_type _xbps_object_type_array = {
	.pot_type		=	XBPS_TYPE_ARRAY,
	.pot_free		=	_xbps_array_free,
	.pot_emergency_free	=	_xbps_array_emergency_free,
	.pot_extern		=	_xbps_array_externalize,
	.pot_equals		=	_xbps_array_equals,
	.pot_equals_finish	=	_xbps_array_equals_finish,
};

#define xbps_object_is_array(x)		\
	((x) != NULL && (x)->pa_obj.po_type == &_xbps_object_type_array)

#define xbps_array_is_immutable(x) (((x)->pa_flags & PA_F_IMMUTABLE) != 0)

struct _xbps_array_iterator {
	struct _xbps_object_iterator pai_base;
	unsigned int		pai_index;
};

#define EXPAND_STEP		16

static _xbps_object_free_rv_t
_xbps_array_free(xbps_stack_t stack, xbps_object_t *obj)
{
	xbps_array_t pa = *obj;
	xbps_object_t po;

	_XBPS_ASSERT(pa->pa_count <= pa->pa_capacity);
	_XBPS_ASSERT((pa->pa_capacity == 0 && pa->pa_array == NULL) ||
		     (pa->pa_capacity != 0 && pa->pa_array != NULL));

	/* The easy case is an empty array, just free and return. */
	if (pa->pa_count == 0) {
		if (pa->pa_array != NULL)
			_XBPS_FREE(pa->pa_array, M_XBPS_ARRAY);

		_XBPS_RWLOCK_DESTROY(pa->pa_rwlock);

		_XBPS_POOL_PUT(_xbps_array_pool, pa);

		return (_XBPS_OBJECT_FREE_DONE);
	}

	po = pa->pa_array[pa->pa_count - 1];
	_XBPS_ASSERT(po != NULL);

	if (stack == NULL) {
		/*
		 * If we are in emergency release mode,
		 * just let caller recurse down.
		 */
		*obj = po;
		return (_XBPS_OBJECT_FREE_FAILED);
	}

	/* Otherwise, try to push the current object on the stack. */
	if (!_xbps_stack_push(stack, pa, NULL, NULL, NULL)) {
		/* Push failed, entering emergency release mode. */
		return (_XBPS_OBJECT_FREE_FAILED);
	}
	/* Object pushed on stack, caller will release it. */
	--pa->pa_count;
	*obj = po;
	return (_XBPS_OBJECT_FREE_RECURSE);
}

static void
_xbps_array_emergency_free(xbps_object_t obj)
{
	xbps_array_t pa = obj;

	_XBPS_ASSERT(pa->pa_count != 0);
	--pa->pa_count;
}

static bool
_xbps_array_externalize(struct _xbps_object_externalize_context *ctx,
			void *v)
{
	xbps_array_t pa = v;
	struct _xbps_object *po;
	xbps_object_iterator_t pi;
	unsigned int i;
	bool rv = false;

	_XBPS_RWLOCK_RDLOCK(pa->pa_rwlock);

	if (pa->pa_count == 0) {
		_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);
		return (_xbps_object_externalize_empty_tag(ctx, "a"));
	}

	/* XXXJRT Hint "count" for the internalize step? */
	if (_xbps_object_externalize_start_tag(ctx, "a") == false ||
	    _xbps_object_externalize_append_char(ctx, '\n') == false)
		goto out;

	pi = _xbps_array_iterator_locked(pa);
	if (pi == NULL)
		goto out;

	ctx->poec_depth++;
	_XBPS_ASSERT(ctx->poec_depth != 0);

	while ((po = _xbps_array_iterator_next_object_locked(pi)) != NULL) {
		if ((*po->po_type->pot_extern)(ctx, po) == false) {
			xbps_object_iterator_release(pi);
			goto out;
		}
	}

	xbps_object_iterator_release(pi);

	ctx->poec_depth--;
	for (i = 0; i < ctx->poec_depth; i++) {
		if (_xbps_object_externalize_append_char(ctx, '\t') == false)
			goto out;
	}
	if (_xbps_object_externalize_end_tag(ctx, "a") == false)
		goto out;

	rv = true;

 out:
	_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);
	return (rv);
}

/* ARGSUSED */
static _xbps_object_equals_rv_t
_xbps_array_equals(xbps_object_t v1, xbps_object_t v2,
    void **stored_pointer1, void **stored_pointer2,
    xbps_object_t *next_obj1, xbps_object_t *next_obj2)
{
	xbps_array_t array1 = v1;
	xbps_array_t array2 = v2;
	uintptr_t idx;
	_xbps_object_equals_rv_t rv = _XBPS_OBJECT_EQUALS_FALSE;

	if (array1 == array2)
		return (_XBPS_OBJECT_EQUALS_TRUE);

	_XBPS_ASSERT(*stored_pointer1 == *stored_pointer2);
	idx = (uintptr_t)*stored_pointer1;

	/* For the first iteration, lock the objects. */
	if (idx == 0) {
		if ((uintptr_t)array1 < (uintptr_t)array2) {
			_XBPS_RWLOCK_RDLOCK(array1->pa_rwlock);
			_XBPS_RWLOCK_RDLOCK(array2->pa_rwlock);
		} else {
			_XBPS_RWLOCK_RDLOCK(array2->pa_rwlock);
			_XBPS_RWLOCK_RDLOCK(array1->pa_rwlock);
		}
	}

	if (array1->pa_count != array2->pa_count)
		goto out;
	if (idx == array1->pa_count) {
		rv = _XBPS_OBJECT_EQUALS_TRUE;
		goto out;
	}
	_XBPS_ASSERT(idx < array1->pa_count);

	*stored_pointer1 = (void *)(idx + 1);
	*stored_pointer2 = (void *)(idx + 1);

	*next_obj1 = array1->pa_array[idx];
	*next_obj2 = array2->pa_array[idx];

	return (_XBPS_OBJECT_EQUALS_RECURSE);

 out:
	_XBPS_RWLOCK_UNLOCK(array1->pa_rwlock);
	_XBPS_RWLOCK_UNLOCK(array2->pa_rwlock);
	return (rv);
}

static void
_xbps_array_equals_finish(xbps_object_t v1, xbps_object_t v2)
{
	_XBPS_RWLOCK_UNLOCK(((xbps_array_t)v1)->pa_rwlock);
	_XBPS_RWLOCK_UNLOCK(((xbps_array_t)v2)->pa_rwlock);
}

static xbps_array_t
_xbps_array_alloc(unsigned int capacity)
{
	xbps_array_t pa;
	xbps_object_t *array;

	if (capacity != 0) {
		array = _XBPS_CALLOC(capacity * sizeof(xbps_object_t),
				     M_XBPS_ARRAY);
		if (array == NULL)
			return (NULL);
	} else
		array = NULL;

	pa = _XBPS_POOL_GET(_xbps_array_pool);
	if (pa != NULL) {
		_xbps_object_init(&pa->pa_obj, &_xbps_object_type_array);
		pa->pa_obj.po_type = &_xbps_object_type_array;

		_XBPS_RWLOCK_INIT(pa->pa_rwlock);
		pa->pa_array = array;
		pa->pa_capacity = capacity;
		pa->pa_count = 0;
		pa->pa_flags = 0;

		pa->pa_version = 0;
	} else if (array != NULL)
		_XBPS_FREE(array, M_XBPS_ARRAY);

	return (pa);
}

static bool
_xbps_array_expand(xbps_array_t pa, unsigned int capacity)
{
	xbps_object_t *array, *oarray;

	/*
	 * Array must be WRITE-LOCKED.
	 */

	oarray = pa->pa_array;

	array = _XBPS_CALLOC(capacity * sizeof(*array), M_XBPS_ARRAY);
	if (array == NULL)
		return (false);
	if (oarray != NULL)
		memcpy(array, oarray, pa->pa_capacity * sizeof(*array));
	pa->pa_array = array;
	pa->pa_capacity = capacity;

	if (oarray != NULL)
		_XBPS_FREE(oarray, M_XBPS_ARRAY);

	return (true);
}

static xbps_object_t
_xbps_array_iterator_next_object_locked(void *v)
{
	struct _xbps_array_iterator *pai = v;
	xbps_array_t pa = pai->pai_base.pi_obj;
	xbps_object_t po = NULL;

	_XBPS_ASSERT(xbps_object_is_array(pa));

	if (pa->pa_version != pai->pai_base.pi_version)
		goto out;	/* array changed during iteration */

	_XBPS_ASSERT(pai->pai_index <= pa->pa_count);

	if (pai->pai_index == pa->pa_count)
		goto out;	/* we've iterated all objects */

	po = pa->pa_array[pai->pai_index];
	pai->pai_index++;

 out:
	return (po);
}

static xbps_object_t
_xbps_array_iterator_next_object(void *v)
{
	struct _xbps_array_iterator *pai = v;
	xbps_array_t pa _XBPS_ARG_UNUSED = pai->pai_base.pi_obj;
	xbps_object_t po;

	_XBPS_ASSERT(xbps_object_is_array(pa));

	_XBPS_RWLOCK_RDLOCK(pa->pa_rwlock);
	po = _xbps_array_iterator_next_object_locked(pai);
	_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);
	return (po);
}

static void
_xbps_array_iterator_reset_locked(void *v)
{
	struct _xbps_array_iterator *pai = v;
	xbps_array_t pa = pai->pai_base.pi_obj;

	_XBPS_ASSERT(xbps_object_is_array(pa));

	pai->pai_index = 0;
	pai->pai_base.pi_version = pa->pa_version;
}

static void
_xbps_array_iterator_reset(void *v)
{
	struct _xbps_array_iterator *pai = v;
	xbps_array_t pa _XBPS_ARG_UNUSED = pai->pai_base.pi_obj;

	_XBPS_ASSERT(xbps_object_is_array(pa));

	_XBPS_RWLOCK_RDLOCK(pa->pa_rwlock);
	_xbps_array_iterator_reset_locked(pai);
	_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);
}

/*
 * xbps_array_create --
 *	Create an empty array.
 */
xbps_array_t
xbps_array_create(void)
{

	return (_xbps_array_alloc(0));
}

/*
 * xbps_array_create_with_capacity --
 *	Create an array with the capacity to store N objects.
 */
xbps_array_t
xbps_array_create_with_capacity(unsigned int capacity)
{

	return (_xbps_array_alloc(capacity));
}

/*
 * xbps_array_copy --
 *	Copy an array.	The new array has an initial capacity equal to
 *	the number of objects stored in the original array.  The new
 *	array contains references to the original array's objects, not
 *	copies of those objects (i.e. a shallow copy).
 */
xbps_array_t
xbps_array_copy(xbps_array_t opa)
{
	xbps_array_t pa;
	xbps_object_t po;
	unsigned int idx;

	if (! xbps_object_is_array(opa))
		return (NULL);

	_XBPS_RWLOCK_RDLOCK(opa->pa_rwlock);

	pa = _xbps_array_alloc(opa->pa_count);
	if (pa != NULL) {
		for (idx = 0; idx < opa->pa_count; idx++) {
			po = opa->pa_array[idx];
			xbps_object_retain(po);
			pa->pa_array[idx] = po;
		}
		pa->pa_count = opa->pa_count;
		pa->pa_flags = opa->pa_flags;
	}
	_XBPS_RWLOCK_UNLOCK(opa->pa_rwlock);
	return (pa);
}

/*
 * xbps_array_copy_mutable --
 *	Like xbps_array_copy(), but the resulting array is mutable.
 */
xbps_array_t
xbps_array_copy_mutable(xbps_array_t opa)
{
	xbps_array_t pa;

	pa = xbps_array_copy(opa);
	if (pa != NULL)
		pa->pa_flags &= ~PA_F_IMMUTABLE;

	return (pa);
}

/*
 * xbps_array_capacity --
 *	Return the capacity of the array.
 */
unsigned int
xbps_array_capacity(xbps_array_t pa)
{
	unsigned int rv;

	if (! xbps_object_is_array(pa))
		return (0);

	_XBPS_RWLOCK_RDLOCK(pa->pa_rwlock);
	rv = pa->pa_capacity;
	_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);

	return (rv);
}

/*
 * xbps_array_count --
 *	Return the number of objects stored in the array.
 */
unsigned int
xbps_array_count(xbps_array_t pa)
{
	unsigned int rv;

	if (! xbps_object_is_array(pa))
		return (0);

	_XBPS_RWLOCK_RDLOCK(pa->pa_rwlock);
	rv = pa->pa_count;
	_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);

	return (rv);
}

/*
 * xbps_array_ensure_capacity --
 *	Ensure that the array has the capacity to store the specified
 *	total number of objects (inluding the objects already stored
 *	in the array).
 */
bool
xbps_array_ensure_capacity(xbps_array_t pa, unsigned int capacity)
{
	bool rv;

	if (! xbps_object_is_array(pa))
		return (false);

	_XBPS_RWLOCK_WRLOCK(pa->pa_rwlock);
	if (capacity > pa->pa_capacity)
		rv = _xbps_array_expand(pa, capacity);
	else
		rv = true;
	_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);

	return (rv);
}

static xbps_object_iterator_t
_xbps_array_iterator_locked(xbps_array_t pa)
{
	struct _xbps_array_iterator *pai;

	if (! xbps_object_is_array(pa))
		return (NULL);

	pai = _XBPS_CALLOC(sizeof(*pai), M_TEMP);
	if (pai == NULL)
		return (NULL);
	pai->pai_base.pi_next_object = _xbps_array_iterator_next_object;
	pai->pai_base.pi_reset = _xbps_array_iterator_reset;
	xbps_object_retain(pa);
	pai->pai_base.pi_obj = pa;
	_xbps_array_iterator_reset_locked(pai);

	return (&pai->pai_base);
}

/*
 * xbps_array_iterator --
 *	Return an iterator for the array.  The array is retained by
 *	the iterator.
 */
xbps_object_iterator_t
xbps_array_iterator(xbps_array_t pa)
{
	xbps_object_iterator_t pi;

	_XBPS_RWLOCK_RDLOCK(pa->pa_rwlock);
	pi = _xbps_array_iterator_locked(pa);
	_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);
	return (pi);
}

/*
 * xbps_array_make_immutable --
 *	Make the array immutable.
 */
void
xbps_array_make_immutable(xbps_array_t pa)
{

	_XBPS_RWLOCK_WRLOCK(pa->pa_rwlock);
	if (xbps_array_is_immutable(pa) == false)
		pa->pa_flags |= PA_F_IMMUTABLE;
	_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);
}

/*
 * xbps_array_mutable --
 *	Returns true if the array is mutable.
 */
bool
xbps_array_mutable(xbps_array_t pa)
{
	bool rv;

	_XBPS_RWLOCK_RDLOCK(pa->pa_rwlock);
	rv = xbps_array_is_immutable(pa) == false;
	_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);

	return (rv);
}

/*
 * xbps_array_get --
 *	Return the object stored at the specified array index.
 */
xbps_object_t
xbps_array_get(xbps_array_t pa, unsigned int idx)
{
	xbps_object_t po = NULL;

	if (! xbps_object_is_array(pa))
		return (NULL);

	_XBPS_RWLOCK_RDLOCK(pa->pa_rwlock);
	if (idx >= pa->pa_count)
		goto out;
	po = pa->pa_array[idx];
	_XBPS_ASSERT(po != NULL);
 out:
	_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);
	return (po);
}

static bool
_xbps_array_add(xbps_array_t pa, xbps_object_t po)
{

	/*
	 * Array must be WRITE-LOCKED.
	 */

	_XBPS_ASSERT(pa->pa_count <= pa->pa_capacity);

	if (xbps_array_is_immutable(pa) ||
	    (pa->pa_count == pa->pa_capacity &&
	    _xbps_array_expand(pa, pa->pa_capacity + EXPAND_STEP) == false))
		return (false);

	xbps_object_retain(po);
	pa->pa_array[pa->pa_count++] = po;
	pa->pa_version++;

	return (true);
}

static bool
_xbps_array_add_first(xbps_array_t pa, xbps_object_t po)
{
	unsigned int cnt;
	/*
	 * Array must be WRITE-LOCKED.
	 */

	_XBPS_ASSERT(pa->pa_count <= pa->pa_capacity);

	if (xbps_array_is_immutable(pa) ||
	    (pa->pa_count == pa->pa_capacity &&
	    _xbps_array_expand(pa, pa->pa_capacity + EXPAND_STEP) == false))
		return false;

	xbps_object_retain(po);
	if (pa->pa_count) {
		cnt = pa->pa_count+1;
		/* move all stored elements to the right */
		while (--cnt) {
			xbps_object_t opo = pa->pa_array[cnt-1];
			_XBPS_ASSERT(opo != NULL);
			xbps_object_retain(opo);
			pa->pa_array[cnt] = opo;
			xbps_object_release(opo);
		}
		/* passed in object is now the first element */
		pa->pa_array[0] = po;
		pa->pa_version++;
		pa->pa_count++;
	} else {
		pa->pa_array[pa->pa_count++] = po;
		pa->pa_version++;
	}
	return true;
}

/*
 * xbps_array_set --
 *	Store a reference to an object at the specified array index.
 *	This method is not allowed to create holes in the array; the
 *	caller must either be setting the object just beyond the existing
 *	count or replacing an already existing object reference.
 */
bool
xbps_array_set(xbps_array_t pa, unsigned int idx, xbps_object_t po)
{
	xbps_object_t opo;
	bool rv = false;

	if (! xbps_object_is_array(pa))
		return (false);

	_XBPS_RWLOCK_WRLOCK(pa->pa_rwlock);

	if (xbps_array_is_immutable(pa))
		goto out;

	if (idx == pa->pa_count) {
		rv = _xbps_array_add(pa, po);
		goto out;
	}

	_XBPS_ASSERT(idx < pa->pa_count);

	opo = pa->pa_array[idx];
	_XBPS_ASSERT(opo != NULL);

	xbps_object_retain(po);
	pa->pa_array[idx] = po;
	pa->pa_version++;

	xbps_object_release(opo);

	rv = true;

 out:
	_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);
	return (rv);
}

/*
 * xbps_array_add --
 *	Add a reference to an object to the specified array, appending
 *	to the end and growing the array's capacity, if necessary.
 */
bool
xbps_array_add(xbps_array_t pa, xbps_object_t po)
{
	bool rv;

	if (! xbps_object_is_array(pa))
		return (false);

	_XBPS_RWLOCK_WRLOCK(pa->pa_rwlock);
	rv = _xbps_array_add(pa, po);
	_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);

	return (rv);
}

/*
 * xbps_array_add_first --
 *	Add a reference to an object to the specified array, inserting it
 *	as first element, moving all objects to the right and growing the
 *	array's capacity, if necessary.
 */
bool
xbps_array_add_first(xbps_array_t pa, xbps_object_t po)
{
	bool rv;

	if (! xbps_object_is_array(pa))
		return (false);

	_XBPS_RWLOCK_WRLOCK(pa->pa_rwlock);
	rv = _xbps_array_add_first(pa, po);
	_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);

	return (rv);
}
/*
 * xbps_array_remove --
 *	Remove the reference to an object from an array at the specified
 *	index.	The array will be compacted following the removal.
 */
void
xbps_array_remove(xbps_array_t pa, unsigned int idx)
{
	xbps_object_t po;

	if (! xbps_object_is_array(pa))
		return;

	_XBPS_RWLOCK_WRLOCK(pa->pa_rwlock);

	_XBPS_ASSERT(idx < pa->pa_count);

	/* XXX Should this be a _XBPS_ASSERT()? */
	if (xbps_array_is_immutable(pa)) {
		_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);
		return;
	}

	po = pa->pa_array[idx];
	_XBPS_ASSERT(po != NULL);

	for (++idx; idx < pa->pa_count; idx++)
		pa->pa_array[idx - 1] = pa->pa_array[idx];
	pa->pa_count--;
	pa->pa_version++;

	_XBPS_RWLOCK_UNLOCK(pa->pa_rwlock);

	xbps_object_release(po);
}

/*
 * xbps_array_equals --
 *	Return true if the two arrays are equivalent.  Note we do a
 *	by-value comparison of the objects in the array.
 */
bool
xbps_array_equals(xbps_array_t array1, xbps_array_t array2)
{
	if (!xbps_object_is_array(array1) || !xbps_object_is_array(array2))
		return (false);

	return (xbps_object_equals(array1, array2));
}

/*
 * xbps_array_externalize --
 *	Externalize an array, return a NUL-terminated buffer
 *	containing the XML-style representation.  The buffer is allocated
 *	with the M_TEMP memory type.
 */
char *
xbps_array_externalize(xbps_array_t pa)
{
	struct _xbps_object_externalize_context *ctx;
	char *cp;

	ctx = _xbps_object_externalize_context_alloc();
	if (ctx == NULL)
		return (NULL);

	if (_xbps_object_externalize_header(ctx) == false ||
	    (*pa->pa_obj.po_type->pot_extern)(ctx, pa) == false ||
	    _xbps_object_externalize_footer(ctx) == false) {
		/* We are responsible for releasing the buffer. */
		_XBPS_FREE(ctx->poec_buf, M_TEMP);
		_xbps_object_externalize_context_free(ctx);
		return (NULL);
	}

	cp = ctx->poec_buf;
	_xbps_object_externalize_context_free(ctx);

	return (cp);
}

/*
 * _xbps_array_internalize --
 *	Parse an <array>...</array> and return the object created from the
 *	external representation.
 */
static bool _xbps_array_internalize_body(xbps_stack_t, xbps_object_t *,
    struct _xbps_object_internalize_context *);

bool
_xbps_array_internalize(xbps_stack_t stack, xbps_object_t *obj,
    struct _xbps_object_internalize_context *ctx)
{
	/* We don't currently understand any attributes. */
	if (ctx->poic_tagattr != NULL)
		return (true);

	*obj = xbps_array_create();
	/*
	 * We are done if the create failed or no child elements exist.
	 */
	if (*obj == NULL || ctx->poic_is_empty_element)
		return (true);

	/*
	 * Opening tag is found, now continue to the first element.
	 */
	return (_xbps_array_internalize_body(stack, obj, ctx));
}

static bool
_xbps_array_internalize_continue(xbps_stack_t stack,
    xbps_object_t *obj,
    struct _xbps_object_internalize_context *ctx,
    void *data, xbps_object_t child)
{
	xbps_array_t array;

	_XBPS_ASSERT(data == NULL);

	if (child == NULL)
		goto bad; /* Element could not be parsed. */

	array = *obj;

	if (xbps_array_add(array, child) == false) {
		xbps_object_release(child);
		goto bad;
	}
	xbps_object_release(child);

	/*
	 * Current element is processed and added, look for next.
	 */
	return (_xbps_array_internalize_body(stack, obj, ctx));

 bad:
	xbps_object_release(*obj);
	*obj = NULL;
	return (true);
}

static bool
_xbps_array_internalize_body(xbps_stack_t stack, xbps_object_t *obj,
    struct _xbps_object_internalize_context *ctx)
{
	xbps_array_t array = *obj;
	const char *tag;

	_XBPS_ASSERT(array != NULL);

	/* Fetch the next tag. */
	if (_xbps_object_internalize_find_tag(ctx, NULL,
				_XBPS_TAG_TYPE_EITHER) == false)
		goto bad;

	/* Check to see if this is the end of the array. */
	if (ctx->poic_xbps) {
		tag = "a";
	} else {
		tag = "array";
	}

	if (_XBPS_TAG_MATCH(ctx, tag) &&
	    ctx->poic_tag_type == _XBPS_TAG_TYPE_END) {
		/* It is, so don't iterate any further. */
		return (true);
	}

	if (_xbps_stack_push(stack, array,
			     _xbps_array_internalize_continue, NULL, NULL))
		return (false);

 bad:
	xbps_object_release(array);
	*obj = NULL;
	return (true);
}

/*
 * xbps_array_internalize --
 *	Create an array by parsing the XML-style representation.
 */
xbps_array_t
xbps_array_internalize(const char *xml)
{
	return _xbps_generic_internalize(xml, "array");
}

/*
 * xbps_array_externalize_to_file --
 *	Externalize an array to the specified file.
 */
bool
xbps_array_externalize_to_file(xbps_array_t array, const char *fname)
{
	char *xml;
	bool rv;
	int save_errno = 0;	/* XXXGCC -Wuninitialized [mips, ...] */

	xml = xbps_array_externalize(array);
	if (xml == NULL)
		return (false);
	rv = _xbps_object_externalize_write_file(fname, xml, strlen(xml), false);
	if (rv == false)
		save_errno = errno;
	_XBPS_FREE(xml, M_TEMP);
	if (rv == false)
		errno = save_errno;

	return (rv);
}

/*
 * xbps_array_internalize_from_file --
 *	Internalize an array from a file.
 */
xbps_array_t
xbps_array_internalize_from_file(const char *fname)
{
	struct _xbps_object_internalize_mapped_file *mf;
	xbps_array_t array;

	mf = _xbps_object_internalize_map_file(fname);
	if (mf == NULL)
		return (NULL);
	array = xbps_array_internalize(mf->poimf_xml);
	_xbps_object_internalize_unmap_file(mf);

	return (array);
}
