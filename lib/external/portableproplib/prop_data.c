/*	$NetBSD: prop_data.c,v 1.17 2020/06/08 21:31:56 thorpej Exp $	*/

/*-
 * Copyright (c) 2006, 2020 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by Jason R. Thorpe.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "prop_object_impl.h"
#include <xbps/xbps_data.h>

#include <errno.h>
#include <limits.h>
#include <stdlib.h>

struct _xbps_data {
	struct _xbps_object	pd_obj;
	union {
		void *		pdu_mutable;
		const void *	pdu_immutable;
	} pd_un;
#define	pd_mutable		pd_un.pdu_mutable
#define	pd_immutable		pd_un.pdu_immutable
	size_t			pd_size;
	int			pd_flags;
};

#define	PD_F_NOCOPY		0x01
#define	PD_F_MUTABLE		0x02

_XBPS_POOL_INIT(_xbps_data_pool, sizeof(struct _xbps_data), "xbpsdata")

static _xbps_object_free_rv_t
		_xbps_data_free(xbps_stack_t, xbps_object_t *);
static bool	_xbps_data_externalize(
				struct _xbps_object_externalize_context *,
				void *);
static _xbps_object_equals_rv_t
		_xbps_data_equals(xbps_object_t, xbps_object_t,
				  void **, void **,
				  xbps_object_t *, xbps_object_t *);

static const struct _xbps_object_type _xbps_object_type_data = {
	.pot_type	=	XBPS_TYPE_DATA,
	.pot_free	=	_xbps_data_free,
	.pot_extern	=	_xbps_data_externalize,
	.pot_equals	=	_xbps_data_equals,
};

#define	xbps_object_is_data(x)		\
	((x) != NULL && (x)->pd_obj.po_type == &_xbps_object_type_data)

/* ARGSUSED */
static _xbps_object_free_rv_t
_xbps_data_free(xbps_stack_t stack, xbps_object_t *obj)
{
	xbps_data_t pd = *obj;

	if ((pd->pd_flags & PD_F_NOCOPY) == 0 && pd->pd_mutable != NULL)
	    	_XBPS_FREE(pd->pd_mutable, M_XBPS_DATA);
	_XBPS_POOL_PUT(_xbps_data_pool, pd);

	return (_XBPS_OBJECT_FREE_DONE);
}

static const char _xbps_data_base64[] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
static const char _xbps_data_pad64 = '=';

static bool
_xbps_data_externalize(struct _xbps_object_externalize_context *ctx, void *v)
{
	xbps_data_t pd = v;
	size_t i, srclen;
	const uint8_t *src;
	uint8_t output[4];
	uint8_t input[3];

	if (pd->pd_size == 0)
		return (_xbps_object_externalize_empty_tag(ctx, "da"));

	if (_xbps_object_externalize_start_tag(ctx, "da") == false)
		return (false);

	for (src = pd->pd_immutable, srclen = pd->pd_size;
	     srclen > 2; srclen -= 3) {
		input[0] = *src++;
		input[1] = *src++;
		input[2] = *src++;

		output[0] = (uint32_t)input[0] >> 2;
		output[1] = ((uint32_t)(input[0] & 0x03) << 4) +
		    ((uint32_t)input[1] >> 4);
		output[2] = ((uint32_t)(input[1] & 0x0f) << 2) +
		    ((uint32_t)input[2] >> 6);
		output[3] = input[2] & 0x3f;
		_XBPS_ASSERT(output[0] < 64);
		_XBPS_ASSERT(output[1] < 64);
		_XBPS_ASSERT(output[2] < 64);
		_XBPS_ASSERT(output[3] < 64);

		if (_xbps_object_externalize_append_char(ctx,
				_xbps_data_base64[output[0]]) == false ||
		    _xbps_object_externalize_append_char(ctx,
		    		_xbps_data_base64[output[1]]) == false ||
		    _xbps_object_externalize_append_char(ctx,
		    		_xbps_data_base64[output[2]]) == false ||
		    _xbps_object_externalize_append_char(ctx,
		    		_xbps_data_base64[output[3]]) == false)
			return (false);
	}

	if (srclen != 0) {
		input[0] = input[1] = input[2] = '\0';
		for (i = 0; i < srclen; i++)
			input[i] = *src++;

		output[0] = (uint32_t)input[0] >> 2;
		output[1] = ((uint32_t)(input[0] & 0x03) << 4) +
		    ((uint32_t)input[1] >> 4);
		output[2] = ((uint32_t)(input[1] & 0x0f) << 2) +
		    ((uint32_t)input[2] >> 6);
		_XBPS_ASSERT(output[0] < 64);
		_XBPS_ASSERT(output[1] < 64);
		_XBPS_ASSERT(output[2] < 64);

		if (_xbps_object_externalize_append_char(ctx,
				_xbps_data_base64[output[0]]) == false ||
		    _xbps_object_externalize_append_char(ctx,
		    		_xbps_data_base64[output[1]]) == false ||
		    _xbps_object_externalize_append_char(ctx,
		    		srclen == 1 ? _xbps_data_pad64
				: _xbps_data_base64[output[2]]) == false ||
		    _xbps_object_externalize_append_char(ctx,
		    		_xbps_data_pad64) == false)
			return (false);
	}

	if (_xbps_object_externalize_end_tag(ctx, "da") == false)
		return (false);
	
	return (true);
}

/* ARGSUSED */
static _xbps_object_equals_rv_t
_xbps_data_equals(xbps_object_t v1, xbps_object_t v2,
    void **stored_pointer1, void **stored_pointer2,
    xbps_object_t *next_obj1, xbps_object_t *next_obj2)
{
	xbps_data_t pd1 = v1;
	xbps_data_t pd2 = v2;

	if (pd1 == pd2)
		return (_XBPS_OBJECT_EQUALS_TRUE);
	if (pd1->pd_size != pd2->pd_size)
		return (_XBPS_OBJECT_EQUALS_FALSE);
	if (pd1->pd_size == 0) {
		_XBPS_ASSERT(pd1->pd_immutable == NULL);
		_XBPS_ASSERT(pd2->pd_immutable == NULL);
		return (_XBPS_OBJECT_EQUALS_TRUE);
	}
	if (memcmp(pd1->pd_immutable, pd2->pd_immutable, pd1->pd_size) == 0)
		return _XBPS_OBJECT_EQUALS_TRUE;
	else
		return _XBPS_OBJECT_EQUALS_FALSE;
}

static xbps_data_t
_xbps_data_alloc(int const flags)
{
	xbps_data_t pd;

	pd = _XBPS_POOL_GET(_xbps_data_pool);
	if (pd != NULL) {
		_xbps_object_init(&pd->pd_obj, &_xbps_object_type_data);

		pd->pd_mutable = NULL;
		pd->pd_size = 0;
		pd->pd_flags = flags;
	}

	return (pd);
}

static xbps_data_t
_xbps_data_instantiate(int const flags, const void * const data,
    size_t const len)
{
	xbps_data_t pd;

	pd = _xbps_data_alloc(flags);
	if (pd != NULL) {
		pd->pd_immutable = data;
		pd->pd_size = len;
	}

	return (pd);
}

_XBPS_DEPRECATED(xbps_data_create_data,
    "this program uses xbps_data_create_data(); all functions "
    "supporting mutable xbps_data objects are deprecated.")
xbps_data_t
xbps_data_create_data(const void *v, size_t size)
{
	xbps_data_t pd;
	void *nv;

	pd = _xbps_data_alloc(PD_F_MUTABLE);
	if (pd != NULL && size != 0) {
		nv = _XBPS_MALLOC(size, M_XBPS_DATA);
		if (nv == NULL) {
			xbps_object_release(pd);
			return (NULL);
		}
		memcpy(nv, v, size);
		pd->pd_mutable = nv;
		pd->pd_size = size;
	}
	return (pd);
}

_XBPS_DEPRECATED(xbps_data_create_data_nocopy,
    "this program uses xbps_data_create_data_nocopy(), "
    "which is deprecated; use xbps_data_create_nocopy() instead.")
xbps_data_t
xbps_data_create_data_nocopy(const void *v, size_t size)
{
	return xbps_data_create_nocopy(v, size);
}

/*
 * xbps_data_create_copy --
 *	Create a data object with a copy of the provided data.
 */
xbps_data_t
xbps_data_create_copy(const void *v, size_t size)
{
	xbps_data_t pd;
	void *nv;

	/* Tolerate the creation of empty data objects. */
	if (v != NULL && size != 0) {
		nv = _XBPS_MALLOC(size, M_XBPS_DATA);
		if (nv == NULL)
			return (NULL);

		memcpy(nv, v, size);
	} else {
		nv = NULL;
		size = 0;
	}

	pd = _xbps_data_instantiate(0, nv, size);
	if (pd == NULL && nv == NULL)
		_XBPS_FREE(nv, M_XBPS_DATA);

	return (pd);
}

/*
 * xbps_data_create_nocopy --
 *	Create a data object using the provided external data reference.
 */
xbps_data_t
xbps_data_create_nocopy(const void *v, size_t size)
{

	/* Tolerate the creation of empty data objects. */
	if (v == NULL || size == 0) {
		v = NULL;
		size = 0;
	}

	return _xbps_data_instantiate(PD_F_NOCOPY, v, size);
}

/*
 * xbps_data_copy --
 *	Copy a data container.  If the original data is external, then
 *	the copy is also references the same external data.
 */
xbps_data_t
xbps_data_copy(xbps_data_t opd)
{
	xbps_data_t pd;

	if (! xbps_object_is_data(opd))
		return (NULL);

	if ((opd->pd_flags & PD_F_NOCOPY) != 0 ||
	    (opd->pd_flags & PD_F_MUTABLE) == 0) {
		/* Just retain and return the original. */
		xbps_object_retain(opd);
		return (opd);
	}

	pd = xbps_data_create_copy(opd->pd_immutable, opd->pd_size);
	if (pd != NULL) {
		/* Preserve deprecated mutability semantics. */
		pd->pd_flags |= PD_F_MUTABLE;
	}

	return (pd);
}

/*
 * xbps_data_size --
 *	Return the size of the data.
 */
size_t
xbps_data_size(xbps_data_t pd)
{

	if (! xbps_object_is_data(pd))
		return (0);

	return (pd->pd_size);
}

/*
 * xbps_data_value --
 *	Returns a pointer to the data object's value.  This pointer
 *	remains valid only as long as the data object.
 */
const void *
xbps_data_value(xbps_data_t pd)
{

	if (! xbps_object_is_data(pd))
		return (0);

	return (pd->pd_immutable);
}

/*
 * xbps_data_copy_value --
 *	Copy the data object's value into the supplied buffer.
 */
bool
xbps_data_copy_value(xbps_data_t pd, void *buf, size_t buflen)
{

	if (! xbps_object_is_data(pd))
		return (false);
	
	if (buf == NULL || buflen < pd->pd_size)
		return (false);

	/* Tolerate empty data objects. */
	if (pd->pd_immutable == NULL || pd->pd_size == 0)
		return (false);

	memcpy(buf, pd->pd_immutable, pd->pd_size);

	return (true);
}

_XBPS_DEPRECATED(xbps_data_data,
    "this program uses xbps_data_data(), "
    "which is deprecated; use xbps_data_copy_value() instead.")
void *
xbps_data_data(xbps_data_t pd)
{
	void *v;

	if (! xbps_object_is_data(pd))
		return (NULL);

	if (pd->pd_size == 0) {
		_XBPS_ASSERT(pd->pd_immutable == NULL);
		return (NULL);
	}

	_XBPS_ASSERT(pd->pd_immutable != NULL);

	v = _XBPS_MALLOC(pd->pd_size, M_TEMP);
	if (v != NULL)
		memcpy(v, pd->pd_immutable, pd->pd_size);
	
	return (v);
}

_XBPS_DEPRECATED(xbps_data_data_nocopy,
    "this program uses xbps_data_data_nocopy(), "
    "which is deprecated; use xbps_data_value() instead.")
const void *
xbps_data_data_nocopy(xbps_data_t pd)
{
	return xbps_data_value(pd);
}

/*
 * xbps_data_equals --
 *	Return true if two data objects are equivalent.
 */
bool
xbps_data_equals(xbps_data_t pd1, xbps_data_t pd2)
{
	if (!xbps_object_is_data(pd1) || !xbps_object_is_data(pd2))
		return (false);

	return (xbps_object_equals(pd1, pd2));
}

/*
 * xbps_data_equals_data --
 *	Return true if the contained data is equivalent to the specified
 *	external data.
 */
bool
xbps_data_equals_data(xbps_data_t pd, const void *v, size_t size)
{

	if (! xbps_object_is_data(pd))
		return (false);

	if (pd->pd_size != size || v == NULL)
		return (false);

	return (memcmp(pd->pd_immutable, v, size) == 0);
}

static bool
_xbps_data_internalize_decode(struct _xbps_object_internalize_context *ctx,
			     uint8_t *target, size_t targsize, size_t *sizep,
			     const char **cpp)
{
	const char *src;
	size_t tarindex;
	int state, ch;
	const char *pos;

	state = 0;
	tarindex = 0;
	src = ctx->poic_cp;

	for (;;) {
		ch = (unsigned char) *src++;
		if (_XBPS_EOF(ch))
			return (false);
		if (_XBPS_ISSPACE(ch))
			continue;
		if (ch == '<') {
			src--;
			break;
		}
		if (ch == _xbps_data_pad64)
			break;

		pos = strchr(_xbps_data_base64, ch);
		if (pos == NULL)
			return (false);

		switch (state) {
		case 0:
			if (target) {
				if (tarindex >= targsize)
					return (false);
				target[tarindex] =
				    (uint8_t)((pos - _xbps_data_base64) << 2);
			}
			state = 1;
			break;

		case 1:
			if (target) {
				if (tarindex + 1 >= targsize)
					return (false);
				target[tarindex] |=
				    (uint32_t)(pos - _xbps_data_base64) >> 4;
				target[tarindex + 1] =
				    (uint8_t)(((pos - _xbps_data_base64) & 0xf)
				        << 4);
			}
			tarindex++;
			state = 2;
			break;

		case 2:
			if (target) {
				if (tarindex + 1 >= targsize)
					return (false);
				target[tarindex] |=
				    (uint32_t)(pos - _xbps_data_base64) >> 2;
				target[tarindex + 1] =
				    (uint8_t)(((pos - _xbps_data_base64)
				        & 0x3) << 6);
			}
			tarindex++;
			state = 3;
			break;

		case 3:
			if (target) {
				if (tarindex >= targsize)
					return (false);
				target[tarindex] |= (uint8_t)
				    (pos - _xbps_data_base64);
			}
			tarindex++;
			state = 0;
			break;

		default:
			_XBPS_ASSERT(/*CONSTCOND*/0);
		}
	}

	/*
	 * We are done decoding the Base64 characters.  Let's see if we
	 * ended up on a byte boundary and/or with unrecognized trailing
	 * characters.
	 */
	if (ch == _xbps_data_pad64) {
		ch = (unsigned char) *src;	/* src already advanced */
		if (_XBPS_EOF(ch))
			return (false);
		switch (state) {
		case 0:		/* Invalid = in first position */
		case 1:		/* Invalid = in second position */
			return (false);

		case 2:		/* Valid, one byte of info */
			/* Skip whitespace */
			for (ch = (unsigned char) *src++;
			     ch != '<'; ch = (unsigned char) *src++) {
				if (_XBPS_EOF(ch))
					return (false);
				if (!_XBPS_ISSPACE(ch))
					break;
			}
			/* Make sure there is another trailing = */
			if (ch != _xbps_data_pad64)
				return (false);
			ch = (unsigned char) *src;
			/* FALLTHROUGH */
		
		case 3:		/* Valid, two bytes of info */
			/*
			 * We know this char is a =.  Is there anything but
			 * whitespace after it?
			 */
			for (ch = (unsigned char) *src++;
			     ch != '<'; ch = (unsigned char) *src++) {
				if (_XBPS_EOF(ch))
					return (false);
				if (!_XBPS_ISSPACE(ch))
					return (false);
			}
			/* back up to '<' */
			src--;
		}
	} else {
		/*
		 * We ended by seeing the end of the Base64 string.  Make
		 * sure there are no partial bytes lying around.
		 */
		if (state != 0)
			return (false);
	}

	_XBPS_ASSERT(*src == '<');
	if (sizep != NULL)
		*sizep = tarindex;
	if (cpp != NULL)
		*cpp = src;

	return (true);
}

/*
 * _xbps_data_internalize --
 *	Parse a <data>...</data> and return the object created from the
 *	external representation.
 */

/* strtoul is used for parsing, enforce. */
typedef int PROP_DATA_ASSERT[/* CONSTCOND */sizeof(size_t) == sizeof(unsigned long) ? 1 : -1];

/* ARGSUSED */
bool
_xbps_data_internalize(xbps_stack_t stack, xbps_object_t *obj,
    struct _xbps_object_internalize_context *ctx)
{
	xbps_data_t data;
	uint8_t *buf;
	size_t len, alen;
	const char *tag;

	/*
	 * We don't accept empty elements.
	 * This actually only checks for the node to be <data/>
	 * (Which actually causes another error if found.)
	 */
	if (ctx->poic_is_empty_element)
		return (true);

	/*
	 * If we got a "size" attribute, get the size of the data blob
	 * from that.  Otherwise, we have to figure it out from the base64.
	 */
	if (ctx->poic_tagattr != NULL) {
		char *cp;

		if (!_XBPS_TAGATTR_MATCH(ctx, "size") ||
		    ctx->poic_tagattrval_len == 0)
			return (true);

#ifndef _KERNEL
		errno = 0;
#endif
		len = strtoul(ctx->poic_tagattrval, &cp, 0);
#ifndef _KERNEL		/* XXX can't check for ERANGE in the kernel */
		if (len == ULONG_MAX && errno == ERANGE)
			return (true);
#endif
		if (cp != ctx->poic_tagattrval + ctx->poic_tagattrval_len)
			return (true);
		_XBPS_ASSERT(*cp == '\"');
	} else if (_xbps_data_internalize_decode(ctx, NULL, 0, &len,
						NULL) == false)
		return (true);

	/*
	 * Always allocate one extra in case we don't land on an even byte
	 * boundary during the decode.
	 */
	buf = _XBPS_MALLOC(len + 1, M_XBPS_DATA);
	if (buf == NULL)
		return (true);
	
	if (_xbps_data_internalize_decode(ctx, buf, len + 1, &alen,
					  &ctx->poic_cp) == false) {
		_XBPS_FREE(buf, M_XBPS_DATA);
		return (true);
	}
	if (alen != len) {
		_XBPS_FREE(buf, M_XBPS_DATA);
		return (true);
	}

	if (ctx->poic_xbps) {
		tag = "da";
	} else {
		tag = "data";
	}

	if (_xbps_object_internalize_find_tag(ctx, tag,
					      _XBPS_TAG_TYPE_END) == false) {
		_XBPS_FREE(buf, M_XBPS_DATA);
		return (true);
	}

	/*
	 * Handle alternate type of empty node.
	 * XML document could contain open/close tags, yet still be empty.
	 */
	if (alen == 0) {
		_XBPS_FREE(buf, M_XBPS_DATA);
		buf = NULL;
	}

	data = _xbps_data_instantiate(0, buf, len);
	if (data == NULL && buf != NULL)
		_XBPS_FREE(buf, M_XBPS_DATA);

	*obj = data;
	return (true);
}
