/*	$NetBSD: prop_number.c,v 1.33 2020/06/06 22:23:31 thorpej Exp $	*/

/*-
 * Copyright (c) 2006, 2020 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by Jason R. Thorpe.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "prop_object_impl.h"
#include <xbps/xbps_number.h>
#include <rbtree.h>
#include <errno.h>
#include <limits.h>
#include <stdlib.h>

struct _xbps_number_value {
	union {
		int64_t  pnu_signed;
		uint64_t pnu_unsigned;
	} pnv_un;
#define	pnv_signed	pnv_un.pnu_signed
#define	pnv_unsigned	pnv_un.pnu_unsigned
	unsigned int	pnv_is_unsigned	:1,
					:31;
};

struct _xbps_number {
	struct _xbps_object	pn_obj;
	struct rb_node		pn_link;
	struct _xbps_number_value pn_value;
};

_XBPS_POOL_INIT(_xbps_number_pool, sizeof(struct _xbps_number), "xbpsnmbr")

static _xbps_object_free_rv_t
		_xbps_number_free(xbps_stack_t, xbps_object_t *);
static bool	_xbps_number_externalize(
				struct _xbps_object_externalize_context *,
				void *);
static _xbps_object_equals_rv_t
		_xbps_number_equals(xbps_object_t, xbps_object_t,
				    void **, void **,
				    xbps_object_t *, xbps_object_t *);

static void _xbps_number_lock(void);
static void _xbps_number_unlock(void);

static const struct _xbps_object_type _xbps_object_type_number = {
	.pot_type	=	XBPS_TYPE_NUMBER,
	.pot_free	=	_xbps_number_free,
	.pot_extern	=	_xbps_number_externalize,
	.pot_equals	=	_xbps_number_equals,
	.pot_lock       =       _xbps_number_lock,
	.pot_unlock     =    	_xbps_number_unlock,
};

#define	xbps_object_is_number(x)	\
	((x) != NULL && (x)->pn_obj.po_type == &_xbps_object_type_number)

/*
 * Number objects are immutable, and we are likely to have many number
 * objects that have the same value.  So, to save memory, we unique'ify
 * numbers so we only have one copy of each.
 */

static int
_xbps_number_compare_values(const struct _xbps_number_value *pnv1,
			    const struct _xbps_number_value *pnv2)
{

	/* Signed numbers are sorted before unsigned numbers. */

	if (pnv1->pnv_is_unsigned) {
		if (! pnv2->pnv_is_unsigned)
			return (1);
		if (pnv1->pnv_unsigned < pnv2->pnv_unsigned)
			return (-1);
		if (pnv1->pnv_unsigned > pnv2->pnv_unsigned)
			return (1);
		return (0);
	}

	if (pnv2->pnv_is_unsigned)
		return (-1);
	if (pnv1->pnv_signed < pnv2->pnv_signed)
		return (-1);
	if (pnv1->pnv_signed > pnv2->pnv_signed)
		return (1);
	return (0);
}

static int
/*ARGSUSED*/
_xbps_number_rb_compare_nodes(void *ctx _XBPS_ARG_UNUSED,
			      const void *n1, const void *n2)
{
	const struct _xbps_number *pn1 = n1;
	const struct _xbps_number *pn2 = n2;

	return _xbps_number_compare_values(&pn1->pn_value, &pn2->pn_value);
}

static int
/*ARGSUSED*/
_xbps_number_rb_compare_key(void *ctx _XBPS_ARG_UNUSED,
			    const void *n, const void *v)
{
	const struct _xbps_number *pn = n;
	const struct _xbps_number_value *pnv = v;

	return _xbps_number_compare_values(&pn->pn_value, pnv);
}

static const rb_tree_ops_t _xbps_number_rb_tree_ops = {
	.rbto_compare_nodes = _xbps_number_rb_compare_nodes,
	.rbto_compare_key = _xbps_number_rb_compare_key,
	.rbto_node_offset = offsetof(struct _xbps_number, pn_link),
	.rbto_context = NULL
};

static struct rb_tree _xbps_number_tree;
_XBPS_MUTEX_DECL_STATIC(_xbps_number_tree_mutex)

/* ARGSUSED */
static _xbps_object_free_rv_t
_xbps_number_free(xbps_stack_t stack, xbps_object_t *obj)
{
	xbps_number_t pn = *obj;

	rb_tree_remove_node(&_xbps_number_tree, pn);

	_XBPS_POOL_PUT(_xbps_number_pool, pn);

	return (_XBPS_OBJECT_FREE_DONE);
}

_XBPS_ONCE_DECL(_xbps_number_init_once)

static int
_xbps_number_init(void)
{

	_XBPS_MUTEX_INIT(_xbps_number_tree_mutex);
	rb_tree_init(&_xbps_number_tree, &_xbps_number_rb_tree_ops);
	return 0;
}

static void 
_xbps_number_lock(void)
{
	/* XXX: init necessary? */
	_XBPS_ONCE_RUN(_xbps_number_init_once, _xbps_number_init);
	_XBPS_MUTEX_LOCK(_xbps_number_tree_mutex);
}

static void
_xbps_number_unlock(void)
{
	_XBPS_MUTEX_UNLOCK(_xbps_number_tree_mutex);
}
	
static bool
_xbps_number_externalize(struct _xbps_object_externalize_context *ctx,
			 void *v)
{
	xbps_number_t pn = v;
	char tmpstr[32];

	/*
	 * For unsigned numbers, we output in hex.  For signed numbers,
	 * we output in decimal.
	 */
	if (pn->pn_value.pnv_is_unsigned)
		snprintf(tmpstr, sizeof(tmpstr), "0x%" PRIx64,
		    pn->pn_value.pnv_unsigned);
	else
		snprintf(tmpstr, sizeof(tmpstr), "%" PRIi64,
		    pn->pn_value.pnv_signed);

	if (_xbps_object_externalize_start_tag(ctx, "i") == false ||
	    _xbps_object_externalize_append_cstring(ctx, tmpstr) == false ||
	    _xbps_object_externalize_end_tag(ctx, "i") == false)
		return (false);
	
	return (true);
}

/* ARGSUSED */
static _xbps_object_equals_rv_t
_xbps_number_equals(xbps_object_t v1, xbps_object_t v2,
    void **stored_pointer1, void **stored_pointer2,
    xbps_object_t *next_obj1, xbps_object_t *next_obj2)
{
	xbps_number_t num1 = v1;
	xbps_number_t num2 = v2;

	/*
	 * There is only ever one copy of a number object at any given
	 * time, so we can reduce this to a simple pointer equality check
	 * in the common case.
	 */
	if (num1 == num2)
		return (_XBPS_OBJECT_EQUALS_TRUE);

	/*
	 * If the numbers are the same signed-ness, then we know they
	 * cannot be equal because they would have had pointer equality.
	 */
	if (num1->pn_value.pnv_is_unsigned == num2->pn_value.pnv_is_unsigned)
		return (_XBPS_OBJECT_EQUALS_FALSE);

	/*
	 * We now have one signed value and one unsigned value.  We can
	 * compare them iff:
	 *	- The unsigned value is not larger than the signed value
	 *	  can represent.
	 *	- The signed value is not smaller than the unsigned value
	 *	  can represent.
	 */
	if (num1->pn_value.pnv_is_unsigned) {
		/*
		 * num1 is unsigned and num2 is signed.
		 */
		if (num1->pn_value.pnv_unsigned > INTMAX_MAX)
			return (_XBPS_OBJECT_EQUALS_FALSE);
		if (num2->pn_value.pnv_signed < 0)
			return (_XBPS_OBJECT_EQUALS_FALSE);
	} else {
		/*
		 * num1 is signed and num2 is unsigned.
		 */
		if (num1->pn_value.pnv_signed < 0)
			return (_XBPS_OBJECT_EQUALS_FALSE);
		if (num2->pn_value.pnv_unsigned > INTMAX_MAX)
			return (_XBPS_OBJECT_EQUALS_FALSE);
	}

	if (num1->pn_value.pnv_signed == num2->pn_value.pnv_signed)
		return _XBPS_OBJECT_EQUALS_TRUE;
	else
		return _XBPS_OBJECT_EQUALS_FALSE;
}

static xbps_number_t
_xbps_number_alloc(const struct _xbps_number_value *pnv)
{
	xbps_number_t opn, pn, rpn;

	_XBPS_ONCE_RUN(_xbps_number_init_once, _xbps_number_init);

	/*
	 * Check to see if this already exists in the tree.  If it does,
	 * we just retain it and return it.
	 */
	_XBPS_MUTEX_LOCK(_xbps_number_tree_mutex);
	opn = rb_tree_find_node(&_xbps_number_tree, pnv);
	if (opn != NULL) {
		xbps_object_retain(opn);
		_XBPS_MUTEX_UNLOCK(_xbps_number_tree_mutex);
		return (opn);
	}
	_XBPS_MUTEX_UNLOCK(_xbps_number_tree_mutex);

	/*
	 * Not in the tree.  Create it now.
	 */

	pn = _XBPS_POOL_GET(_xbps_number_pool);
	if (pn == NULL)
		return (NULL);

	_xbps_object_init(&pn->pn_obj, &_xbps_object_type_number);

	pn->pn_value = *pnv;

	/*
	 * We dropped the mutex when we allocated the new object, so
	 * we have to check again if it is in the tree.
	 */
	_XBPS_MUTEX_LOCK(_xbps_number_tree_mutex);
	opn = rb_tree_find_node(&_xbps_number_tree, pnv);
	if (opn != NULL) {
		xbps_object_retain(opn);
		_XBPS_MUTEX_UNLOCK(_xbps_number_tree_mutex);
		_XBPS_POOL_PUT(_xbps_number_pool, pn);
		return (opn);
	}
	rpn = rb_tree_insert_node(&_xbps_number_tree, pn);
	_XBPS_ASSERT(rpn == pn);
	_XBPS_MUTEX_UNLOCK(_xbps_number_tree_mutex);
	return (rpn);
}

/*
 * xbps_number_create_signed --
 *	Create a xbps_number_t and initialize it with the
 *	provided signed value.
 */
xbps_number_t
xbps_number_create_signed(intmax_t val)
{
	struct _xbps_number_value pnv;

	memset(&pnv, 0, sizeof(pnv));
	pnv.pnv_signed = val;
	pnv.pnv_is_unsigned = false;

	return (_xbps_number_alloc(&pnv));
}

_XBPS_DEPRECATED(xbps_number_create_integer,
    "this program uses xbps_number_create_integer(), "
    "which is deprecated; use xbps_number_create_signed() instead.")
xbps_number_t
xbps_number_create_integer(int64_t val)
{
	return xbps_number_create_signed(val);
}

/*
 * xbps_number_create_unsigned --
 *	Create a xbps_number_t and initialize it with the
 *	provided unsigned value.
 */
xbps_number_t
xbps_number_create_unsigned(uintmax_t val)
{
	struct _xbps_number_value pnv;

	memset(&pnv, 0, sizeof(pnv));
	pnv.pnv_unsigned = val;
	pnv.pnv_is_unsigned = true;

	return (_xbps_number_alloc(&pnv));
}

_XBPS_DEPRECATED(xbps_number_create_unsigned_integer,
    "this program uses xbps_number_create_unsigned_integer(), "
    "which is deprecated; use xbps_number_create_unsigned() instead.")
xbps_number_t
xbps_number_create_unsigned_integer(uint64_t val)
{
	return xbps_number_create_unsigned(val);
}

/*
 * xbps_number_copy --
 *	Copy a xbps_number_t.
 */
xbps_number_t
xbps_number_copy(xbps_number_t opn)
{

	if (! xbps_object_is_number(opn))
		return (NULL);

	/*
	 * Because we only ever allocate one object for any given
	 * value, this can be reduced to a simple retain operation.
	 */
	xbps_object_retain(opn);
	return (opn);
}

/*
 * xbps_number_unsigned --
 *	Returns true if the xbps_number_t has an unsigned value.
 */
bool
xbps_number_unsigned(xbps_number_t pn)
{

	return (pn->pn_value.pnv_is_unsigned);
}

/*
 * xbps_number_size --
 *	Return the size, in bits, required to hold the value of
 *	the specified number.
 */
int
xbps_number_size(xbps_number_t pn)
{
	struct _xbps_number_value *pnv;

	if (! xbps_object_is_number(pn))
		return (0);

	pnv = &pn->pn_value;

	if (pnv->pnv_is_unsigned) {
		if (pnv->pnv_unsigned > UINT32_MAX)
			return (64);
		if (pnv->pnv_unsigned > UINT16_MAX)
			return (32);
		if (pnv->pnv_unsigned > UINT8_MAX)
			return (16);
		return (8);
	}

	if (pnv->pnv_signed > INT32_MAX || pnv->pnv_signed < INT32_MIN)
	    	return (64);
	if (pnv->pnv_signed > INT16_MAX || pnv->pnv_signed < INT16_MIN)
		return (32);
	if (pnv->pnv_signed > INT8_MAX  || pnv->pnv_signed < INT8_MIN)
		return (16);
	return (8);
}

/*
 * xbps_number_signed_value --
 *	Get the signed value of a xbps_number_t.
 */
intmax_t
xbps_number_signed_value(xbps_number_t pn)
{

	/*
	 * XXX Impossible to distinguish between "not a xbps_number_t"
	 * XXX and "xbps_number_t has a value of 0".
	 */
	if (! xbps_object_is_number(pn))
		return (0);

	return (pn->pn_value.pnv_signed);
}

_XBPS_DEPRECATED(xbps_number_integer_value,
    "this program uses xbps_number_integer_value(), "
    "which is deprecated; use xbps_number_signed_value() instead.")
int64_t
xbps_number_integer_value(xbps_number_t pn)
{
	return xbps_number_signed_value(pn);
}

/*
 * xbps_number_unsigned_value --
 *	Get the unsigned value of a xbps_number_t.
 */
uintmax_t
xbps_number_unsigned_value(xbps_number_t pn)
{

	/*
	 * XXX Impossible to distinguish between "not a xbps_number_t"
	 * XXX and "xbps_number_t has a value of 0".
	 */
	if (! xbps_object_is_number(pn))
		return (0);

	return (pn->pn_value.pnv_unsigned);
}

_XBPS_DEPRECATED(xbps_number_unsigned_integer_value,
    "this program uses xbps_number_unsigned_integer_value(), "
    "which is deprecated; use xbps_number_unsigned_value() instead.")
uint64_t
xbps_number_unsigned_integer_value(xbps_number_t pn)
{
	return xbps_number_unsigned_value(pn);
}

/*
 * xbps_number_[...]_value --
 *	Retrieve the bounds-checked value as the specified type.
 *	Returns true if successful.
 */
#define	TEMPLATE(name, typ, minv, maxv)					\
bool									\
xbps_number_ ## name ## _value(xbps_number_t pn, typ * const valp)	\
{									\
									\
	if (! xbps_object_is_number(pn))				\
		return (false);						\
									\
	if (pn->pn_value.pnv_is_unsigned) {				\
		if (pn->pn_value.pnv_unsigned > (maxv))			\
			return (false);					\
		*valp = (typ) pn->pn_value.pnv_unsigned;		\
	} else {							\
		if ((pn->pn_value.pnv_signed > 0 &&			\
		     (uintmax_t)pn->pn_value.pnv_signed > (maxv)) ||	\
		    pn->pn_value.pnv_signed < (minv))			\
			return (false);					\
		*valp = (typ) pn->pn_value.pnv_signed;			\
	}								\
									\
	return (true);							\
}
TEMPLATE(schar,    signed char, SCHAR_MIN,  SCHAR_MAX)
TEMPLATE(short,    short,       SHRT_MIN,   SHRT_MAX)
TEMPLATE(int,      int,         INT_MIN,    INT_MAX)
TEMPLATE(long,     long,        LONG_MIN,   LONG_MAX)
TEMPLATE(longlong, long long,   LLONG_MIN,  LLONG_MAX)
TEMPLATE(intptr,   intptr_t,    INTPTR_MIN, INTPTR_MAX)
TEMPLATE(int8,     int8_t,      INT8_MIN,   INT8_MAX)
TEMPLATE(int16,    int16_t,     INT16_MIN,  INT16_MAX)
TEMPLATE(int32,    int32_t,     INT32_MIN,  INT32_MAX)
TEMPLATE(int64,    int64_t,     INT64_MIN,  INT64_MAX)

TEMPLATE(uchar,     unsigned char,      0, UCHAR_MAX)
TEMPLATE(ushort,    unsigned short,     0, USHRT_MAX)
TEMPLATE(uint,      unsigned int,       0, UINT_MAX)
TEMPLATE(ulong,     unsigned long,      0, ULONG_MAX)
TEMPLATE(ulonglong, unsigned long long, 0, ULLONG_MAX)
TEMPLATE(uintptr,   uintptr_t,          0, UINTPTR_MAX)
TEMPLATE(uint8,     uint8_t,            0, UINT8_MAX)
TEMPLATE(uint16,    uint16_t,           0, UINT16_MAX)
TEMPLATE(uint32,    uint32_t,           0, UINT32_MAX)
TEMPLATE(uint64,    uint64_t,           0, UINT64_MAX)

#undef TEMPLATE

/*
 * xbps_number_equals --
 *	Return true if two numbers are equivalent.
 */
bool
xbps_number_equals(xbps_number_t num1, xbps_number_t num2)
{
	if (!xbps_object_is_number(num1) || !xbps_object_is_number(num2))
		return (false);

	return (xbps_object_equals(num1, num2));
}

/*
 * xbps_number_equals_signed --
 *	Return true if the number is equivalent to the specified signed
 *	value.
 */
bool
xbps_number_equals_signed(xbps_number_t pn, intmax_t val)
{

	if (! xbps_object_is_number(pn))
		return (false);

	if (pn->pn_value.pnv_is_unsigned &&
	    (pn->pn_value.pnv_unsigned > INTMAX_MAX || val < 0))
		return (false);
	
	return (pn->pn_value.pnv_signed == val);
}

_XBPS_DEPRECATED(xbps_number_equals_integer,
    "this program uses xbps_number_equals_integer(), "
    "which is deprecated; use xbps_number_equals_signed() instead.")
bool
xbps_number_equals_integer(xbps_number_t pn, int64_t val)
{
	return xbps_number_equals_signed(pn, val);
}

/*
 * xbps_number_equals_unsigned --
 *	Return true if the number is equivalent to the specified
 *	unsigned value.
 */
bool
xbps_number_equals_unsigned(xbps_number_t pn, uintmax_t val)
{

	if (! xbps_object_is_number(pn))
		return (false);
	
	if (! pn->pn_value.pnv_is_unsigned &&
	    (pn->pn_value.pnv_signed < 0 || val > INT64_MAX))
		return (false);
	
	return (pn->pn_value.pnv_unsigned == val);
}

_XBPS_DEPRECATED(xbps_number_equals_unsigned_integer,
    "this program uses xbps_number_equals_unsigned_integer(), "
    "which is deprecated; use xbps_number_equals_unsigned() instead.")
bool
xbps_number_equals_unsigned_integer(xbps_number_t pn, uint64_t val)
{
	return xbps_number_equals_unsigned(pn, val);
}

static bool
_xbps_number_internalize_unsigned(struct _xbps_object_internalize_context *ctx,
				  struct _xbps_number_value *pnv)
{
	char *cp;

	_XBPS_ASSERT(/*CONSTCOND*/sizeof(unsigned long long) ==
		     sizeof(uint64_t));

	errno = 0;
	pnv->pnv_unsigned = (uint64_t) strtoull(ctx->poic_cp, &cp, 0);
	if (pnv->pnv_unsigned == UINT64_MAX && errno == ERANGE)
		return (false);

	pnv->pnv_is_unsigned = true;
	ctx->poic_cp = cp;

	return (true);
}

static bool
_xbps_number_internalize_signed(struct _xbps_object_internalize_context *ctx,
				struct _xbps_number_value *pnv)
{
	char *cp;

	_XBPS_ASSERT(/*CONSTCOND*/sizeof(long long) == sizeof(int64_t));

	errno = 0;
	pnv->pnv_signed = (int64_t) strtoll(ctx->poic_cp, &cp, 0);
	if ((pnv->pnv_signed == INT64_MAX || pnv->pnv_signed == INT64_MIN) &&
	    errno == ERANGE)
	    	return (false);

	pnv->pnv_is_unsigned = false;
	ctx->poic_cp = cp;

	return (true);
}

/*
 * _xbps_number_internalize --
 *	Parse a <number>...</number> and return the object created from
 *	the external representation.
 */
/* ARGSUSED */
bool
_xbps_number_internalize(xbps_stack_t stack, xbps_object_t *obj,
    struct _xbps_object_internalize_context *ctx)
{
	struct _xbps_number_value pnv;
	const char *tag;

	memset(&pnv, 0, sizeof(pnv));

	/* No attributes, no empty elements. */
	if (ctx->poic_tagattr != NULL || ctx->poic_is_empty_element)
		return (true);

	/*
	 * If the first character is '-', then we treat as signed.
	 * If the first two characters are "0x" (i.e. the number is
	 * in hex), then we treat as unsigned.  Otherwise, we try
	 * signed first, and if that fails (presumably due to ERANGE),
	 * then we switch to unsigned.
	 */
	if (ctx->poic_cp[0] == '-') {
		if (_xbps_number_internalize_signed(ctx, &pnv) == false)
			return (true);
	} else if (ctx->poic_cp[0] == '0' && ctx->poic_cp[1] == 'x') {
		if (_xbps_number_internalize_unsigned(ctx, &pnv) == false)
			return (true);
	} else {
		if (_xbps_number_internalize_signed(ctx, &pnv) == false &&
		    _xbps_number_internalize_unsigned(ctx, &pnv) == false)
		    	return (true);
	}

	if (ctx->poic_xbps) {
		tag = "i";
	} else {
		tag = "integer";
	}

	if (_xbps_object_internalize_find_tag(ctx, tag,
					      _XBPS_TAG_TYPE_END) == false) {
		return (true);
	}

	*obj = _xbps_number_alloc(&pnv);
	return (true);
}
