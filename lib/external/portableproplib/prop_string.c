/*	$NetBSD: prop_string.c,v 1.15 2020/06/20 00:16:50 christos Exp $	*/

/*-
 * Copyright (c) 2006, 2020 The NetBSD Foundation, Inc.
 * All rights reserved.
 *
 * This code is derived from software contributed to The NetBSD Foundation
 * by Jason R. Thorpe.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE NETBSD FOUNDATION, INC. AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE FOUNDATION OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include "prop_object_impl.h"
#include <xbps/xbps_string.h>

#include <rbtree.h>
#include <stdarg.h>

struct _xbps_string {
	struct _xbps_object	ps_obj;
	union {
		char *		psu_mutable;
		const char *	psu_immutable;
	} ps_un;
#define	ps_mutable		ps_un.psu_mutable
#define	ps_immutable		ps_un.psu_immutable
	size_t			ps_size;	/* not including \0 */
	struct rb_node		ps_link;
	int			ps_flags;
};

#define	PS_F_NOCOPY		0x01
#define	PS_F_MUTABLE		0x02

_XBPS_POOL_INIT(_xbps_string_pool, sizeof(struct _xbps_string), "xbpsstng")

static _xbps_object_free_rv_t
		_xbps_string_free(xbps_stack_t, xbps_object_t *);
static bool	_xbps_string_externalize(
				struct _xbps_object_externalize_context *,
				void *);
static _xbps_object_equals_rv_t
		_xbps_string_equals(xbps_object_t, xbps_object_t,
				    void **, void **,
				    xbps_object_t *, xbps_object_t *);

static const struct _xbps_object_type _xbps_object_type_string = {
	.pot_type	=	XBPS_TYPE_STRING,
	.pot_free	=	_xbps_string_free,
	.pot_extern	=	_xbps_string_externalize,
	.pot_equals	=	_xbps_string_equals,
};

#define	xbps_object_is_string(x)	\
	((x) != NULL && (x)->ps_obj.po_type == &_xbps_object_type_string)
#define	xbps_string_contents(x)  ((x)->ps_immutable ? (x)->ps_immutable : "")

/*
 * In order to reduce memory usage, all immutable string objects are
 * de-duplicated.
 */

static int
/*ARGSUSED*/
_xbps_string_rb_compare_nodes(void *ctx _XBPS_ARG_UNUSED,
			      const void *n1, const void *n2)
{
	const struct _xbps_string * const ps1 = n1;
	const struct _xbps_string * const ps2 = n2;

	_XBPS_ASSERT(ps1->ps_immutable != NULL);
	_XBPS_ASSERT(ps2->ps_immutable != NULL);

	return strcmp(ps1->ps_immutable, ps2->ps_immutable);
}

static int
/*ARGSUSED*/
_xbps_string_rb_compare_key(void *ctx _XBPS_ARG_UNUSED,
			    const void *n, const void *v)
{
	const struct _xbps_string * const ps = n;
	const char * const cp = v;

	_XBPS_ASSERT(ps->ps_immutable != NULL);

	return strcmp(ps->ps_immutable, cp);
}

static const rb_tree_ops_t _xbps_string_rb_tree_ops = {
	.rbto_compare_nodes = _xbps_string_rb_compare_nodes,
	.rbto_compare_key = _xbps_string_rb_compare_key,
	.rbto_node_offset = offsetof(struct _xbps_string, ps_link),
	.rbto_context = NULL
};

static struct rb_tree _xbps_string_tree;

_XBPS_ONCE_DECL(_xbps_string_init_once)
_XBPS_MUTEX_DECL_STATIC(_xbps_string_tree_mutex)

static int
_xbps_string_init(void)
{

	_XBPS_MUTEX_INIT(_xbps_string_tree_mutex);
	rb_tree_init(&_xbps_string_tree,
		     &_xbps_string_rb_tree_ops);
	
	return 0;
}

/* ARGSUSED */
static _xbps_object_free_rv_t
_xbps_string_free(xbps_stack_t stack, xbps_object_t *obj)
{
	xbps_string_t ps = *obj;

	if ((ps->ps_flags & PS_F_MUTABLE) == 0) {
		_XBPS_MUTEX_LOCK(_xbps_string_tree_mutex);
		/*
		 * Double-check the retain count now that we've
		 * acqured the tree lock; holding this lock prevents
		 * new retains from coming in by finding it in the
		 * tree.
		 */
		if (_XBPS_ATOMIC_LOAD(&ps->ps_obj.po_refcnt) == 0)
			rb_tree_remove_node(&_xbps_string_tree, ps);
		else
			ps = NULL;
		_XBPS_MUTEX_UNLOCK(_xbps_string_tree_mutex);

		if (ps == NULL)
			return (_XBPS_OBJECT_FREE_DONE);
	}

	if ((ps->ps_flags & PS_F_NOCOPY) == 0 && ps->ps_mutable != NULL)
	    	_XBPS_FREE(ps->ps_mutable, M_XBPS_STRING);
	_XBPS_POOL_PUT(_xbps_string_pool, ps);

	return (_XBPS_OBJECT_FREE_DONE);
}

static bool
_xbps_string_externalize(struct _xbps_object_externalize_context *ctx,
			 void *v)
{
	xbps_string_t ps = v;

	if (ps->ps_size == 0)
		return (_xbps_object_externalize_empty_tag(ctx, "s"));

	if (_xbps_object_externalize_start_tag(ctx, "s") == false ||
	    _xbps_object_externalize_append_encoded_cstring(ctx,
	    					ps->ps_immutable) == false ||
	    _xbps_object_externalize_end_tag(ctx, "s") == false)
		return (false);
	
	return (true);
}

/* ARGSUSED */
static _xbps_object_equals_rv_t
_xbps_string_equals(xbps_object_t v1, xbps_object_t v2,
    void **stored_pointer1, void **stored_pointer2,
    xbps_object_t *next_obj1, xbps_object_t *next_obj2)
{
	xbps_string_t str1 = v1;
	xbps_string_t str2 = v2;

	if (str1 == str2)
		return (_XBPS_OBJECT_EQUALS_TRUE);
	if (str1->ps_size != str2->ps_size)
		return (_XBPS_OBJECT_EQUALS_FALSE);
	if (strcmp(xbps_string_contents(str1), xbps_string_contents(str2)))
		return (_XBPS_OBJECT_EQUALS_FALSE);
	else
		return (_XBPS_OBJECT_EQUALS_TRUE);
}

static xbps_string_t
_xbps_string_alloc(int const flags)
{
	xbps_string_t ps;

	ps = _XBPS_POOL_GET(_xbps_string_pool);
	if (ps != NULL) {
		_xbps_object_init(&ps->ps_obj, &_xbps_object_type_string);

		ps->ps_mutable = NULL;
		ps->ps_size = 0;
		ps->ps_flags = flags;
	}

	return (ps);
}

static xbps_string_t
_xbps_string_instantiate(int const flags, const char * const str,
    size_t const len)
{
	xbps_string_t ps;

	_XBPS_ONCE_RUN(_xbps_string_init_once, _xbps_string_init);

	ps = _xbps_string_alloc(flags);
	if (ps != NULL) {
		ps->ps_immutable = str;
		ps->ps_size = len;

		if ((flags & PS_F_MUTABLE) == 0) {
			xbps_string_t ops;

			_XBPS_MUTEX_LOCK(_xbps_string_tree_mutex);
			ops = rb_tree_insert_node(&_xbps_string_tree, ps);
			if (ops != ps) {
				/*
				 * Equivalent string object already exist;
				 * free the new one and return a reference
				 * to the existing object.
				 */
				xbps_object_retain(ops);
				_XBPS_MUTEX_UNLOCK(_xbps_string_tree_mutex);
				_XBPS_POOL_PUT(_xbps_string_pool, ps);
				ps = ops;
			} else {
				_XBPS_MUTEX_UNLOCK(_xbps_string_tree_mutex);
			}
		}
	}

	return (ps);
}

_XBPS_DEPRECATED(xbps_string_create,
    "this program uses xbps_string_create(); all functions "
    "supporting mutable xbps_strings are deprecated.")
xbps_string_t
xbps_string_create(void)
{

	return (_xbps_string_alloc(PS_F_MUTABLE));
}

_XBPS_DEPRECATED(xbps_string_create_cstring,
    "this program uses xbps_string_create_cstring(); all functions "
    "supporting mutable xbps_strings are deprecated.")
xbps_string_t
xbps_string_create_cstring(const char *str)
{
	xbps_string_t ps;
	char *cp;
	size_t len;

	_XBPS_ASSERT(str != NULL);

	ps = _xbps_string_alloc(PS_F_MUTABLE);
	if (ps != NULL) {
		len = strlen(str);
		cp = _XBPS_MALLOC(len + 1, M_XBPS_STRING);
		if (cp == NULL) {
			xbps_object_release(ps);
			return (NULL);
		}
		strcpy(cp, str);
		ps->ps_mutable = cp;
		ps->ps_size = len;
	}
	return (ps);
}

_XBPS_DEPRECATED(xbps_string_create_cstring_nocopy,
    "this program uses xbps_string_create_cstring_nocopy(), "
    "which is deprecated; use xbps_string_create_nocopy() instead.")
xbps_string_t
xbps_string_create_cstring_nocopy(const char *str)
{
	return xbps_string_create_nocopy(str);
}

/*
 * xbps_string_create_format --
 *	Create a string object using the provided format string.
 */
xbps_string_t __printflike(1, 2)
xbps_string_create_format(const char *fmt, ...)
{
	xbps_string_t ps;
	char *str = NULL;
	int len;
	size_t nlen;
	va_list ap;

	_XBPS_ASSERT(fmt != NULL);

	va_start(ap, fmt);
	len = vsnprintf(NULL, 0, fmt, ap);
	va_end(ap);

	if (len < 0)
		return (NULL);
	nlen = len + 1;

	str = _XBPS_MALLOC(nlen, M_XBPS_STRING);
	if (str == NULL)
		return (NULL);

	va_start(ap, fmt);
	vsnprintf(str, nlen, fmt, ap);
	va_end(ap);

	ps = _xbps_string_instantiate(0, str, (size_t)len);
	if (ps == NULL)
		_XBPS_FREE(str, M_XBPS_STRING);

	return (ps);
}

/*
 * xbps_string_create_copy --
 *	Create a string object by coping the provided constant string.
 */
xbps_string_t
xbps_string_create_copy(const char *str)
{
	return xbps_string_create_format("%s", str);
}

/*
 * xbps_string_create_nocopy --
 *	Create a string object using the provided external constant
 *	string.
 */
xbps_string_t
xbps_string_create_nocopy(const char *str)
{

	_XBPS_ASSERT(str != NULL);

	return _xbps_string_instantiate(PS_F_NOCOPY, str, strlen(str));
}

/*
 * xbps_string_copy --
 *	Copy a string.  This reduces to a retain in the common case.
 *	Deprecated mutable string objects must be copied.
 */
xbps_string_t
xbps_string_copy(xbps_string_t ops)
{
	xbps_string_t ps;
	char *cp;

	if (! xbps_object_is_string(ops))
		return (NULL);

	if ((ops->ps_flags & PS_F_MUTABLE) == 0) {
		xbps_object_retain(ops);
		return (ops);
	}

	cp = _XBPS_MALLOC(ops->ps_size + 1, M_XBPS_STRING);
	if (cp == NULL)
		return NULL;
	
	strcpy(cp, xbps_string_contents(ops));

	ps = _xbps_string_instantiate(PS_F_MUTABLE, cp, ops->ps_size);
	if (ps == NULL)
		_XBPS_FREE(cp, M_XBPS_STRING);

	return (ps);
}

_XBPS_DEPRECATED(xbps_string_copy_mutable,
    "this program uses xbps_string_copy_mutable(); all functions "
    "supporting mutable xbps_strings are deprecated.")
xbps_string_t
xbps_string_copy_mutable(xbps_string_t ops)
{
	xbps_string_t ps;
	char *cp;

	if (! xbps_object_is_string(ops))
		return (NULL);

	cp = _XBPS_MALLOC(ops->ps_size + 1, M_XBPS_STRING);
	if (cp == NULL)
		return NULL;
	
	strcpy(cp, xbps_string_contents(ops));

	ps = _xbps_string_instantiate(PS_F_MUTABLE, cp, ops->ps_size);
	if (ps == NULL)
		_XBPS_FREE(cp, M_XBPS_STRING);

	return (ps);
}

/*
 * xbps_string_size --
 *	Return the size of the string, not including the terminating NUL.
 */
size_t
xbps_string_size(xbps_string_t ps)
{

	if (! xbps_object_is_string(ps))
		return (0);

	return (ps->ps_size);
}

/*
 * xbps_string_value --
 *	Returns a pointer to the string object's value.  This pointer
 *	remains valid only as long as the string object.
 */
const char *
xbps_string_value(xbps_string_t ps)
{

	if (! xbps_object_is_string(ps))
		return (NULL);

	if ((ps->ps_flags & PS_F_MUTABLE) == 0)
		return (ps->ps_immutable);
	
	return (xbps_string_contents(ps));
}

/*
 * xbps_string_copy_value --
 *	Copy the string object's value into the supplied buffer.
 */
bool
xbps_string_copy_value(xbps_string_t ps, void *buf, size_t buflen)
{

	if (! xbps_object_is_string(ps))
		return (false);

	if (buf == NULL || buflen < ps->ps_size + 1)
		return (false);
	
	strcpy(buf, xbps_string_contents(ps));

	return (true);
}

_XBPS_DEPRECATED(xbps_string_mutable,
    "this program uses xbps_string_mutable(); all functions "
    "supporting mutable xbps_strings are deprecated.")
bool
xbps_string_mutable(xbps_string_t ps)
{

	if (! xbps_object_is_string(ps))
		return (false);

	return ((ps->ps_flags & PS_F_MUTABLE) != 0);
}

_XBPS_DEPRECATED(xbps_string_cstring,
    "this program uses xbps_string_cstring(), "
    "which is deprecated; use xbps_string_copy_value() instead.")
char *
xbps_string_cstring(xbps_string_t ps)
{
	char *cp;

	if (! xbps_object_is_string(ps))
		return (NULL);

	cp = _XBPS_MALLOC(ps->ps_size + 1, M_TEMP);
	if (cp != NULL)
		strcpy(cp, xbps_string_contents(ps));
	
	return (cp);
}

_XBPS_DEPRECATED(xbps_string_cstring_nocopy,
    "this program uses xbps_string_cstring_nocopy(), "
    "which is deprecated; use xbps_string_value() instead.")
const char *
xbps_string_cstring_nocopy(xbps_string_t ps)
{

	if (! xbps_object_is_string(ps))
		return (NULL);

	return (xbps_string_contents(ps));
}

_XBPS_DEPRECATED(xbps_string_append,
    "this program uses xbps_string_append(); all functions "
    "supporting mutable xbps_strings are deprecated.")
bool
xbps_string_append(xbps_string_t dst, xbps_string_t src)
{
	char *ocp, *cp;
	size_t len;

	if (! (xbps_object_is_string(dst) &&
	       xbps_object_is_string(src)))
		return (false);

	if ((dst->ps_flags & PS_F_MUTABLE) == 0)
		return (false);

	len = dst->ps_size + src->ps_size;
	cp = _XBPS_MALLOC(len + 1, M_XBPS_STRING);
	if (cp == NULL)
		return (false);
	snprintf(cp, len + 1, "%s%s", xbps_string_contents(dst),
		xbps_string_contents(src));
	ocp = dst->ps_mutable;
	dst->ps_mutable = cp;
	dst->ps_size = len;
	if (ocp != NULL)
		_XBPS_FREE(ocp, M_XBPS_STRING);
	
	return (true);
}

_XBPS_DEPRECATED(xbps_string_append_cstring,
    "this program uses xbps_string_append_cstring(); all functions "
    "supporting mutable xbps_strings are deprecated.")
bool
xbps_string_append_cstring(xbps_string_t dst, const char *src)
{
	char *ocp, *cp;
	size_t len;

	if (! xbps_object_is_string(dst))
		return (false);

	_XBPS_ASSERT(src != NULL);

	if ((dst->ps_flags & PS_F_MUTABLE) == 0)
		return (false);

	len = dst->ps_size + strlen(src);
	cp = _XBPS_MALLOC(len + 1, M_XBPS_STRING);
	if (cp == NULL)
		return (false);
	snprintf(cp, len + 1, "%s%s", xbps_string_contents(dst), src);
	ocp = dst->ps_mutable;
	dst->ps_mutable = cp;
	dst->ps_size = len;
	if (ocp != NULL)
		_XBPS_FREE(ocp, M_XBPS_STRING);
	
	return (true);
}

/*
 * xbps_string_equals --
 *	Return true if two strings are equivalent.
 */
bool
xbps_string_equals(xbps_string_t str1, xbps_string_t str2)
{
	if (!xbps_object_is_string(str1) || !xbps_object_is_string(str2))
		return (false);

	return xbps_object_equals(str1, str2);
}

/*
 * xbps_string_equals_string --
 *	Return true if the string object is equivalent to the specified
 *	C string.
 */
bool
xbps_string_equals_string(xbps_string_t ps, const char *cp)
{

	if (! xbps_object_is_string(ps))
		return (false);

	return (strcmp(xbps_string_contents(ps), cp) == 0);
}

_XBPS_DEPRECATED(xbps_string_equals_cstring,
    "this program uses xbps_string_equals_cstring(), "
    "which is deprecated; xbps_string_equals_string() instead.")
bool
xbps_string_equals_cstring(xbps_string_t ps, const char *cp)
{
	return xbps_string_equals_string(ps, cp);
}

/*
 * xbps_string_compare --
 *	Compare two string objects, using strcmp() semantics.
 */
int
xbps_string_compare(xbps_string_t ps1, xbps_string_t ps2)
{
	if (!xbps_object_is_string(ps1) || !xbps_object_is_string(ps2))
		return (-666);	/* arbitrary */

	return (strcmp(xbps_string_contents(ps1),
		       xbps_string_contents(ps2)));
}

/*
 * xbps_string_compare_string --
 *	Compare a string object to the specified C string, using
 *	strcmp() semantics.
 */
int
xbps_string_compare_string(xbps_string_t ps, const char *cp)
{
	if (!xbps_object_is_string(ps))
		return (-666);	/* arbitrary */

	return (strcmp(xbps_string_contents(ps), cp));
}

/*
 * _xbps_string_internalize --
 *	Parse a <string>...</string> and return the object created from the
 *	external representation.
 */
/* ARGSUSED */
bool
_xbps_string_internalize(xbps_stack_t stack, xbps_object_t *obj,
    struct _xbps_object_internalize_context *ctx)
{
	xbps_string_t string;
	const char *tag;
	char *str;
	size_t len, alen;

	if (ctx->poic_is_empty_element) {
		*obj = xbps_string_create();
		return (true);
	}
	
	/* No attributes recognized here. */
	if (ctx->poic_tagattr != NULL)
		return (true);

	/* Compute the length of the result. */
	if (_xbps_object_internalize_decode_string(ctx, NULL, 0, &len,
						   NULL) == false)
		return (true);
	
	str = _XBPS_MALLOC(len + 1, M_XBPS_STRING);
	if (str == NULL)
		return (true);
	
	if (_xbps_object_internalize_decode_string(ctx, str, len, &alen,
						   &ctx->poic_cp) == false ||
	    alen != len) {
		_XBPS_FREE(str, M_XBPS_STRING);
		return (true);
	}
	str[len] = '\0';

	if (ctx->poic_xbps) {
		tag = "s";
	} else {
		tag = "string";
	}
	if (_xbps_object_internalize_find_tag(ctx, tag,
					      _XBPS_TAG_TYPE_END) == false) {
		_XBPS_FREE(str, M_XBPS_STRING);
		return (true);
	}

	string = _xbps_string_instantiate(0, str, len);
	if (string == NULL)
		_XBPS_FREE(str, M_XBPS_STRING);
	
	*obj = string;
	return (true);
}
