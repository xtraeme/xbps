/*-
 * Copyright (c) 2008-2020 Juan Romero Pardines.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <sys/mman.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <limits.h>

#include "external/blake3/blake3.h"
#include <openssl/sha.h>

#include "xbps_api_impl.h"

static void
digest2hex(const unsigned char *digest, char *string, size_t len)
{
	while (len--) {
		if (*digest / 16 < 10)
			*string++ = '0' + *digest / 16;
		else
			*string++ = 'a' + *digest / 16 - 10;
		if (*digest % 16 < 10)
			*string++ = '0' + *digest % 16;
		else
			*string++ = 'a' + *digest % 16 - 10;
		++digest;
	}
	*string = '\0';
}

static bool
digest2nix(char *dst, size_t dstlen, const unsigned char *digest, size_t digestlen)
{
	/* omitted: E O U T */
	const char *base32chars = "0123456789abcdfghijklmnpqrsvwxyz";
	size_t len = (digestlen * 8 - 1) / 5 + 1;

	if (dstlen < len) {
		errno = ENOBUFS;
		return false;
	}
	if (!dst || !digest) {
		errno = EINVAL;
		return false;
	}

	*dst = '\0';
	for (int n = (int)len - 1; n >= 0; n--) {
		unsigned int b = n * 5;
		unsigned int i = b / 8;
		unsigned int j = b % 8;
		unsigned char c = (digest[i] >> j) |
			(i >= digestlen - 1 ? 0 : digest[i+1] << (8 - j));
		/* C++ string::push_back equivalent */
		dst[len-(n+1)] = base32chars[c & 0x1f];
	}
	dst[len] = '\0';

	return true;
}

bool
xbps_file_blake3_raw(unsigned char *dst, size_t dstlen, const char *file)
{
	int fd;
	ssize_t len;
	char buf[65536];
	blake3_hasher hasher;

	if (dstlen < XBPS_BINDIGEST_SIZE) {
		errno = ENOBUFS;
		return false;
	}

	if ((fd = open(file, O_RDONLY)) < 0) {
		return false;
	}
	blake3_hasher_init(&hasher);
	while ((len = read(fd, buf, sizeof(buf))) > 0) {
		blake3_hasher_update(&hasher, buf, len);
	}
	(void)close(fd);
	if (len == -1) {
		return false;
	}
	blake3_hasher_finalize(&hasher, dst, dstlen);
	return true;
}

bool
xbps_file_blake3(char *dst, size_t dstlen, const char *file)
{
	unsigned char digest[XBPS_BINDIGEST_SIZE];

	if (dstlen < XBPS_DIGEST_SIZE) {
		errno = ENOBUFS;
		return false;
	}
	if (!xbps_file_blake3_raw(digest, sizeof digest, file)) {
		return false;
	}
	digest2hex(digest, dst, XBPS_BINDIGEST_SIZE);
	return true;
}

bool
xbps_file_blake3_nixb32(char *dst, size_t dstlen, const char *file)
{
	/*
	 * This is a blake3 binary digest converted to the nix store
	 * encoding format (hash truncated to 160 bits encoded with base32).
	 */
	unsigned char digest[XBPS_BINDIGEST_SIZE];
	unsigned char dtrunc[20] = {0};

	if (!xbps_file_blake3_raw(digest, sizeof digest, file)) {
		return false;
	}
	for (unsigned int i = 0; i < sizeof digest; ++i) {
		dtrunc[i % sizeof dtrunc] ^= digest[i];
	}
	return digest2nix(dst, dstlen, dtrunc, sizeof dtrunc);
}

bool
xbps_file_sha256_raw(unsigned char *dst, size_t dstlen, const char *file)
{
	int fd;
	ssize_t len;
	char buf[65536];
	SHA256_CTX sha256;

	if (dstlen < XBPS_BINDIGEST_SIZE) {
		errno = ENOBUFS;
		return false;
	}
	if ((fd = open(file, O_RDONLY)) < 0) {
		return false;
	}
	SHA256_Init(&sha256);
	while ((len = read(fd, buf, sizeof(buf))) > 0) {
		SHA256_Update(&sha256, buf, len);
	}
	(void)close(fd);
	if (len == -1) {
		return false;
	}
	SHA256_Final(dst, &sha256);
	return true;
}

bool
xbps_file_sha256(char *dst, size_t dstlen, const char *file)
{
	unsigned char digest[XBPS_BINDIGEST_SIZE];

	if (dstlen < XBPS_DIGEST_SIZE) {
		errno = ENOBUFS;
		return false;
	}
	if (!xbps_file_sha256_raw(digest, sizeof digest, file)) {
		return false;
	}
	digest2hex(digest, dst, XBPS_BINDIGEST_SIZE);
	return true;
}

bool
xbps_file_sha256_nixb32(char *dst, size_t dstlen, const char *file)
{
	/*
	 * This is a sha256 binary digest converted to the nix store
	 * encoding format (hash truncated to 160 bits encoded with base32).
	 */
	unsigned char digest[XBPS_BINDIGEST_SIZE];
	unsigned char dtrunc[20] = {0};

	if (!xbps_file_sha256_raw(digest, sizeof digest, file)) {
		return false;
	}
	for (unsigned int i = 0; i < sizeof digest; ++i) {
		dtrunc[i % sizeof dtrunc] ^= digest[i];
	}
	return digest2nix(dst, dstlen, dtrunc, sizeof dtrunc);
}

static bool
digest_compare(const char *hash, size_t shalen,
		const unsigned char *digest, size_t digestlen)
{

	if (shalen != XBPS_DIGEST_SIZE -1)
		return false;

	if (digestlen != XBPS_BINDIGEST_SIZE)
		return false;

	for (; *hash;) {
		if (*digest / 16 < 10) {
			if (*hash++ != '0' + *digest / 16)
				return false;
		} else {
			if (*hash++ != 'a' + *digest / 16 - 10)
				return false;
		}
		if (*digest % 16 < 10) {
			if (*hash++ != '0' + *digest % 16)
				return false;
		} else {
			if (*hash++ != 'a' + *digest % 16 - 10)
				return false;
		}
		digest++;
	}

	return true;
}

int
xbps_file_blake3_check(const char *file, const char *str)
{
	char digest[XBPS_DIGEST_SIZE];

	assert(file != NULL);
	assert(str != NULL);

	if (!xbps_file_blake3_nixb32(digest, sizeof digest, file)) {
		return errno;
	}
	if (strncmp(str, digest, sizeof digest) != 0) {
		return ERANGE;
	}
	return 0;
}

int
xbps_file_sha256_check(const char *file, const char *sha256)
{
	unsigned char digest[XBPS_BINDIGEST_SIZE];

	assert(file != NULL);
	assert(sha256 != NULL);

	if (!xbps_file_sha256_raw(digest, sizeof digest, file)) {
		return errno;
	}
	if (!digest_compare(sha256, strlen(sha256), digest, sizeof digest)) {
		return ERANGE;
	}
	return 0;
}
