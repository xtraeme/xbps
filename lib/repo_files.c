/*-
 * Copyright (c) 2021 Juan Romero Pardines.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

#include "xbps_api_impl.h"

xbps_dictionary_t
xbps_repo_get_filelist(struct xbps_handle *xhp, const char *url)
{
	xbps_dictionary_t result = NULL;
	struct archive *ar = NULL;
	struct archive_entry *entry = NULL;
	struct stat st;
	char *repofile = NULL;
	int fd = -1;

	if (!xhp || !url) {
		return NULL;
	}

	if (xbps_repository_is_remote(url)) {
		/* remote repository */
		char *rpath;

		if ((rpath = xbps_get_remote_repo_string(url)) == NULL) {
			return NULL;
		}
		repofile = xbps_xasprintf("%s/%s/%s-repodata",
				xhp->metadir, rpath,
				xhp->target_arch ? xhp->target_arch : xhp->native_arch);
		free(rpath);
	} else {
		/* local repository */
		repofile = xbps_repo_path(xhp, url);
	}
	if (!repofile) {
		return NULL;
	}
	fd = open(repofile, O_RDONLY|O_CLOEXEC);
	if (fd == -1) {
		xbps_dbg_printf(xhp, "%s: open %s\n",
				repofile, strerror(errno));
		goto out;
	}

	ar = archive_read_new();
	assert(ar);
	archive_read_support_filter_gzip(ar);
	archive_read_support_filter_bzip2(ar);
	archive_read_support_filter_xz(ar);
	archive_read_support_filter_lz4(ar);
	archive_read_support_filter_zstd(ar);
	archive_read_support_format_tar(ar);

        if (fstat(fd, &st) == -1) {
		xbps_dbg_printf(xhp, "%s: fstat repodata %s\n",
				repofile, strerror(errno));
		goto out;
	}

	if (archive_read_open_fd(ar, fd, st.st_blksize) == ARCHIVE_FATAL) {
		xbps_dbg_printf(xhp,
		    "%s: failed to open repodata archive %s\n",
		    repofile, archive_error_string(ar));
		goto out;
	}

	for (uint8_t i = 0; i < 3; i++) {
		int rv;
		/*
		 * repodata contains max 3 files (in that order):
		 *
		 *  - index.plist
		 *  - index-meta.plist
		 *  - files.plist
		 *
		 * We are only interested in files.plist.
		 */
		rv = archive_read_next_header(ar, &entry);
		if (rv != ARCHIVE_OK) {
			xbps_dbg_printf(xhp, "%s: read_next_header %s\n",
					url, archive_error_string(ar));
			goto out;
		}
		if ((strcmp(archive_entry_pathname(entry), "files.plist")) == 0) {
			result = xbps_archive_get_dictionary(ar, entry);
			break;
		}
		archive_read_data_skip(ar);
	}
out:
	if (repofile) {
		free(repofile);
	}
	if (fd != -1) {
		close(fd);
	}
	if (ar) {
		archive_read_free(ar);
	}
	return result;
}
