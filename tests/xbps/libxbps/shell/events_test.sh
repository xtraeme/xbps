#! /usr/bin/env atf-sh

atf_test_case install

install_head() {
	atf_set "descr" "log events: install"
}
install_body() {
	mkdir -p repo pkg_A
	cd repo
	xbps-create -A noarch -n A-1_1 -s "A pkg" ../pkg_A
	atf_check_equal $? 0
	xbps-rindex -d -a $PWD/*.xbps
	atf_check_equal $? 0
	cd ..

	xbps-install -r root -R repo -y A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 install" "$2 $3"
}

atf_test_case reinstall

reinstall_head() {
	atf_set "descr" "log events: reinstall"
}
reinstall_body() {
	mkdir -p repo pkg_A
	cd repo
	xbps-create -A noarch -n A-1_1 -s "A pkg" ../pkg_A
	atf_check_equal $? 0
	xbps-rindex -d -a $PWD/*.xbps
	atf_check_equal $? 0
	cd ..

	xbps-install -r root -R repo -y A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 install" "$2 $3"
	xbps-install -r root -R repo -yf A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 reinstall" "$2 $3"
}

atf_test_case update

update_head() {
	atf_set "descr" "log events: update"
}
update_body() {
	mkdir -p repo pkg_A
	cd repo
	xbps-create -A noarch -n A-1_1 -s "A pkg" ../pkg_A
	atf_check_equal $? 0
	xbps-rindex -d -a $PWD/*.xbps
	atf_check_equal $? 0
	cd ..

	xbps-install -r root -R repo -y A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 install" "$2 $3"

	cd repo
	xbps-create -A noarch -n A-2_1 -s "A pkg" ../pkg_A
	atf_check_equal $? 0
	xbps-rindex -d -a $PWD/*.xbps
	atf_check_equal $? 0
	cd ..

	xbps-install -r root -R repo -yu
	atf_check_equal $? 0
	repopath=$(realpath repo)
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 update $repopath A-2_1" "$2 $3 $4 $5"
}

atf_test_case remove

remove_head() {
	atf_set "descr" "log events: remove"
}
remove_body() {
	mkdir -p repo pkg_A
	cd repo
	xbps-create -A noarch -n A-1_1 -s "A pkg" ../pkg_A
	atf_check_equal $? 0
	xbps-rindex -d -a $PWD/*.xbps
	atf_check_equal $? 0
	cd ..

	xbps-install -r root -R repo -y A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 install" "$2 $3"
	xbps-remove -r root -y A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 remove manual" "$2 $3 $4"
	xbps-install -r root -R repo -Ay A
	atf_check_equal $? 0
	xbps-remove -r root -y A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 remove automatic" "$2 $3 $4"
}

atf_test_case downgrade

downgrade_head() {
	atf_set "descr" "log events: downgrade"
}
downgrade_body() {
	mkdir -p repo pkg_A
	cd repo
	xbps-create -A noarch -n A-2_1 -s "A pkg" ../pkg_A
	atf_check_equal $? 0
	xbps-rindex -d -a $PWD/*.xbps
	atf_check_equal $? 0
	cd ..

	xbps-install -r root -R repo -y A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-2_1 install" "$2 $3"

	rm -f repo/*
	cd repo
	xbps-create -A noarch -n A-1_1 -s "A pkg" ../pkg_A
	atf_check_equal $? 0
	xbps-rindex -d -a -f $PWD/*.xbps
	atf_check_equal $? 0
	cd ..

	xbps-install -r root -R repo -yf A
	atf_check_equal $? 0
	repopath=$(realpath repo)
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-2_1 downgrade $repopath A-1_1" "$2 $3 $4 $5"
}

atf_test_case configure

configure_head() {
	atf_set "descr" "log events: configure"
}
configure_body() {
	mkdir -p repo pkg_A
	cd repo
	xbps-create -A noarch -n A-1_1 -s "A pkg" ../pkg_A
	atf_check_equal $? 0
	xbps-rindex -d -a $PWD/*.xbps
	atf_check_equal $? 0
	cd ..

	xbps-install -r root -R repo -y A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 install" "$2 $3"

	xbps-reconfigure -r root -f A
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 configure" "$2 $3"

	rm -rf root
	xbps-install -r root -R repo -yU A
	atf_check_equal $? 0
	xbps-reconfigure -r root -a
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 configure" "$2 $3"
}

atf_test_case auto

auto_head() {
	atf_set "descr" "log events: automatic mode"
}
auto_body() {
	mkdir -p repo pkg_A
	cd repo
	xbps-create -A noarch -n A-1_1 -s "A pkg" ../pkg_A
	atf_check_equal $? 0
	xbps-rindex -d -a $PWD/*.xbps
	atf_check_equal $? 0
	cd ..

	xbps-install -r root -R repo -y A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 install" "$2 $3"

	xbps-pkgdb -r root -m auto A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 automatic" "$2 $3"
	atf_check_equal "$(xbps-query -r root -p automatic-install A)" "yes"
}

atf_test_case manual

manual_head() {
	atf_set "descr" "log events: manual mode"
}
manual_body() {
	mkdir -p repo pkg_A
	cd repo
	xbps-create -A noarch -n A-1_1 -s "A pkg" ../pkg_A
	atf_check_equal $? 0
	xbps-rindex -d -a $PWD/*.xbps
	atf_check_equal $? 0
	cd ..

	xbps-install -r root -R repo -y A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 install" "$2 $3"

	xbps-pkgdb -r root -m auto A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 automatic" "$2 $3"
	xbps-pkgdb -r root -m manual A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 manual" "$2 $3"
	atf_check_equal "$(xbps-query -r root -p automatic-install A)" ""
}

atf_add_test_case repolock

repolock_head() {
	atf_set "descr" "log events: repolock mode"
}
repolock_body() {
	mkdir -p repo pkg_A
	cd repo
	xbps-create -A noarch -n A-1_1 -s "A pkg" ../pkg_A
	atf_check_equal $? 0
	xbps-rindex -d -a $PWD/*.xbps
	atf_check_equal $? 0
	cd ..

	xbps-install -r root -R repo -y A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 install" "$2 $3"

	xbps-pkgdb -r root -m repolock A
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 repolock" "$2 $3"
	atf_check_equal "$(xbps-query -r root -p repolock A)" "yes"

	xbps-pkgdb -r root -m repounlock A
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 repounlock" "$2 $3"
	atf_check_equal "$(xbps-query -r root -p repolock A)" ""
}

atf_add_test_case alternatives

alternatives_head() {
	atf_set "descr" "log events: alternatives set"
}
alternatives_body() {
	mkdir -p repo pkg_A pkg_B
	touch pkg_A/foo-a
	touch pkg_B/foo-b
	touch pkg_B/baz-b
	cd repo
	xbps-create -A noarch -n A-1_1 -s "A pkg" --alternatives "foo:foo:/foo-a" ../pkg_A
	atf_check_equal $? 0
	xbps-create -A noarch -n B-1_1 -s "B pkg" --alternatives "foo:foo:/foo-b baz:baz:/baz-b" ../pkg_B
	atf_check_equal $? 0
	xbps-rindex -d -a $PWD/*.xbps
	atf_check_equal $? 0
	cd ..

	xbps-install -r root -R repo -y A
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 install" "$2 $3"
	cat root/var/db/xbps/events.txt
	set -- $(grep alternatives root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 alternatives-group-set foo" "$2 $3 $4"

	xbps-install -r root -R repo -y B
	atf_check_equal $? 0
	cat root/var/db/xbps/events.txt
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "B-1_1 install" "$2 $3"

	xbps-alternatives -r root -s B
	atf_check_equal $? 0
	set -- $(tail -2 root/var/db/xbps/events.txt)
	atf_check_equal "B-1_1 alternatives-group-set baz" "$2 $3 $4"
	set -- $(tail -2 root/var/db/xbps/events.txt|grep foo)
	atf_check_equal "B-1_1 alternatives-group-set foo" "$2 $3 $4"

	xbps-remove -r root -y B
	atf_check_equal $? 0
	cat root/var/db/xbps/events.txt
	set -- $(tail -2 root/var/db/xbps/events.txt|grep alternatives)
	atf_check_equal "A-1_1 alternatives-group-set foo" "$2 $3 $4"
}

atf_add_test_case log_disabled

log_disabled_head() {
	atf_set "descr" "log events: test disabled"
}
log_disabled_body() {
	mkdir -p repo pkg_A
	cd repo
	xbps-create -A noarch -n A-1_1 -s "A pkg" ../pkg_A
	atf_check_equal $? 0
	xbps-create -A noarch -n B-1_1 -s "B pkg" ../pkg_A
	atf_check_equal $? 0
	xbps-rindex -d -a $PWD/*.xbps
	atf_check_equal $? 0
	cd ..

	xbps-install -r root -R repo -y A
	atf_check_equal $? 0
	set -- $(cat root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 install" "$2 $3"

	echo "logging=false" > root/etc/xbps.d/foo.conf
	xbps-install -r root -R repo -y B
	atf_check_equal $? 0
	set -- $(cat root/var/db/xbps/events.txt)
	atf_check_equal "A-1_1 install" "$2 $3"

	echo "logging=true" > root/etc/xbps.d/foo.conf
	xbps-remove -r root -y B
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "B-1_1 remove" "$2 $3"

	rm -rf root/etc
	xbps-install -r root -R repo -y B
	atf_check_equal $? 0
	set -- $(tail -1 root/var/db/xbps/events.txt)
	atf_check_equal "B-1_1 install" "$2 $3"
}

atf_init_test_cases() {
	atf_add_test_case install
	atf_add_test_case reinstall
	atf_add_test_case update
	atf_add_test_case remove
	atf_add_test_case downgrade
	atf_add_test_case configure
	atf_add_test_case auto
	atf_add_test_case manual
	atf_add_test_case repolock
	atf_add_test_case alternatives
	atf_add_test_case log_disabled
}
