#! /usr/bin/env atf-sh

atf_test_case remove_directory

remoe_directory_head() {
	atf_set "descr" "xbps-remove(1): remove nested directories"
}

remove_directory_body() {
	mkdir -p some_repo pkg_A/B/C
	touch pkg_A/B/C/file00
	cd some_repo
	xbps-create -A noarch -n A-1.0_1 -s "A pkg" ../pkg_A
	atf_check_equal $? 0
	xbps-rindex -d -a $PWD/*.xbps
	atf_check_equal $? 0
	cd ..
	xbps-install -r root -C empty.conf --repository=$PWD/some_repo -y A
	atf_check_equal $? 0
	xbps-remove -r root -C empty.conf -y A
	atf_check_equal $? 0
	test -d root/B
	atf_check_equal $? 1
}

atf_test_case remove_orphans

remove_orphans_head() {
	atf_set "descr" "xbps-remove(1): remove orphaned packages"
}

remove_orphans_body() {
	mkdir -p some_repo pkg_A/B/C
	touch pkg_A/
	cd some_repo
	xbps-create -A noarch -n A-1.0_1 -s "A pkg" ../pkg_A
	atf_check_equal $? 0
	xbps-rindex -d -a $PWD/*.xbps
	atf_check_equal $? 0
	cd ..
	xbps-install -r root -C empty.conf --repository=$PWD/some_repo -yA A
	atf_check_equal $? 0
	xbps-remove -r root -C empty.conf -yvdO
	atf_check_equal $? 0
	xbps-query -r root A
	atf_check_equal $? 2
}

atf_test_case remove_recursively

remove_recursively_head() {
	atf_set "descr" "xbps-remove(1): removing multiple pkgs recursively"
}

remove_recursively_body() {
	mkdir -p repo pkg
	cd repo
	xbps-create -A noarch -n A-1.0_1 -s "A pkg" -D "B>=0" ../pkg
	atf_check_equal $? 0
	xbps-create -A noarch -n B-1.0_1 -s "B pkg" -D "C>=0" ../pkg
	atf_check_equal $? 0
	xbps-create -A noarch -n C-1.0_1 -s "C pkg" ../pkg
	atf_check_equal $? 0
	xbps-create -A noarch -n D-1.0_1 -s "D pkg" -D "C>=0 E>=0" ../pkg
	atf_check_equal $? 0
	xbps-create -A noarch -n E-1.0_1 -s "E pkg" -D "B>=0" ../pkg
	atf_check_equal $? 0
	xbps-rindex -d -a $PWD/*.xbps
	atf_check_equal $? 0
	cd ..
	xbps-install -r root --repo=repo -yU A D
	atf_check_equal $? 0
	atf_check_equal 1 $(xbps-remove -r root -Rn A|wc -l)
	atf_check_equal 2 $(xbps-remove -r root -Rn D|wc -l)
	echo "Removing A D"
	xbps-remove -r root -Rn A D
	atf_check_equal 5 $(xbps-remove -r root -Rn A D|wc -l)
	echo "Removing D A"
	xbps-remove -r root -Rn D A
	atf_check_equal 5 $(xbps-remove -r root -Rn D A|wc -l)
}

atf_init_test_cases() {
	atf_add_test_case remove_directory
	atf_add_test_case remove_orphans
	atf_add_test_case remove_recursively
}
