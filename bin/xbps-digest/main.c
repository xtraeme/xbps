/*-
 * Copyright (c) 2019-2020 Juan Romero Pardines.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <unistd.h>
#include <getopt.h>

#include <xbps.h>

enum {
	MODE_SHA256,
	MODE_BLAKE3,
	MODE_NIX_SHA256,
	MODE_NIX_BLAKE3,
	MODE_DEFAULT = MODE_SHA256
};

static void __attribute__((noreturn))
usage(bool fail)
{
	fprintf(stdout,
	"Usage: xbps-digest [options] [file] [file+N]\n"
	"\n"
	"OPTIONS\n"
	" -h, --help\n"
	" -m, --mode <blake3|sha256|nix-blake3|nix-sha256> (sha256 default)\n"
	" -V, --version\n"
	"\nNOTES\n"
	" If [file] not set, reads from stdin\n");
	exit(fail ? EXIT_FAILURE : EXIT_SUCCESS);
}

int
main(int argc, char **argv)
{
	int c, dmode = MODE_DEFAULT;
	char digest[XBPS_DIGEST_SIZE];
	const char *mode = NULL, *progname = argv[0];
	const struct option longopts[] = {
		{ "mode", required_argument, NULL, 'm' },
		{ "help", no_argument, NULL, 'h' },
		{ "version", no_argument, NULL, 'V' },
		{ NULL, 0, NULL, 0 }
	};

	while ((c = getopt_long(argc, argv, "m:hV", longopts, NULL)) != -1) {
		switch (c) {
		case 'h':
			usage(false);
			/* NOTREACHED */
		case 'm':
			mode = optarg;
			break;
		case 'V':
			printf("%s\n", XBPS_RELVER);
			exit(EXIT_SUCCESS);
		case '?':
		default:
			usage(true);
			/* NOTREACHED */
		}
	}

	argc -= optind;
	argv += optind;

	if (mode) {
		if (strcmp(mode, "blake3") == 0) {
			dmode = MODE_BLAKE3;
		} else if (strcmp(mode, "sha256") == 0) {
			dmode = MODE_SHA256;
		} else if (strcmp(mode, "nix-sha256") == 0) {
			dmode = MODE_NIX_SHA256;
		} else if (strcmp(mode, "nix-blake3") == 0) {
			dmode = MODE_NIX_BLAKE3;
		} else {
			fprintf(stderr, "%s: unsupported digest mode %s\n", progname, mode);
			exit(EXIT_FAILURE);
		}
	} else {
		dmode = MODE_DEFAULT;
	}

#define DIGEST(type, func, arg) 					\
do {									\
	if (dmode == type ) {						\
		if (!xbps_file_ ## func (digest, sizeof digest, arg)) 	\
			exit(EXIT_FAILURE);				\
	}								\
} while (0)

	if (argc < 1) {
		DIGEST(MODE_BLAKE3, blake3, "/dev/stdin");
		DIGEST(MODE_NIX_BLAKE3, blake3_nixb32, "/dev/stdin");
		DIGEST(MODE_SHA256, sha256, "/dev/stdin");
		DIGEST(MODE_NIX_SHA256, sha256_nixb32, "/dev/stdin");
		printf("%s\n", digest);
	} else {
		for (int i = 0; i < argc; i++) {
			DIGEST(MODE_BLAKE3, blake3, argv[i]);
			DIGEST(MODE_NIX_BLAKE3, blake3_nixb32, argv[i]);
			DIGEST(MODE_SHA256, sha256, argv[i]);
			DIGEST(MODE_NIX_SHA256, sha256_nixb32, argv[i]);
			printf("%s\n", digest);
		}
	}
	exit(EXIT_SUCCESS);
}
