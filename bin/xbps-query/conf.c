/*-
 * Copyright (c) 2021 Juan Romero Pardines.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <limits.h>
#include <errno.h>
#include <assert.h>

#include "defs.h"

int
list_modified_conf(struct xbps_handle *xhp,
		xbps_object_t obj,
		const char *pkgname,
		void *arg UNUSED,
		bool *done UNUSED)
{
	char buf[PATH_MAX];
	xbps_dictionary_t d;
	xbps_array_t array;
	xbps_object_t lobj;
	xbps_object_iterator_t iter;

	/* we are only interested in pkgs with conf_files */
	if (!xbps_dictionary_get(obj, "conf_files")) {
		return 0;
	}

	snprintf(buf, sizeof(buf)-1, "%s/.%s-files.plist", xhp->metadir, pkgname);
	d = xbps_plist_dictionary_from_file(xhp, buf);
	if (d == NULL) {
		return ENOENT;
	}
	/* d contains an array of dicts */
	array = xbps_dictionary_get(d, "conf_files");
	if (xbps_object_type(array) != XBPS_TYPE_ARRAY) {
		return EINVAL;
	}
	/* match all files against ones stored in rootdir */
	iter = xbps_array_iterator(array);
	assert(iter);

	while ((lobj = xbps_object_iterator_next(iter))) {
		const char *fname = NULL, *hash = NULL;
		int rv;

		if (!xbps_dictionary_get_string(lobj, "file", &fname)) {
			return EINVAL;
		}
		snprintf(buf, sizeof(buf)-1, "%s", fname);

		if (xbps_dictionary_get_string(lobj, "blake3", &hash)) {
			rv = xbps_file_blake3_check(buf, hash);
		} else {
			if (!xbps_dictionary_get_string(lobj, "sha256", &hash)) {
				return EINVAL;
			}
			rv = xbps_file_sha256_check(buf, hash);
		}
		switch (rv) {
		case 0:
		/* ignore, to be sure elevate privs! */
		case EACCES:
		case EPERM:
			break;
		/* report failure if conf file is gone */
		case ENOENT:
			printf("%s %s [missing]\n", pkgname, fname);
			break;

		default:
			printf("%s %s [modified]\n", pkgname, fname);
			break;
		}
	}
	xbps_object_iterator_release(iter);
	xbps_object_release(d);

	return 0;
}
