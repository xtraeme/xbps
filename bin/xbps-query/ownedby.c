/*-
 * Copyright (c) 2010-2021 Juan Romero Pardines.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <fnmatch.h>
#include <assert.h>
#include <regex.h>

#include <xbps.h>
#include "defs.h"

struct ffdata {
	bool rematch;
	const char *pat, *repouri;
	regex_t regex;
	xbps_array_t allkeys;
	xbps_dictionary_t filesd;
};

static int
files_match(struct ffdata *ffd)
{
	xbps_dictionary_keysym_t key;
	xbps_array_t array;
	const char *pkgname = NULL, *file = NULL;

	for (unsigned int i = 0; i < xbps_array_count(ffd->allkeys); i++) {
		key = xbps_array_get(ffd->allkeys, i);
		pkgname = xbps_dictionary_keysym_value(key);
		array = xbps_dictionary_get_keysym(ffd->filesd, key);
		for (unsigned int x = 0; x < xbps_array_count(array); x++) {
			xbps_array_get_string(array, x, &file);
			if (ffd->rematch) {
				if (regexec(&ffd->regex, file, 0, 0, 0) == 0) {
					printf("%s %s\n", pkgname, file);
				}
			} else {
				if ((fnmatch(ffd->pat, file, FNM_PERIOD)) == 0) {
					printf("%s %s\n", pkgname, file);
				}
			}
		}
	}
	return 0;
}

static int
rpool_cb(struct xbps_repo *repo, void *arg, bool *done UNUSED)
{
	struct ffdata *ffd = arg;

	ffd->repouri = repo->uri;
	ffd->filesd = xbps_repo_get_filelist(repo->xhp, repo->uri);
	if (ffd->filesd == NULL) {
		return EINVAL;
	}
	ffd->allkeys = xbps_dictionary_all_keys(ffd->filesd);
	return files_match(ffd);
}

static int
pkgdb_mode(struct xbps_handle *xhp, struct ffdata *ffd)
{
	char *plist;

	plist = xbps_xasprintf("%s/%s", xhp->metadir, XBPS_PKGDB_FILES);
	assert(plist);
	ffd->filesd = xbps_dictionary_internalize_from_file(plist);
	if (ffd->filesd == NULL) {
		free(plist);
		return 0;
	}
	free(plist);
	ffd->allkeys = xbps_dictionary_all_keys(ffd->filesd);
	return files_match(ffd);
}

int
ownedby(struct xbps_handle *xhp, const char *pat, bool repo, bool regex)
{
	struct ffdata ffd = {0};
	int rv;

	ffd.rematch = false;
	ffd.pat = pat;

	if (regex) {
		ffd.rematch = true;
		if (regcomp(&ffd.regex, ffd.pat, REG_EXTENDED|REG_NOSUB|REG_ICASE) != 0)
			return EINVAL;
	}
	if (repo) {
		rv = xbps_rpool_foreach(xhp, rpool_cb, &ffd);
	} else {
		rv = pkgdb_mode(xhp, &ffd);
	}
	xbps_object_release(ffd.filesd);
	xbps_object_release(ffd.allkeys);

	if (regex) {
		regfree(&ffd.regex);
	}
	return rv;
}
