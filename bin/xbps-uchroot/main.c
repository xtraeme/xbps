/*-
 * Copyright (c) 2014-2021 Juan Romero Pardines.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * This is based on linux-user-chroot by Colin Walters, but has been adapted
 * specifically for xbps-src use:
 *
 *     - Specifically requires user_namespaces(7).
 *     - Supports overlayfs on a temporary directory or on top of tmpfs (changes are always discarded).
 *     - Supports read-only bind mounts.
 */

#define _GNU_SOURCE
#define _XOPEN_SOURCE 700
#include <sys/types.h>
#include <sys/mount.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <stdarg.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <sched.h>
#include <limits.h>	/* PATH_MAX */
#include <ftw.h>
#include <signal.h>
#include <getopt.h>
#include <dirent.h>
#include <libgen.h>

#include <xbps.h>
#include "queue.h"

struct bindmnt {
	SIMPLEQ_ENTRY(bindmnt) entries;
	char *src;
	const char *dest;
	bool ro;	/* readonly */
};

static char *tmpdir;
static bool overlayfs_on_tmpfs;
static SIMPLEQ_HEAD(bindmnt_head, bindmnt) bindmnt_queue =
    SIMPLEQ_HEAD_INITIALIZER(bindmnt_queue);

static void __attribute__((noreturn))
usage(const char *p, bool fail)
{
	printf("Usage: %s [OPTIONS] [--] <dir> [<cmd> <cmdargs>]\n\n"
	    "-B, --bind-ro <src:dest>      Bind mounts <src> into <dir>/<dest> (read-only)\n"
	    "-b, --bind-rw <src:dest>      Bind mounts <src> into <dir>/<dest> (read-write)\n"
	    "-l, --lowerdir <dir>[:<dir>]  Sets up lowerdir for overlayfs\n"
	    "-O, --overlayfs               Mounts <dir> in a temporary overlayfs (changes are discarded)\n"
	    "-o, --options <opts>          Options to be passed to the tmpfs mount (for use with -t)\n"
	    "-t, --tmpfs                   mounts <dir> on top of tmpfs (changes are discarded)\n"
	    "-V, --version                 Show XBPS version\n"
	    "-h, --help                    Show usage\n", p);
	exit(fail ? EXIT_FAILURE : EXIT_SUCCESS);
}

static void __attribute__((noreturn)) __attribute__((__format__ (__printf__, 1, 0)))
die(const char *fmt, ...)
{
	va_list ap;
	int save_errno = errno;

	va_start(ap, fmt);
	fprintf(stderr, "ERROR: ");
	vfprintf(stderr, fmt, ap);
	fprintf(stderr, " (%s)\n", strerror(save_errno));
	va_end(ap);
	exit(EXIT_FAILURE);
}

static int
ftw_cb(const char *fpath, const struct stat *sb)
{
	int sverrno = 0;

	if (S_ISDIR(sb->st_mode)) {
		if (rmdir(fpath) == -1) {
			sverrno = errno;
			if (sverrno == EBUSY) {
				/* maybe a mountpoint? */
				umount2(fpath, MNT_DETACH);
				sverrno = 0;
			}
			if (rmdir(fpath) == -1) {
				sverrno = errno;
			}
		}
	} else {
		if (unlink(fpath) == -1)
			sverrno = errno;
	}
	if (sverrno != 0) {
		fprintf(stderr, "Failed to remove %s: %s\n", fpath, strerror(sverrno));
	}
	return sverrno;
}

static int
walk_dir(const char *path,
	int (*fn)(const char *fpath, const struct stat *sb))
{
	struct dirent **list;
	struct stat sb;
	const char *p;
	char tmp_path[PATH_MAX] = {0};
	int rv = 0, i;

	i = scandir(path, &list, NULL, alphasort);
	if (i == -1) {
		rv = -1;
		goto out;
	}
	while (i--) {
		p = list[i]->d_name;
		if (strcmp(p, ".") == 0 || strcmp(p, "..") == 0)
			continue;
		if (strlen(path) + strlen(p) + 1 >= (PATH_MAX - 1)) {
			errno = ENAMETOOLONG;
			rv = -1;
			break;
		}
		strncpy(tmp_path, path, PATH_MAX - 1);
		strncat(tmp_path, "/", PATH_MAX - 1 - strlen(tmp_path));
		strncat(tmp_path, p, PATH_MAX - 1 - strlen(tmp_path));
		if (lstat(tmp_path, &sb) < 0) {
			break;
		}
		if (S_ISDIR(sb.st_mode)) {
			if (walk_dir(tmp_path, fn) < 0) {
				rv = -1;
				break;
			}
		}
		rv = fn(tmp_path, &sb);
		if (rv != 0) {
			break;
		}
	}
out:
	free(list);
	return rv;
}

static void
cleanup_overlayfs(void)
{
	if (tmpdir == NULL)
		return;

	if (overlayfs_on_tmpfs)
		goto out;

	/* recursively remove the temporary dir */
	if (walk_dir(tmpdir, ftw_cb) != 0) {
		fprintf(stderr, "Failed to remove directory tree %s: %s\n",
			tmpdir, strerror(errno));
		exit(EXIT_FAILURE);
	}
out:
	umount2(tmpdir, MNT_DETACH);
	rmdir(tmpdir);
}

static void __attribute__((noreturn))
sighandler_cleanup(int signum)
{
	switch (signum) {
	case SIGINT:
	case SIGTERM:
	case SIGQUIT:
		cleanup_overlayfs();
		break;
	}
	_exit(signum);
}

static void
add_bindmount(const char *bm, bool ro)
{
	struct bindmnt *bmnt;
	char *b, *src, *dest;
	size_t len;

	src = strdup(bm);
	assert(src);
	dest = strchr(bm, ':');
	if (dest == NULL || *dest == '\0') {
		errno = EINVAL;
		die("invalid argument for bindmount: %s", bm);
	}
	dest++;
	b = strchr(bm, ':');
	len = strlen(bm) - strlen(b);
	src[len] = '\0';

	bmnt = malloc(sizeof(struct bindmnt));
	assert(bmnt);

	bmnt->src = src;
	bmnt->dest = dest;
	bmnt->ro = ro;
	SIMPLEQ_INSERT_TAIL(&bindmnt_queue, bmnt, entries);
}

static void
bindmount(const char *chrootdir, const char *src, const char *dest)
{
	struct stat sb;
	char *p, *p1, mountdir[PATH_MAX-1];
	int fd, flags = MS_BIND|MS_REC|MS_PRIVATE;

	snprintf(mountdir, sizeof(mountdir), "%s%s", chrootdir, dest ? dest : src);
	/*
	 * If dest does not exist, create it. Only directories and regular files
	 * are currently supported.
	 */
	if (stat(src, &sb) != 0) {
		die("Failed to stat %s", src);
	}
	if (S_ISDIR(sb.st_mode)) {
		/* directory */
		if (xbps_mkpath(mountdir, 0755) == -1) {
			if (errno != EEXIST) {
				die("Failed to create dir %s", mountdir);
			}
		}
	} else if (S_ISREG(sb.st_mode)) {
		/* regular file */
		p = strdup(mountdir);
		assert(p);
		p1 = dirname(p);
		assert(p1);
		if (xbps_mkpath(p1, 0755) == -1) {
			if (errno != EEXIST) {
				die("Failed to create dir %s", mountdir);
			}
		}
		/* Check if file exists already on dest */
		free(p);
		if ((access(mountdir, F_OK) == -1) && (errno == ENOENT)) {
			fd = open(mountdir, O_WRONLY|O_CREAT|O_EXCL, sb.st_mode);
			if (fd == -1) {
				die("Failed to create file %s", mountdir);
			}
			close(fd);
		}
	} else {
		die("Unsupported filetype for bindmount %s", src);
	}

	if (mount(src, mountdir, NULL, flags, NULL) == -1)
		die("Failed to bind mount %s at %s", src, mountdir);
}

static void
remount_rdonly(const char *chrootdir, const char *src, const char *dest, bool ro)
{
	char mountdir[PATH_MAX-1];
	int flags = MS_REMOUNT|MS_BIND|MS_RDONLY;

	if (!ro)
		return;

	snprintf(mountdir, sizeof(mountdir), "%s%s", chrootdir, dest ? dest : src);
	if (mount(src, mountdir, NULL, flags, NULL) == -1)
		die("Failed to remount read-only %s at %s", src, mountdir);
}

static char *
setup_overlayfs(const char *chrootdir, const char *lowerdir,
		bool tmpfs, const char *tmpfs_opts)
{
	char *upperdir, *workdir, *newchrootdir, *mopts;
	const void *opts = NULL;

	if (tmpfs) {
		/*
		* Create a temporary directory on tmpfs for overlayfs storage.
		*/
		opts = tmpfs_opts;
		if (mount("tmpfs", tmpdir, "tmpfs", 0, opts) == -1)
			die("failed to mount tmpfs on %s", tmpdir);
	}
	/*
	 * Create the upper/work dirs to setup overlayfs.
	 */
	upperdir = xbps_xasprintf("%s/upperdir", tmpdir);
	if (mkdir(upperdir, 0755) == -1)
		die("failed to create upperdir (%s)", upperdir);

	workdir = xbps_xasprintf("%s/workdir", tmpdir);
	if (mkdir(workdir, 0755) == -1)
		die("failed to create workdir (%s)", workdir);

	newchrootdir = xbps_xasprintf("%s/masterdir", tmpdir);
	if (mkdir(newchrootdir, 0755) == -1)
		die("failed to create newchrootdir (%s)", newchrootdir);

	mopts = xbps_xasprintf("userxattr,nfs_export=off,xino=off,upperdir=%s,lowerdir=%s,workdir=%s",
		upperdir, lowerdir ? lowerdir : chrootdir, workdir);

	opts = mopts;
	if (mount(chrootdir, newchrootdir, "overlay", 0, opts) == -1)
		die("failed to mount overlayfs on %s", newchrootdir);

	free(mopts);
	free(upperdir);
	free(workdir);

	return newchrootdir;
}

int
main(int argc, char **argv)
{
	struct sigaction sa = {0};
	const char *rootdir, *tmpfs_opts, *cmd, *argv0, *lowerdir;
	char **cmdargs, *b, *chrootdir;
	char buf[32], mountdir[PATH_MAX-1];
	int fd, c, clone_flags, child_status = 0;
	uid_t uid;
	gid_t gid;
	pid_t child;
	bool overlayfs = false;
	const struct option longopts[] = {
		{ "bind-rw", required_argument, NULL, 'B' },
		{ "bind-ro", required_argument, NULL, 'b' },
		{ "lowerdir", required_argument, NULL, 'l' },
		{ "overlayfs", no_argument, NULL, 'O' },
		{ "options", required_argument, NULL, 'o' },
		{ "tmpfs", no_argument, NULL, 't' },
		{ "version", no_argument, NULL, 'V' },
		{ "help", no_argument, NULL, 'h' },
		{ NULL, 0, NULL, 0 }
	};

	tmpfs_opts = rootdir = cmd = lowerdir = NULL;
	argv0 = argv[0];

	while ((c = getopt_long(argc, argv, "B:b:l:Oo:tVh", longopts, NULL)) != -1) {
		switch (c) {
		case 'B':
			if (optarg == NULL || *optarg == '\0')
				break;
			add_bindmount(optarg, true);
			break;
		case 'b':
			if (optarg == NULL || *optarg == '\0')
				break;
			add_bindmount(optarg, false);
			break;
		case 'l':
			lowerdir = optarg;
			break;
		case 'O':
			overlayfs = true;
			break;
		case 'o':
			tmpfs_opts = optarg;
			break;
		case 't':
			overlayfs_on_tmpfs = true;
			break;
		case 'V':
			printf("%s\n", XBPS_RELVER);
			exit(EXIT_SUCCESS);
		case 'h':
			usage(argv0, false);
			/* NOTREACHED */
		case '?':
		default:
			usage(argv0, true);
			/* NOTREACHED */
		}
	}
	argc -= optind;
	argv += optind;

	if (argc < 1) {
		usage(argv0, true);
		/* NOTREACHED */
	}

	rootdir = argv[0];
	cmd = argv[1];
	cmdargs = argv + 1;
	if (!cmd) {
		cmd = "/bin/sh";
	}
	/* Make chrootdir absolute */
	chrootdir = realpath(rootdir, NULL);
	if (!chrootdir)
		die("realpath rootdir");

	/* Never allow chrootdir == / */
	if (strcmp(chrootdir, "/") == 0)
		die("/ is not allowed to be used as chrootdir");

        /* Never allow chrootdir == / */
        if (strcmp(chrootdir, "/") == 0)
                die("/ is not allowed to be used as chrootdir");

        /* Make chrootdir absolute */
        if (chrootdir[0] != '/') {
                char cwd[PATH_MAX-1];
                if (getcwd(cwd, sizeof(cwd)) == NULL)
                        die("getcwd");
                chrootdir = xbps_xasprintf("%s/%s", cwd, chrootdir);
        }

	/* get effective user/group ID before unsharing from current namespace */
	uid = geteuid();
	gid = getegid();

        /*
         * Unshare from the current process namespaces and set ours.
         */
	clone_flags = CLONE_NEWPID|CLONE_FILES|CLONE_NEWUSER|CLONE_NEWNS|CLONE_NEWUTS|CLONE_NEWIPC;
	if (unshare(clone_flags) == -1) {
		die("unshare");
	}
	/*
         * Setup uid/gid user mappings and restrict setgroups().
         */
        if ((fd = open("/proc/self/uid_map", O_RDWR)) == -1)
                die("failed to open /proc/self/uid_map rw");
        if (write(fd, buf, snprintf(buf, sizeof buf, "%u %u 1\n", uid, uid)) == -1)
                die("failed to write to /proc/self/uid_map");

        close(fd);

        if ((fd = open("/proc/self/setgroups", O_RDWR)) != -1) {
                if (write(fd, "deny", 4) == -1)
                        die("failed to write to /proc/self/setgroups");
                close(fd);
        }

        if ((fd = open("/proc/self/gid_map", O_RDWR)) == -1)
                die("failed to open /proc/self/gid_map rw");
        if (write(fd, buf, snprintf(buf, sizeof buf, "%u %u 1\n", gid, gid)) == -1)
                die("failed to write to /proc/self/gid_map");

        close(fd);

	/* create overlayfs masterdir */
	if (overlayfs) {
		b = xbps_xasprintf("%s.XXXXXXXXXX", chrootdir);
		if ((tmpdir = mkdtemp(b)) == NULL)
			die("failed to create tmpdir directory");
		/*
		 * Register a signal handler to clean up temporary masterdir.
		 */
		sa.sa_handler = sighandler_cleanup;
		sigaction(SIGINT, &sa, NULL);
		sigaction(SIGTERM, &sa, NULL);
		sigaction(SIGQUIT, &sa, NULL);
	}

	child = fork();
	if (child == 0) {
		struct bindmnt *bmnt;
		/* mount as private, systemd mounts it as shared by default */
		if (mount(NULL, "/", "none", MS_PRIVATE|MS_REC, NULL) == -1)
			die("Failed to mount / private");

		/* setup our overlayfs if set */
		if (overlayfs)
			chrootdir = setup_overlayfs(chrootdir, lowerdir,
			    overlayfs_on_tmpfs, tmpfs_opts);

		/* mount /proc */
		snprintf(mountdir, sizeof(mountdir), "%s/proc", chrootdir);
		if (mkdir(mountdir, 0755) == -1 && errno != EEXIST) {
			die("mkdir %s", mountdir);
		}
		if (mount("proc", mountdir, "proc",
			  MS_MGC_VAL|MS_PRIVATE|MS_RDONLY, NULL) == -1) {
			/* try bind mount */
			add_bindmount("/proc:/proc", true);
		}
		/* bind mount /sys, /dev (recursively) */
		add_bindmount("/sys:/sys", false);
		add_bindmount("/dev:/dev", false);

		/* bind mount all specified mnts */
		SIMPLEQ_FOREACH(bmnt, &bindmnt_queue, entries)
			bindmount(chrootdir, bmnt->src, bmnt->dest);

		/* remount bind mounts as read-only if set */
		SIMPLEQ_FOREACH(bmnt, &bindmnt_queue, entries)
			remount_rdonly(chrootdir, bmnt->src, bmnt->dest, bmnt->ro);

		/* move chrootdir to / and chroot to it */
		if (chdir(chrootdir) == -1)
			die("Failed to chdir to %s", chrootdir);

		if (mount(".", ".", NULL, MS_BIND|MS_PRIVATE, NULL) == -1)
			die("Failed to bind mount %s", chrootdir);

		mount(chrootdir, "/", NULL, MS_MOVE, NULL);

		if (chroot(".") == -1)
			die("Failed to chroot to %s", chrootdir);

		if (execvp(cmd, cmdargs) == -1)
			die("Failed to execute command %s", cmd);
	}
	/* Wait until the child terminates */
	while (waitpid(child, &child_status, 0) < 0) {
		if (errno != EINTR)
			die("waitpid");
	}

	if (!WIFEXITED(child_status)) {
		cleanup_overlayfs();
		return -1;
	}

	cleanup_overlayfs();
	return WEXITSTATUS(child_status);
}
