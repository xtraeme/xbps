/*-
 * Copyright (c) 2012-2021 Juan Romero Pardines.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <sys/stat.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <libgen.h>
#include <assert.h>
#include <pthread.h>
#include <fcntl.h>

#include <xbps.h>
#include "defs.h"

static xbps_dictionary_t dest;

struct CleanerCbInfo {
	const char *repourl;
	bool hashcheck;
	xbps_dictionary_t idxfiles;
};

static int
idx_cleaner_cb(struct xbps_handle *xhp,
		xbps_object_t obj,
		const char *key UNUSED,
		void *arg,
		bool *done UNUSED)
{
	struct CleanerCbInfo *info = arg;
	const char *arch = NULL, *pkgver = NULL, *hash = NULL;
	char *filen, pkgname[XBPS_NAME_SIZE];

	xbps_dictionary_get_string(obj, "architecture", &arch);
	xbps_dictionary_get_string(obj, "pkgver", &pkgver);

	xbps_dbg_printf(xhp, "%s: checking %s [%s] ...\n", info->repourl, pkgver, arch);

	filen = xbps_xasprintf("%s/%s.%s.xbps", info->repourl, pkgver, arch);
	if (access(filen, R_OK) == -1) {
		/*
		 * File cannot be read, might be permissions,
		 * broken or simply unexistent; either way, remove it.
		 */
		if (!xbps_pkg_name(pkgname, sizeof(pkgname), pkgver))
			goto out;
		xbps_dictionary_remove(dest, pkgname);
		xbps_dictionary_remove(info->idxfiles, pkgver);
		printf("index: removed pkg %s (access error)\n", pkgver);
	} else if (info->hashcheck) {
		/*
		 * File can be read; check its hash.
		 */
		int rv;
		if (xbps_dictionary_get_string(obj, "filename-blake3", &hash)) {
			rv = xbps_file_blake3_check(filen, hash);
		} else {
			xbps_dictionary_get_string(obj, "filename-sha256", &hash);
			rv = xbps_file_sha256_check(filen, hash);
		}
		if (rv != 0) {
			if (!xbps_pkg_name(pkgname, sizeof(pkgname), pkgver))
				goto out;
			xbps_dictionary_remove(dest, pkgname);
			xbps_dictionary_remove(info->idxfiles, pkgver);
			printf("index: removed pkg %s (unmatched hash)\n", pkgver);
		}
	}
out:
	free(filen);
	return 0;
}

static void
cleanup_idxfiles(struct xbps_handle *xhp,
		xbps_dictionary_t idxfiles, const char *repodir)
{
	xbps_dictionary_keysym_t key = NULL;
	xbps_array_t allkeys = NULL, array = NULL;
	const char *pkgver = NULL, *arch = NULL;
	char file[XBPS_MAXPATH];

	if (xhp->target_arch) {
		arch = xhp->target_arch;
	} else {
		arch = xhp->native_arch;
	}

	allkeys = xbps_dictionary_all_keys(idxfiles);
	if (!xbps_array_count(allkeys)) {
		return;
	}
	array = xbps_array_create();
	assert(array);
	for (unsigned int i = 0; i < xbps_array_count(allkeys); i++) {
		key = xbps_array_get(allkeys, i);
		pkgver = xbps_dictionary_keysym_value(key);
		snprintf(file, sizeof(file)-1, "%s/%s.%s.xbps", repodir, pkgver, arch);
		if (access(file, R_OK) == -1) {
			xbps_array_add_string(array, pkgver);
		}
	}
	xbps_object_release(allkeys);
	for (unsigned int i = 0; i < xbps_array_count(array); i++) {
		xbps_array_get_string(array, i, &pkgver);
		xbps_dictionary_remove(idxfiles, pkgver);
		printf("index: removed %s from files.plist\n", pkgver);
	}
	xbps_object_release(array);
}

static int
cleanup_repo(struct xbps_handle *xhp, const char *repodir, struct xbps_repo *repo,
	const char *reponame, bool hashcheck, const char *compression)
{
	int rv = 0;
	xbps_dictionary_t idxfiles;
	xbps_array_t allkeys;
	struct CleanerCbInfo info = {
		.hashcheck = hashcheck,
		.repourl = repodir
	};

	idxfiles = xbps_repo_get_filelist(xhp, repodir);
	if (idxfiles) {
		info.idxfiles = xbps_dictionary_copy(idxfiles);
	}
	/*
	 * First pass: find out obsolete entries on index and index-files.
	 */
	dest = xbps_dictionary_copy_mutable(repo->idx);
	allkeys = xbps_dictionary_all_keys(dest);
	(void)xbps_array_foreach_cb_multi(xhp, allkeys, repo->idx, idx_cleaner_cb, &info);
	xbps_object_release(allkeys);

	if (strcmp("stagedata", reponame) == 0 && xbps_dictionary_count(dest) == 0) {
		char *stagefile = xbps_repo_path_with_name(xhp, repodir, "stagedata");
		unlink(stagefile);
		free(stagefile);
	}
	/*
	 * Second pass: find out obsolete entries in files.plist.
	 */
	cleanup_idxfiles(xhp, info.idxfiles, repodir);

	/*
	 * Finally flush to storage if there's any change in dictionaries.
	 */
	if ((!xbps_dictionary_equals(dest, repo->idx)) ||
	    (!xbps_dictionary_equals(idxfiles, info.idxfiles))) {
		if (!repodata_flush(xhp, repodir, reponame, dest, info.idxfiles, repo->idxmeta, compression)) {
			rv = errno;
			fprintf(stderr, "failed to write repodata: %s\n",
			    strerror(errno));
			return rv;
		}
	}
	if (strcmp("stagedata", reponame) == 0)
		printf("stage: %u packages registered.\n", xbps_dictionary_count(dest));
	else
		printf("index: %u packages registered.\n", xbps_dictionary_count(dest));
	return rv;
}

/*
 * Removes stalled pkg entries in repository's XBPS_REPOIDX file, if any
 * binary package cannot be read (unavailable, not enough perms, etc).
 */
int
index_clean(struct xbps_handle *xhp, const char *repodir, const bool hashcheck, const char *compression)
{
	struct xbps_repo *repo, *stage;
	char *rlockfname = NULL;
	int rv = 0, rlockfd = -1;

	if (!xbps_repo_lock(xhp, repodir, &rlockfd, &rlockfname)) {
		rv = errno;
		fprintf(stderr, "%s: cannot lock repository: %s\n",
		    _XBPS_RINDEX, strerror(rv));
		return rv;
	}
	repo = xbps_repo_public_open(xhp, repodir);
	if (repo == NULL) {
		rv = errno;
		if (rv == ENOENT) {
			xbps_repo_unlock(rlockfd, rlockfname);
			return 0;
		}
		fprintf(stderr, "%s: cannot read repository data: %s\n",
		    _XBPS_RINDEX, strerror(errno));
		xbps_repo_unlock(rlockfd, rlockfname);
		return rv;
	}
	stage = xbps_repo_stage_open(xhp, repodir);
	if (repo->idx == NULL || (stage && stage->idx == NULL)) {
		fprintf(stderr, "%s: incomplete repository data file!\n", _XBPS_RINDEX);
		rv = EINVAL;
		goto out;
	}
	printf("Cleaning `%s' index, please wait...\n", repodir);

	if ((rv = cleanup_repo(xhp, repodir, repo, "repodata", hashcheck, compression))) {
		goto out;
	}
	if (stage) {
		cleanup_repo(xhp, repodir, stage, "stagedata", hashcheck, compression);
	}

out:
	xbps_repo_release(repo);
	if(stage)
		xbps_repo_release(stage);
	xbps_repo_unlock(rlockfd, rlockfname);

	return rv;
}
