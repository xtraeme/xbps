## xbps-22.1 (WIP)

* events: when installing or removing pkgs also print its
  installation mode, i.e "automatic" or "manual"; this way we
  we can know what pkgs were installed/removed by the user.
  *[xtraeme]*

* New xbps proplib externalization format. Smaller, faster.
  Both formats can be internalized (for compat), but externalization
  will always be done in the new format. *[xtraeme]*

* libxbps: get rid of `xbps_mmap_file()`; unused; Bumped
  major shlib. *[xtraeme]*
  
* configure: get rid of `--enable-api-docs`; it was unmaintained for
  years and I don't really care at this point. *[xtraeme]*

## xbps-21.1 (2021-12-03)

* `xbps-query(1)`: faster implementation for `-o, --ownedby`
  (2x faster in repo mode, 3x faster in local mode).
  Both local and repository modes are supported. Repository mode
  no longer needs to fetch data from binary packages, because repodata
  contains everything that is necessary. *[xtraeme]*

* `xbps-rindex(1)`: implemented pkg file caching for faster `-o, --ownedby`
  support in `xbps-query(1)`. `files.plist` is added to repodata. It's
  always updated when adding or removing pkgs from repodata.
  *[xtraeme]*

* `xbps-rindex(1)`: when signing archives make sure to match
  the message digest algorithm (SHA256). That means that repositories
  signed with this version won't be valid with void-linux/xbps.
  *[xtraeme]*

* `xbps-create(1)`, `xbps-pkgdb(1)`: fixed an issue while handling relative
  symlinks pointing to absolute paths.
  Fixes [void-linux/xbps#435](https://github.com/void-linux/xbps/issues/435).
  *[xtraeme]*

* bin: all xbps utilities are now built against static libxbps, so that
  the shared library is not necessary anymore. *[xtraeme]*

* `xbps-remove(1)`: fix -R (recursive removal) with multiple packages.
  Fixes [void-linux/xbps#377](https://github.com/void-linux/xbps/issues/377).
  *[xtraeme]*

* libxbps: fix [void-linux/xbps#363](https://github.com/void-linux/xbps/issues/363).
  *[xtraeme]*

* libxbps: fix [void-linux/xbps#417](https://github.com/void-linux/xbps/pull/417).
  *[xtraeme]*

* `xbps-uunshare(1)`: removed; `xbps-uchroot(1)` uses `user_namespaces(7)`
  by default, so that it's redundant. *[xtraeme]*

* libxbps: fix issues with updating packages in unpacked state.
  Merged from [void-linux/xbps](void-linux/xbps). *[duncaen]*

* libxbps: run all scripts before and after unpacking all packages,
  to avoid running things in a half unpacked state.
  Merged from [void-linux/xbps](void-linux/xbps). *[duncaen]*

* libxbps: fix configuration parsing with missing trailing newline
  and remove trailing spaces from values.
  Merged from [void-linux/xbps](void-linux/xbps). *[eater, duncaen]*

* libxbps: fix `XBPS_ARCH` environment variable if architecture
  is also defined in a configuration file.
  Merged from [void-linux/xbps](void-linux/xbps). *[duncaen]*

* libxbps: fix how the automatic/manual mode is set when replacing a
  package using replaces. This makes it possible to correctly replace
  manually installed packages using transitional packages.
  Merged from [void-linux/xbps](void-linux/xbps). *[duncaen]*

* libxbps: fix inconsistent dependency resolution when a dependency
  is on hold. xbps will now exit with ENODEV (19) if a held dependency
  breaks the installation or update of a package instead of just ignoring
  it, resulting in an inconsistent pkgdb.
  Merged from [void-linux/xbps](void-linux/xbps). *[duncaen]*

* `xbps-install(1)`: list reinstalled packages.
  Merged from [void-linux/xbps](void-linux/xbps). *[chocimier]*

* `xbps-install(1)`: in dry-run mode ignore out of space error.
  Merged from [void-linux/xbps](void-linux/xbps). *[chocimier]*

* `xbps-install(1)`: fix bug where a repo locked dependency could be updated
  from a repository it was not locked to.
  Merged from [void-linux/xbps](void-linux/xbps). *[chocimier]*

* `xbps-fetch(1)`: make sure to exit with failure if a failure was encountered.
  Merged from [void-linux/xbps](void-linux/xbps).  *[duncaen]*

* `xbps-fetch(1)`: fix printing uninitialized memory in error cases.
  Merged from [void-linux/xbps](void-linux/xbps). *[duncaen]*

* `xbps-pkgdb(1)`: remove mtime checks, they are unreliable on fat filesystems
  and xbps does not rely on mtime matching the package anymore.
  Merged from [void-linux/xbps](void-linux/xbps). *[duncaen]*

* `xbps-checkvers(1)`: with `--installed` also lists subpackages.
  Merged from [void-linux/xbps](void-linux/xbps). *[chocimier]*

* `xbps.d(5)`: describe ignorepkg more precisely.
  Merged from [void-linux/xbps](void-linux/xbps). *[chocimier]*

* `xbps-uchroot(1)`: switch to `user_namespaces(7)`. That means that
  this does not need to be SGID anymore. Requires linux kernel 5.11. *[xtraeme]*

* `xbps-query(1)`: implemented `--list-modified` option. This lists
  package configuration files that are missing or were modified in rootdir. *[xtraeme]*

* `xbps-query(1)`: slightly change output returned by `-o, --ownedby`.
  Output format is now `"pkgver file type target"`. *[xtraeme]*

* `xbps-remove(1)`: make `-n, --dry-run` work correctly with `-o, --clean-cache`.
  Just print the filepath when it's detected as obsolete.
  Fixes [void-linux/xbps#357](https://github.com/void-linux/xbps/issues/357)
  *[xtraeme]*

* `xbps-install(1)`: change `-n, --dry-run` output slightly, this
  now prints 8 arguments as: `<pkgname> <installedversion> <version> <action> <arch> <repository> <installedsize> <downloadsize>`.
  This way one can see installed version and new version in updates or downgrades.
  Fixes [void-linux/xbps#362](https://github.com/void-linux/xbps/issues/362)
  *[xtraeme]*

* xbps-fetch: use standards compliant struct initializer.
  Merged from [void-linux/xbps#353](https://github.com/void-linux/xbps/pull/353)
  *[ericonr]*

* Removed unnecessary usage of `__UNCONST()`.
  Merged from [void-linux/xbps#355](https://github.com/void-linux/xbps/pull/355)
  *[ericonr]*

* libxbps: speedup package unpack drastically while installing,
  updating or reinstalling packages.
  Merged from [void-linux/xbps#359](https://github.com/void-linux/xbps/pull/359)
  *[duncaen]*

* `xbps-uchroot(1)`: added long options; added support for read-only
  bind mounts; added support for bind mounting single files;
  added support to setup lowerdir with overlayfs; make cmd arg
  optional (defaults to `/bin/sh` if unset). *[xtraeme]*

* `xbps-digest(1)`: added two new modes for `-m,--mode`:
  *nix-sha256* and *nix-blake3*. This encodes the resulting
  hash in the [nix](https://nixos.org/) store format:
  hash is truncated to 160 bits and encoded with base32.
  *[xtraeme]*

* `xbps-alternatives(1)`: added `-M, --memory-sync` flag
  to store remote repository data on memory. *[xtraeme]*

* Syslog logging has been removed completely. The `syslog`
  keyword in `xbps.d(5)` has been renamed to `logging` and
  if set to false disables logging package events (see below).
  Enabled by default. *[xtraeme]*

* xbps-pkgdb(1): the `-m, --mode` flag now removes the object
  from pkgdb rather than setting it to false when unsetting
  the mode. *[xtraeme]*

* Bumped major SONAME due to changing the API required by
  the introduced package events support.

	- `xbps_configure_pkg()`
	- `xbps_configure_packages()`

  Those functions now have two more bool arguments, to force
  reconfiguration and to store records in the log file.
  `XBPS_FLAG_FORCE_CONFIGURE` is now deprecated and kept only for
  historical reasons. *[xtraeme]*

* Log package events when performing changes to pkgdb,
  including *install*, *reinstall*, *update*, *configure*, *downgrade*,
  *remove*, *alternatives set/remove*, and *modes* set via `xbps-pkgdb(1)`.
  The events are recorded in `<rootdir>/<metadir>/events.txt`. *[xtraeme]*

* `xbps-fbulk(1)`: removed; Use [go-xbulk](https://gitlab.com/xtraeme/go-xbulk)
  instead; it's a lot more reliable than the previous C version. *[xtraeme]*

* Merged *portableproplib-0.6.10*: this implements the new
  immutable API from NetBSD HEAD by *thorpej*. Strings are
  internally de-duplicated as a memory footprint optimization.
  While here, get rid of `proplib_wrapper.c`. *[xtraeme]*

* Added *blake3* support for package files, binary packages
  (in local repositories) and metadata files. *[xtraeme]*

* `xbps-create(1)`: added `--cryptohash` option to switch
  between blake3 (default) and sha256. *[xtraeme]*

* `xbps-rindex(1)`: switched to blake3 by default. *[xtraeme]*

* `xbps-digest(1)`: added blake3 support, sha256 is still the
  default mode (required by `xbps-src`). *[xtraeme]*

* libfetch: handle `HTTP_TEMP_REDIRECT` correctly.
  Merged from [void-linux/xbps#349](https://github.com/void-linux/xbps/pull/349)
  *[ericonr]*

* `xbps-alternatives(1)`: added support to list alternatives
  in repository mode via `-R, --repository`.
  Merged from [void-linux/xbps#340](https://github.com/void-linux/xbps/pull/340)
  *[duncaen]*

* `xbps-rindex(1)`: fix a memleak in the sign mode.
  Merged from [void-linux/xbps#327](https://github.com/void-linux/xbps/pull/327)
 *[ArsenArsen]*

* libxbps: transaction: fix file descriptor leaks when
  downloading packages from remote repositories.
  Merged from [void-linux/xbps#326](https://github.com/void-linux/xbps/pull/326)
  *[gt7-void]*

* `xbps-install(1)`: fix dry-run mode to print transaction
  even when rootdir is full (*ENOSPC*).
  Merged from [void-linux/xbps#322](https://github.com/void-linux/xbps/pull/322)
  *[Chocimier]*

* `xbps-install(1)`: list reinstalled package in non colmode.
  Merged from [void-linux/xbps#321](https://github.com/void-linux/xbps/pull/321)
  *[Chocimier]*

* libxbps: improve handling of public keys in remote repositories.
  Merged from [void-linux/xbps#306](https://github.com/void-linux/xbps/pull/306)
  *[duncaen]*

* `xbps-fetch(1)`: return error if any of the specified files
  wasn't downloaded correctly.
  Merged from [void-linux/xbps#296](https://github.com/void-linux/xbps/pull/296)
  *[duncaen]*

* libxbps: transaction: fix leaving behind obsolete
  directories that should be removed.
  Merged from [void-linux/xbps#289](https://github.com/void-linux/xbps/pull/289)
  *[duncaen]*

* configure: accept any version of *OpenSSL* and *LibreSSL*, both
  should be supported. *[xtraeme]*

* misc: removed bash/zsh completions; they get unsync too frequently
  and they don't really belong in this repository. *[xtraeme]*

* libxbps: make sure to respect pkgdb properties while updating
  packages that are in hold or repolock mode. *[xtraeme]*

* libxbps: improved and simplified path/symlinks handling. *[duncaen]*

* `xbps-query(1)`: simplified code and misc fixes. *[duncaen]*

* misc: fixed multiple issues found by Coverity scan. *[xtraeme]*
